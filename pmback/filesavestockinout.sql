PGDMP     .    $    
            x            ERP    12.3    12.3 �    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    24602    ERP    DATABASE     �   CREATE DATABASE "ERP" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE "ERP";
                postgres    false                        3079    24603 	   uuid-ossp 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;
    DROP EXTENSION "uuid-ossp";
                   false            �           0    0    EXTENSION "uuid-ossp"    COMMENT     W   COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';
                        false    2            �            1259    24614    ApprovalStatuses    TABLE     �   CREATE TABLE public."ApprovalStatuses" (
    id integer NOT NULL,
    description character varying(255),
    "descriptionVN" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 &   DROP TABLE public."ApprovalStatuses";
       public         heap    postgres    false            �            1259    24620    ApprovalStatuses_id_seq    SEQUENCE     �   CREATE SEQUENCE public."ApprovalStatuses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public."ApprovalStatuses_id_seq";
       public          postgres    false    203            �           0    0    ApprovalStatuses_id_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public."ApprovalStatuses_id_seq" OWNED BY public."ApprovalStatuses".id;
          public          postgres    false    204            �            1259    24622 	   Customers    TABLE       CREATE TABLE public."Customers" (
    id integer NOT NULL,
    name character varying(255),
    address character varying(255),
    contact character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."Customers";
       public         heap    postgres    false            �            1259    24628    Customers_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Customers_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public."Customers_id_seq";
       public          postgres    false    205            �           0    0    Customers_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public."Customers_id_seq" OWNED BY public."Customers".id;
          public          postgres    false    206            �            1259    49294    DailyReports    TABLE     *  CREATE TABLE public."DailyReports" (
    id integer NOT NULL,
    plan character varying(255),
    result character varying(255),
    "picId" integer,
    "reportDate" timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 "   DROP TABLE public."DailyReports";
       public         heap    postgres    false            �            1259    49292    DailyReports_id_seq    SEQUENCE     �   CREATE SEQUENCE public."DailyReports_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public."DailyReports_id_seq";
       public          postgres    false    231            �           0    0    DailyReports_id_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public."DailyReports_id_seq" OWNED BY public."DailyReports".id;
          public          postgres    false    230            �            1259    57446 
   Inventorys    TABLE     �  CREATE TABLE public."Inventorys" (
    id integer NOT NULL,
    "partNumber" character varying(255),
    "partName" character varying(255),
    price double precision,
    currency character varying(255),
    store character varying(255),
    "position" character varying(255),
    unit character varying(255),
    "actualQty" integer,
    note character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
     DROP TABLE public."Inventorys";
       public         heap    postgres    false            �            1259    57444    Inventorys_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Inventorys_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public."Inventorys_id_seq";
       public          postgres    false    233            �           0    0    Inventorys_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public."Inventorys_id_seq" OWNED BY public."Inventorys".id;
          public          postgres    false    232            �            1259    24630    POItems    TABLE     b  CREATE TABLE public."POItems" (
    id integer NOT NULL,
    "poID" integer,
    "partNumber" character varying(255),
    "partName" character varying(255),
    quantity integer,
    "unitPrice" double precision,
    currency character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."POItems";
       public         heap    postgres    false            �            1259    24636    POItems_id_seq    SEQUENCE     �   CREATE SEQUENCE public."POItems_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public."POItems_id_seq";
       public          postgres    false    207            �           0    0    POItems_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public."POItems_id_seq" OWNED BY public."POItems".id;
          public          postgres    false    208            �            1259    24638    POPICs    TABLE     �   CREATE TABLE public."POPICs" (
    id integer NOT NULL,
    "poID" integer,
    "picID" integer,
    roles character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."POPICs";
       public         heap    postgres    false            �            1259    24641    POPICs_id_seq    SEQUENCE     �   CREATE SEQUENCE public."POPICs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public."POPICs_id_seq";
       public          postgres    false    209            �           0    0    POPICs_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public."POPICs_id_seq" OWNED BY public."POPICs".id;
          public          postgres    false    210            �            1259    24643    POSteps    TABLE     P  CREATE TABLE public."POSteps" (
    id integer NOT NULL,
    "poID" integer,
    "stepID" integer,
    status integer,
    priority character varying(255),
    "photoURLs" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    comment character varying(255)
);
    DROP TABLE public."POSteps";
       public         heap    postgres    false            �            1259    24649    PORunningSituations_id_seq    SEQUENCE     �   CREATE SEQUENCE public."PORunningSituations_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."PORunningSituations_id_seq";
       public          postgres    false    211            �           0    0    PORunningSituations_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."PORunningSituations_id_seq" OWNED BY public."POSteps".id;
          public          postgres    false    212            �            1259    24651    POStatusMasters    TABLE     �   CREATE TABLE public."POStatusMasters" (
    id integer NOT NULL,
    description character varying(255),
    "descriptionVN" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 %   DROP TABLE public."POStatusMasters";
       public         heap    postgres    false            �            1259    24657    POStatusMasters_id_seq    SEQUENCE     �   CREATE SEQUENCE public."POStatusMasters_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."POStatusMasters_id_seq";
       public          postgres    false    213            �           0    0    POStatusMasters_id_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."POStatusMasters_id_seq" OWNED BY public."POStatusMasters".id;
          public          postgres    false    214            �            1259    24659    POStepMasters    TABLE     �   CREATE TABLE public."POStepMasters" (
    id integer NOT NULL,
    sequence integer,
    "stepName" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 #   DROP TABLE public."POStepMasters";
       public         heap    postgres    false            �            1259    24662    POStepMasters_id_seq    SEQUENCE     �   CREATE SEQUENCE public."POStepMasters_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public."POStepMasters_id_seq";
       public          postgres    false    215            �           0    0    POStepMasters_id_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public."POStepMasters_id_seq" OWNED BY public."POStepMasters".id;
          public          postgres    false    216            �            1259    24664    POs    TABLE     �  CREATE TABLE public."POs" (
    id integer NOT NULL,
    "poNumber" character varying(255),
    "customerID" integer,
    "issuedDate" timestamp with time zone,
    "deliveryDate" timestamp with time zone,
    "paymentTerms" character varying(255),
    "poStatus" character varying(255),
    "createdBy" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."POs";
       public         heap    postgres    false            �            1259    24670 
   POs_id_seq    SEQUENCE     �   CREATE SEQUENCE public."POs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public."POs_id_seq";
       public          postgres    false    217            �           0    0 
   POs_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public."POs_id_seq" OWNED BY public."POs".id;
          public          postgres    false    218            �            1259    24672    QuotationItems    TABLE     g  CREATE TABLE public."QuotationItems" (
    id integer NOT NULL,
    "quotationID" integer,
    "partNumber" character varying(255),
    "partName" character varying(255),
    quantity integer,
    "unitPrice" integer,
    currency character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
 $   DROP TABLE public."QuotationItems";
       public         heap    postgres    false            �            1259    24678    QuotationItems_id_seq    SEQUENCE     �   CREATE SEQUENCE public."QuotationItems_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public."QuotationItems_id_seq";
       public          postgres    false    219            �           0    0    QuotationItems_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public."QuotationItems_id_seq" OWNED BY public."QuotationItems".id;
          public          postgres    false    220            �            1259    24680 
   Quotations    TABLE     �  CREATE TABLE public."Quotations" (
    id integer NOT NULL,
    "poID" integer,
    "supplierID" integer,
    "issuedDate" timestamp with time zone,
    "validUntil" timestamp with time zone,
    approved boolean,
    "approvedBy" integer,
    "approvedTime" timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
     DROP TABLE public."Quotations";
       public         heap    postgres    false            �            1259    24683    Quotations_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Quotations_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE public."Quotations_id_seq";
       public          postgres    false    221            �           0    0    Quotations_id_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public."Quotations_id_seq" OWNED BY public."Quotations".id;
          public          postgres    false    222            �            1259    24685    Roles    TABLE     6  CREATE TABLE public."Roles" (
    id integer NOT NULL,
    scope character varying(255),
    role character varying(255),
    description character varying(255),
    "descriptionVN" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."Roles";
       public         heap    postgres    false            �            1259    24691    Roles_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Roles_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."Roles_id_seq";
       public          postgres    false    223            �           0    0    Roles_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public."Roles_id_seq" OWNED BY public."Roles".id;
          public          postgres    false    224            �            1259    24693    SequelizeMeta    TABLE     R   CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);
 #   DROP TABLE public."SequelizeMeta";
       public         heap    postgres    false            �            1259    57457    StockIns    TABLE     �  CREATE TABLE public."StockIns" (
    id integer NOT NULL,
    "receiptNo" character varying(255),
    "partNumber" character varying(255),
    unit character varying(255),
    quantity integer,
    "quotationID" integer,
    date timestamp with time zone,
    note character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."StockIns";
       public         heap    postgres    false            �            1259    57455    StockIns_id_seq    SEQUENCE     �   CREATE SEQUENCE public."StockIns_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public."StockIns_id_seq";
       public          postgres    false    235            �           0    0    StockIns_id_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public."StockIns_id_seq" OWNED BY public."StockIns".id;
          public          postgres    false    234            �            1259    57468 	   StockOuts    TABLE     �  CREATE TABLE public."StockOuts" (
    id integer NOT NULL,
    "receiptNo" character varying(255),
    "partNumber" character varying(255),
    unit character varying(255),
    quantity integer,
    "poID" character varying(255),
    date timestamp with time zone,
    evidence character varying(255),
    note character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."StockOuts";
       public         heap    postgres    false            �            1259    57466    StockOuts_id_seq    SEQUENCE     �   CREATE SEQUENCE public."StockOuts_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public."StockOuts_id_seq";
       public          postgres    false    237            �           0    0    StockOuts_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public."StockOuts_id_seq" OWNED BY public."StockOuts".id;
          public          postgres    false    236            �            1259    24696 	   Suppliers    TABLE       CREATE TABLE public."Suppliers" (
    id integer NOT NULL,
    name character varying(255),
    address character varying(255),
    contact character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);
    DROP TABLE public."Suppliers";
       public         heap    postgres    false            �            1259    24702    Suppliers_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Suppliers_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public."Suppliers_id_seq";
       public          postgres    false    226            �           0    0    Suppliers_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public."Suppliers_id_seq" OWNED BY public."Suppliers".id;
          public          postgres    false    227            �            1259    24704    Users    TABLE     �  CREATE TABLE public."Users" (
    id integer NOT NULL,
    email character varying(255),
    password character varying(255),
    "displayName" character varying(255),
    tel character varying(255),
    address character varying(255),
    "lastLogin" timestamp with time zone,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    uuid uuid DEFAULT public.uuid_generate_v4() NOT NULL
);
    DROP TABLE public."Users";
       public         heap    postgres    false    2            �            1259    24711    Users_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Users_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."Users_id_seq";
       public          postgres    false    228            �           0    0    Users_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public."Users_id_seq" OWNED BY public."Users".id;
          public          postgres    false    229            �
           2604    24755    ApprovalStatuses id    DEFAULT     ~   ALTER TABLE ONLY public."ApprovalStatuses" ALTER COLUMN id SET DEFAULT nextval('public."ApprovalStatuses_id_seq"'::regclass);
 D   ALTER TABLE public."ApprovalStatuses" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    203            �
           2604    24756    Customers id    DEFAULT     p   ALTER TABLE ONLY public."Customers" ALTER COLUMN id SET DEFAULT nextval('public."Customers_id_seq"'::regclass);
 =   ALTER TABLE public."Customers" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    206    205            
           2604    49297    DailyReports id    DEFAULT     v   ALTER TABLE ONLY public."DailyReports" ALTER COLUMN id SET DEFAULT nextval('public."DailyReports_id_seq"'::regclass);
 @   ALTER TABLE public."DailyReports" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    230    231    231                       2604    57449    Inventorys id    DEFAULT     r   ALTER TABLE ONLY public."Inventorys" ALTER COLUMN id SET DEFAULT nextval('public."Inventorys_id_seq"'::regclass);
 >   ALTER TABLE public."Inventorys" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    233    232    233            �
           2604    24757 
   POItems id    DEFAULT     l   ALTER TABLE ONLY public."POItems" ALTER COLUMN id SET DEFAULT nextval('public."POItems_id_seq"'::regclass);
 ;   ALTER TABLE public."POItems" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    208    207            �
           2604    24758 	   POPICs id    DEFAULT     j   ALTER TABLE ONLY public."POPICs" ALTER COLUMN id SET DEFAULT nextval('public."POPICs_id_seq"'::regclass);
 :   ALTER TABLE public."POPICs" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    210    209                       2604    24759    POStatusMasters id    DEFAULT     |   ALTER TABLE ONLY public."POStatusMasters" ALTER COLUMN id SET DEFAULT nextval('public."POStatusMasters_id_seq"'::regclass);
 C   ALTER TABLE public."POStatusMasters" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    213                       2604    24760    POStepMasters id    DEFAULT     x   ALTER TABLE ONLY public."POStepMasters" ALTER COLUMN id SET DEFAULT nextval('public."POStepMasters_id_seq"'::regclass);
 A   ALTER TABLE public."POStepMasters" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    215                        2604    24761 
   POSteps id    DEFAULT     x   ALTER TABLE ONLY public."POSteps" ALTER COLUMN id SET DEFAULT nextval('public."PORunningSituations_id_seq"'::regclass);
 ;   ALTER TABLE public."POSteps" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211                       2604    24762    POs id    DEFAULT     d   ALTER TABLE ONLY public."POs" ALTER COLUMN id SET DEFAULT nextval('public."POs_id_seq"'::regclass);
 7   ALTER TABLE public."POs" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    218    217                       2604    24763    QuotationItems id    DEFAULT     z   ALTER TABLE ONLY public."QuotationItems" ALTER COLUMN id SET DEFAULT nextval('public."QuotationItems_id_seq"'::regclass);
 B   ALTER TABLE public."QuotationItems" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    220    219                       2604    24764    Quotations id    DEFAULT     r   ALTER TABLE ONLY public."Quotations" ALTER COLUMN id SET DEFAULT nextval('public."Quotations_id_seq"'::regclass);
 >   ALTER TABLE public."Quotations" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    222    221                       2604    24765    Roles id    DEFAULT     h   ALTER TABLE ONLY public."Roles" ALTER COLUMN id SET DEFAULT nextval('public."Roles_id_seq"'::regclass);
 9   ALTER TABLE public."Roles" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    224    223                       2604    57460    StockIns id    DEFAULT     n   ALTER TABLE ONLY public."StockIns" ALTER COLUMN id SET DEFAULT nextval('public."StockIns_id_seq"'::regclass);
 <   ALTER TABLE public."StockIns" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    235    234    235                       2604    57471    StockOuts id    DEFAULT     p   ALTER TABLE ONLY public."StockOuts" ALTER COLUMN id SET DEFAULT nextval('public."StockOuts_id_seq"'::regclass);
 =   ALTER TABLE public."StockOuts" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    236    237    237                       2604    24766    Suppliers id    DEFAULT     p   ALTER TABLE ONLY public."Suppliers" ALTER COLUMN id SET DEFAULT nextval('public."Suppliers_id_seq"'::regclass);
 =   ALTER TABLE public."Suppliers" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    227    226            	           2604    24767    Users id    DEFAULT     h   ALTER TABLE ONLY public."Users" ALTER COLUMN id SET DEFAULT nextval('public."Users_id_seq"'::regclass);
 9   ALTER TABLE public."Users" ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    229    228            �          0    24614    ApprovalStatuses 
   TABLE DATA           h   COPY public."ApprovalStatuses" (id, description, "descriptionVN", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    203   ]�       �          0    24622 	   Customers 
   TABLE DATA           [   COPY public."Customers" (id, name, address, contact, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    205   z�       �          0    49294    DailyReports 
   TABLE DATA           k   COPY public."DailyReports" (id, plan, result, "picId", "reportDate", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    231   ��       �          0    57446 
   Inventorys 
   TABLE DATA           �   COPY public."Inventorys" (id, "partNumber", "partName", price, currency, store, "position", unit, "actualQty", note, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    233   �       �          0    24630    POItems 
   TABLE DATA           �   COPY public."POItems" (id, "poID", "partNumber", "partName", quantity, "unitPrice", currency, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    207   ԛ       �          0    24638    POPICs 
   TABLE DATA           X   COPY public."POPICs" (id, "poID", "picID", roles, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    209   �       �          0    24651    POStatusMasters 
   TABLE DATA           g   COPY public."POStatusMasters" (id, description, "descriptionVN", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    213   ��       �          0    24659    POStepMasters 
   TABLE DATA           ]   COPY public."POStepMasters" (id, sequence, "stepName", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    215   _�       �          0    24643    POSteps 
   TABLE DATA           {   COPY public."POSteps" (id, "poID", "stepID", status, priority, "photoURLs", "createdAt", "updatedAt", comment) FROM stdin;
    public          postgres    false    211   �       �          0    24664    POs 
   TABLE DATA           �   COPY public."POs" (id, "poNumber", "customerID", "issuedDate", "deliveryDate", "paymentTerms", "poStatus", "createdBy", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    217   \�       �          0    24672    QuotationItems 
   TABLE DATA           �   COPY public."QuotationItems" (id, "quotationID", "partNumber", "partName", quantity, "unitPrice", currency, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    219   ��       �          0    24680 
   Quotations 
   TABLE DATA           �   COPY public."Quotations" (id, "poID", "supplierID", "issuedDate", "validUntil", approved, "approvedBy", "approvedTime", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    221   l�       �          0    24685    Roles 
   TABLE DATA           j   COPY public."Roles" (id, scope, role, description, "descriptionVN", "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    223   �       �          0    24693    SequelizeMeta 
   TABLE DATA           /   COPY public."SequelizeMeta" (name) FROM stdin;
    public          postgres    false    225   �       �          0    57457    StockIns 
   TABLE DATA           �   COPY public."StockIns" (id, "receiptNo", "partNumber", unit, quantity, "quotationID", date, note, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    235   �       �          0    57468 	   StockOuts 
   TABLE DATA           �   COPY public."StockOuts" (id, "receiptNo", "partNumber", unit, quantity, "poID", date, evidence, note, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    237   ;�       �          0    24696 	   Suppliers 
   TABLE DATA           [   COPY public."Suppliers" (id, name, address, contact, "createdAt", "updatedAt") FROM stdin;
    public          postgres    false    226   ��       �          0    24704    Users 
   TABLE DATA           �   COPY public."Users" (id, email, password, "displayName", tel, address, "lastLogin", "createdAt", "updatedAt", uuid) FROM stdin;
    public          postgres    false    228   ��       �           0    0    ApprovalStatuses_id_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public."ApprovalStatuses_id_seq"', 1, false);
          public          postgres    false    204            �           0    0    Customers_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public."Customers_id_seq"', 6, true);
          public          postgres    false    206            �           0    0    DailyReports_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public."DailyReports_id_seq"', 1, false);
          public          postgres    false    230            �           0    0    Inventorys_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public."Inventorys_id_seq"', 11, true);
          public          postgres    false    232            �           0    0    POItems_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public."POItems_id_seq"', 7, true);
          public          postgres    false    208            �           0    0    POPICs_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public."POPICs_id_seq"', 98, true);
          public          postgres    false    210            �           0    0    PORunningSituations_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."PORunningSituations_id_seq"', 50, true);
          public          postgres    false    212            �           0    0    POStatusMasters_id_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public."POStatusMasters_id_seq"', 6, true);
          public          postgres    false    214            �           0    0    POStepMasters_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public."POStepMasters_id_seq"', 5, true);
          public          postgres    false    216            �           0    0 
   POs_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public."POs_id_seq"', 81, true);
          public          postgres    false    218            �           0    0    QuotationItems_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public."QuotationItems_id_seq"', 9, true);
          public          postgres    false    220            �           0    0    Quotations_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public."Quotations_id_seq"', 4, true);
          public          postgres    false    222            �           0    0    Roles_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public."Roles_id_seq"', 213, true);
          public          postgres    false    224            �           0    0    StockIns_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public."StockIns_id_seq"', 1, false);
          public          postgres    false    234            �           0    0    StockOuts_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public."StockOuts_id_seq"', 1, false);
          public          postgres    false    236            �           0    0    Suppliers_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public."Suppliers_id_seq"', 9, true);
          public          postgres    false    227            �           0    0    Users_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."Users_id_seq"', 14, true);
          public          postgres    false    229                       2606    24727 &   ApprovalStatuses ApprovalStatuses_pkey 
   CONSTRAINT     h   ALTER TABLE ONLY public."ApprovalStatuses"
    ADD CONSTRAINT "ApprovalStatuses_pkey" PRIMARY KEY (id);
 T   ALTER TABLE ONLY public."ApprovalStatuses" DROP CONSTRAINT "ApprovalStatuses_pkey";
       public            postgres    false    203                       2606    24729    Customers Customers_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."Customers"
    ADD CONSTRAINT "Customers_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public."Customers" DROP CONSTRAINT "Customers_pkey";
       public            postgres    false    205            +           2606    49302    DailyReports DailyReports_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public."DailyReports"
    ADD CONSTRAINT "DailyReports_pkey" PRIMARY KEY (id);
 L   ALTER TABLE ONLY public."DailyReports" DROP CONSTRAINT "DailyReports_pkey";
       public            postgres    false    231            -           2606    57454    Inventorys Inventorys_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public."Inventorys"
    ADD CONSTRAINT "Inventorys_pkey" PRIMARY KEY (id);
 H   ALTER TABLE ONLY public."Inventorys" DROP CONSTRAINT "Inventorys_pkey";
       public            postgres    false    233                       2606    24731    POItems POItems_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public."POItems"
    ADD CONSTRAINT "POItems_pkey" PRIMARY KEY (id);
 B   ALTER TABLE ONLY public."POItems" DROP CONSTRAINT "POItems_pkey";
       public            postgres    false    207                       2606    24733    POPICs POPICs_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public."POPICs"
    ADD CONSTRAINT "POPICs_pkey" PRIMARY KEY (id);
 @   ALTER TABLE ONLY public."POPICs" DROP CONSTRAINT "POPICs_pkey";
       public            postgres    false    209                       2606    24735     POSteps PORunningSituations_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."POSteps"
    ADD CONSTRAINT "PORunningSituations_pkey" PRIMARY KEY (id);
 N   ALTER TABLE ONLY public."POSteps" DROP CONSTRAINT "PORunningSituations_pkey";
       public            postgres    false    211                       2606    24737 $   POStatusMasters POStatusMasters_pkey 
   CONSTRAINT     f   ALTER TABLE ONLY public."POStatusMasters"
    ADD CONSTRAINT "POStatusMasters_pkey" PRIMARY KEY (id);
 R   ALTER TABLE ONLY public."POStatusMasters" DROP CONSTRAINT "POStatusMasters_pkey";
       public            postgres    false    213                       2606    24739     POStepMasters POStepMasters_pkey 
   CONSTRAINT     b   ALTER TABLE ONLY public."POStepMasters"
    ADD CONSTRAINT "POStepMasters_pkey" PRIMARY KEY (id);
 N   ALTER TABLE ONLY public."POStepMasters" DROP CONSTRAINT "POStepMasters_pkey";
       public            postgres    false    215                       2606    24741    POs POs_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public."POs"
    ADD CONSTRAINT "POs_pkey" PRIMARY KEY (id);
 :   ALTER TABLE ONLY public."POs" DROP CONSTRAINT "POs_pkey";
       public            postgres    false    217                       2606    24743 "   QuotationItems QuotationItems_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public."QuotationItems"
    ADD CONSTRAINT "QuotationItems_pkey" PRIMARY KEY (id);
 P   ALTER TABLE ONLY public."QuotationItems" DROP CONSTRAINT "QuotationItems_pkey";
       public            postgres    false    219            !           2606    24745    Quotations Quotations_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public."Quotations"
    ADD CONSTRAINT "Quotations_pkey" PRIMARY KEY (id);
 H   ALTER TABLE ONLY public."Quotations" DROP CONSTRAINT "Quotations_pkey";
       public            postgres    false    221            #           2606    24747    Roles Roles_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Roles"
    ADD CONSTRAINT "Roles_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Roles" DROP CONSTRAINT "Roles_pkey";
       public            postgres    false    223            %           2606    24749     SequelizeMeta SequelizeMeta_pkey 
   CONSTRAINT     d   ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);
 N   ALTER TABLE ONLY public."SequelizeMeta" DROP CONSTRAINT "SequelizeMeta_pkey";
       public            postgres    false    225            /           2606    57465    StockIns StockIns_pkey 
   CONSTRAINT     X   ALTER TABLE ONLY public."StockIns"
    ADD CONSTRAINT "StockIns_pkey" PRIMARY KEY (id);
 D   ALTER TABLE ONLY public."StockIns" DROP CONSTRAINT "StockIns_pkey";
       public            postgres    false    235            1           2606    57476    StockOuts StockOuts_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."StockOuts"
    ADD CONSTRAINT "StockOuts_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public."StockOuts" DROP CONSTRAINT "StockOuts_pkey";
       public            postgres    false    237            '           2606    24751    Suppliers Suppliers_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public."Suppliers"
    ADD CONSTRAINT "Suppliers_pkey" PRIMARY KEY (id);
 F   ALTER TABLE ONLY public."Suppliers" DROP CONSTRAINT "Suppliers_pkey";
       public            postgres    false    226            )           2606    24753    Users Users_pkey 
   CONSTRAINT     R   ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public."Users" DROP CONSTRAINT "Users_pkey";
       public            postgres    false    228            �      x������ � �      �   5  x�}P�J�0}N��ۆ$MڭO�-�8�`s������D���ߛ�C�.��\ι�0���F*��[���;mf�6�~Jz�8�4�<f��<IrA���+��a���n�����Ţ@�6��w��Yc7,��v?�DN%a|L���ִOʴhs��SD"�����q�r��7~%��d�'�dΦ9��5� ��r�P��՛uP,���RA0[���n��B���[��<��n,U�ٹ��#�,fi�@�'H����1K��^+��pq4Z+�Zwh�׍�[�9Ҙ1`>�,�ް���%����1�����      �   ?   x�3�L�TH�������44�4202�50�52V00�#ms҄��@�f����S��=... �#�      �   �   x����
�0�s���ʟ�Y��w�$�x�EPP�)��oo;'lx0	)$���/3\�3���㔢�AP"���Z����CT�·�*x$/��a�(�2�?+0Q�(�j�a��[#pL��y�R�w-u��4���D�ߩ�u��k��ֱ��yd�1�y���/O��B���
V��w�(�7��K      �   7   x�3�42��M�T�̃Q��@d`` b�X�(X����9a�=... �".      �   �   x�u�11k�
��`������m%QR�D7;�"/P!�A6$:e��]����eg�hN�Z��y5|};!.U����_`��?��r��Ȋ%��EZ���Ǐ��oj=��4P�nl�'��^A�/1      �   �   x�}�K
�@@��)��d>:3��	�H+m�:��篶P�Bv�<^b���c���
�4i*I����L�A�6'�� �pO����@A��A��1�nS7�b���a)�~�TЎ�e�Na��q�b'�0�\̹�2��
�T]���"toǧ���%/����/�OW{      �   �   x�}��
�0E痯�.y/I#�D����Rl��mJ���."��s��j�p�0�a�)��4���H�,$��ӥ�(��S��c3��0�����{�gK_����]�j�Ids_V�! T���lU���P��B�����C�=�N1�)Ďo�i%P�b+��1�ƈP�      �   3  x����j�0����I������{�\z��(}f%$�(+9
����X\t�Η������# �A���`̈5wo�[���;��5���q#nP2a�6(���#��[.O�:&���<&�ы��nfcf�[�Sö2�N�:F����t"�cqaX�?�	��p�q����d��a�1�%$0ߚc��~.���|�W�	l�Y1V�\��m�2k�(�G����a+w�GK�,)H��bk��U����l�(�`qrT˪;lH�b�>��U��\��Pُ���� w���`}���^R<      �     x����j�0�g�)��s�dk����d0��C�M�u �_�㋆-ȃ�w�'��}}#  � +T�?d�* ���A��� ��ǣZ/N��9M�#S�V*w�!�81(��@$2Z�jn0DZ�jV}�7��`x�~t�\o��1X7��!-��߼v$J�V�Lz9�j�E���A�E	�x�04�#-�f�][1u�Ơ��:�,9���.A=o�������?߻�~W������3����H#��H]zl��Α.��4��)D���2ј ��Z�^��      �   �   x���1k�0����Wx/ߝN��9�	J�..4���f鿯��8�P�rzу$OJ�J ���|���$���"��F�E+��k���4�d!4��֓
��ZL�m���|YWo�́<m�@����?|���v�H�$f���$ٍv�8�b���[w [f�\le�7=�����/���.�p��E��+Y�5θ����Nа��w�M����1���|�      �   �   x���;�0Dk��W����Cp�����A)(�������$k�$x/@<���ע8ķ�]�0���@'c�l�.��B�y�,S)��S��_���{{7���n!�Z��Ԥ���EƜ���⣲M<��N��n��lS)�RH�N�;���H�V6      �   �   x����j�@E�ݯP<�cg�9��@�)E����o-a!kc�ݹ�;��}|��Nut�:�����؏��;F��j�hA�0�L� OX��L���r�<�CIb�]�<=�Ò��;
&jB L��	y&YU��m?�1k��j�����0���k]�����И�MB&��nz���%�����}�*D���q{jƏf�A�%5�4o6�����_�{-;��      �   �  x����n�0��y��HYҹ�������2 ���X�-w�ۏJ�ʲ��E���/�)�"%�`Lp�
�}S��vm���n7bh�����C���8A�Һ�u�<�vu},:�'�]��)A���YR)m5Д� �G�n�K~LG.�ႍ�42ɣ����ӿf�q6䚮�H�a*`u��̆�4#�@���E,a���h��4	�3!��`9ˀ�&e�3w�T������m�QQ/4��~�S�A��<p�Cy23V�pR�j�vߔ����筳�_|>?���T�{l�V��Ky���=v�jW�����;��<�Ǉ����H`�삐d6�oU�������zz���4����v��:�{��9�w�"�	L�q1KX*��bU�{Z.�'�e|�JL��[�T��K�u�{]��T䉜�� ���OG��l	�	 �2q��0+i��վ�ﲨ&�)��$�Z�S �L��̮eĐ�D�Fi�H��,:yPW��.�"�H��3�L��r���rQ�i)԰����s����<#�L�ڡ�WK�w!K�	p�L��;���h�Je]4z����ۦ�m�"J�R4+(3s�L�JR�S�TF��ٓ��3���?�)& aћ�%)��1���0�W��J���0;�4�9����ce�RT�6�#D��	=Y��	Z�:&l���fNt�Mo%���'h`��Џ��f��O�n      �   F   x�3�4��P040�v�44�4��!##]]#+0�60�!�e1���j����qqq -�p      �   D   x�3�4��P040�v�44�42��"##]]#+0�60�!�e1����
���qqq /j=      �     x�}��n�0E�㯘}��G��W��D�hV]�q"FM� ����]UHԅ�9w����f�}yg�)kg8�0/���%|9l7�����k@0��� W:�Z	�Y���{$��'e����������07�۶v.������Қ�Dj�Д��(��#i�h
/�Pt��T�鱨�e�E�n����G;��ݨ�e[��?YN��	!�`����7̥�V�M~:����a�ӫ�8��Ө�D"h8���T����u�)DR�?��|�II�"��]p�ȒB��́C      �   N  x�}��n�8���S��w#���S���2��R�B���O!%@<�~�y�y�I�f�#���X���^�R4y�c���,r���9�䔮��������mS�/��F�+�̟\f�3��������$k�����M�m�#���Q�	�	W	����W
�j#�A̽��rM�,<S5@��j#������kVo�yoQ<Σ��t�->�����Ɯ���g}ӗ�ڐN<�,`Pa��tX���6�􄲤�*
>mHL�	�I��Hj^IRi�c���Ɯh��R%�� �ԬN��D>d���i3T��߉4���y1�W�"�J�/�_�f��b/[��v��*( zX�Ѓ�	M(� f�� 	������ا �3R^�V����q:���g������~�����lwu�|�l�m�.o|�������\���!��{�������~�qD�7'�X"��'����^x�9�5K6`D�DX�Y��2��ܓ���5蹺|����s�'��V˅��~�u�`��O����ɒߠ���
��j�Qc�����0߄�� �����$�.&e�D��&Ȉ�� ����qNh�0�8-a�2҅�(ϳ�8���Ѣ%���>M�^;]~��=��g��$��wꥱn��X���"%���E��#"�>h����2Sͧ��:�$!qBH���73)U V��M�g�‭��*�Y�M�n��B��u((�Ǯ��{������n��Y����ۮ+6����:䤘��?�ӥ-������������Wev_��)o��*1D���v�޳2}������ԧ���FlS)0%�q��Q�V��i�     