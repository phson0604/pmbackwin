"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _chai = _interopRequireDefault(require("chai"));

var _chaiHttp = _interopRequireDefault(require("chai-http"));

var _chaiDatetime = _interopRequireDefault(require("chai-datetime"));

require("chai/register-should");

var _index = _interopRequireDefault(require("../index"));

var HTTP_CODE = require("../server/utils/HttpStatusCode");

_chai["default"].use(_chaiHttp["default"]);

_chai["default"].use(_chaiDatetime["default"]);

var expect = _chai["default"].expect;
describe("Testing the Supplier endpoints:", function () {
  it("It should create a Supplier", function (done) {
    var supplier = {
      name: "DELL LLC",
      address: "Malaysia",
      contact: "ThienCang"
    }; // console.log(HTTP_CODE);

    _chai["default"].request(_index["default"]).post("/api/v1/Suppliers").set("Accept", "application/json").send(supplier).end(function (err, res) {
      res.should.have.status(HTTP_CODE.CREATED);
      expect(res.body.data).to.include({
        name: supplier.name,
        address: supplier.address,
        contact: supplier.contact
      });
      done();
    });
  });
  it("It should not create a Supplier with incomplete parameters", function (done) {
    var supplier = {
      name: supplier.name,
      //   address: supplier.address,
      contact: supplier.contact
    };

    _chai["default"].request(_index["default"]).post("/api/v1/Suppliers").set("Accept", "application/json").send(supplier).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      done();
    });
  });
  it("It should get all Suppliers", function (done) {
    _chai["default"].request(_index["default"]).get("/api/v1/Suppliers").set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(200);
      res.body.data[0].should.have.property("id");
      res.body.data[0].should.have.property("name");
      res.body.data[0].should.have.property("address");
      res.body.data[0].should.have.property("contact");
      done();
    });
  });
  it("It should get a particular Supplier", function (done) {
    var supplierID = "3";

    _chai["default"].request(_index["default"]).get("/api/v1/Suppliers/".concat(supplierID)).set("Accept", "application/json").end(function (err, res) {
      // console.log("get a particular Supplier: ", res.status);
      expect(res.status).to.equal(HTTP_CODE.OK);
      res.should.be.json;
      res.body.data.should.have.property("id");
      res.body.data[0].should.have.property("name");
      res.body.data[0].should.have.property("address");
      res.body.data[0].should.have.property("contact");
      done();
    });
  });
  it("It should not get a particular Supplier with invalid id", function (done) {
    var supplierID = 8888;

    _chai["default"].request(_index["default"]).get("/api/v1/Suppliers/".concat(supplierID)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Cannot find Supplier with the id ".concat(supplierID));
      done();
    });
  });
  it("It should not get a particular Supplier with non-numeric id", function (done) {
    var supplierID = "aaa";

    _chai["default"].request(_index["default"]).get("/api/v1/Suppliers/".concat(supplierID)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please input a valid numeric value");
      done();
    });
  });
  it("It should update a Supplier", function (done) {
    var supplierID = 5;
    var updatedSupplier = {
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/Suppliers/".concat(supplierID)).set("Accept", "application/json").send(updatedSupplier).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.OK);
      expect(res.body.data.id).equal(updatedSupplier.id);
      expect(res.body.data.name).equal(updatedSupplier.name);
      expect(res.body.data.address).equal(updatedSupplier.address);
      expect(res.body.data.contact).equal(updatedSupplier.contact);
      done();
    });
  });
  it("It should not update a Supplier with invalid id", function (done) {
    var supplierID = "9999";
    var updatedSupplier = {
      id: supplierID,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/Suppliers/".concat(supplierID)).set("Accept", "application/json").send(updatedSupplier).end(function (err, res) {
      // console.log("res:", res.body);
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Cannot find Supplier with the id: ".concat(supplierID));
      done();
    });
  });
  it("It should not update a Supplier with non-numeric id value", function (done) {
    var SupplierId = "ggg";
    var updatedSupplier = {
      id: SupplierId,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/Suppliers/".concat(SupplierId)).set("Accept", "application/json").send(updatedSupplier).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please input a valid numeric value");
      done();
    });
  });
  it("It should delete a Supplier", function (done) {
    var SupplierId = 7;

    _chai["default"].request(_index["default"])["delete"]("/api/v1/Suppliers/".concat(SupplierId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.OK);
      expect(res.body.data).to.include({});
      done();
    });
  });
  it("It should not delete a Supplier with invalid id", function (done) {
    var SupplierId = 777;

    _chai["default"].request(_index["default"])["delete"]("/api/v1/Suppliers/".concat(SupplierId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Supplier with the id ".concat(SupplierId, " cannot be found"));
      done();
    });
  });
  it("It should not delete a Supplier with non-numeric id", function (done) {
    var SupplierId = "bbb";

    _chai["default"].request(_index["default"])["delete"]("/api/v1/Suppliers/".concat(SupplierId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please provide a numeric value");
      done();
    });
  });
});
//# sourceMappingURL=testSupplier.js.map