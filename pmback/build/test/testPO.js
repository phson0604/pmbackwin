"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _chai = _interopRequireDefault(require("chai"));

var _chaiHttp = _interopRequireDefault(require("chai-http"));

require("chai/register-should");

var _index = _interopRequireDefault(require("../index"));

var HTTP_CODE = require("../server/utils/HttpStatusCode");

_chai["default"].use(_chaiHttp["default"]);

var expect = _chai["default"].expect;
describe("Testing the PO endpoints:", function () {
  it("It should create a PO", function (done) {
    var PO = {
      poNumber: "First Automation PO",
      customerID: 1,
      issuedDate: "1/16/2020",
      deliveryDate: "1/16/2021",
      paymentTerms: "COD"
    }; // console.log(HTTP_CODE);

    _chai["default"].request(_index["default"]).post("/api/v1/POs").set("Accept", "application/json").send(PO).end(function (err, res) {
      res.should.have.status(HTTP_CODE.CREATED);
      expect(res.body.data).to.include({
        // id: 1,
        poNumber: PO.poNumber,
        customerID: PO.customerID // issuedDate: new Date(2020, 1, 16),
        // deliveryDate: new Date(2021, 1, 16)

      });
      done();
    });
  }); // it("It should not create a PO with incomplete parameters", done => {
  //   const po = {
  //     poNumber: "Second PO",
  //     issuedDate: Date.now(),
  //     deliveryDate: Date.now()
  //   };
  //   chai
  //     .request(app)
  //     .post("/api/v1/POs")
  //     .set("Accept", "application/json")
  //     .send(po)
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //       done();
  //     });
  // });
  // it("It should get all POs", done => {
  //   chai
  //     .request(app)
  //     .get("/api/v1/POs")
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       expect(res.status).to.equal(200);
  //       // console.log("RequestReturn:", res.body.data);
  //       res.body.data[0].should.have.property("id");
  //       res.body.data[0].should.have.property("poNumber");
  //       res.body.data[0].should.have.property("customerID");
  //       res.body.data[0].should.have.property("issuedDate");
  //       res.body.data[0].should.have.property("deliveryDate");
  //       done();
  //     });
  // });
  // it("It should get a particular PO", done => {
  //   const poID = "3";
  //   chai
  //     .request(app)
  //     .get(`/api/v1/POs/${poID}`)
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       // console.log("get a particular PO: ", res.status);
  //       expect(res.status).to.equal(HTTP_CODE.OK);
  //       res.should.be.json;
  //       res.body.data.should.have.property("id");
  //       res.body.data.should.have.property("poNumber");
  //       res.body.data.should.have.property("customerID");
  //       res.body.data.should.have.property("issuedDate");
  //       res.body.data.should.have.property("deliveryDate");
  //       done();
  //     });
  // });
  // it("It should not get a particular PO with invalid id", done => {
  //   const poID = 8888;
  //   chai
  //     .request(app)
  //     .get(`/api/v1/POs/${poID}`)
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //       res.body.should.have
  //         .property("message")
  //         .eql(`Cannot find PO with the id ${poID}`);
  //       done();
  //     });
  // });
  // it("It should not get a particular PO with non-numeric id", done => {
  //   const poID = "aaa";
  //   chai
  //     .request(app)
  //     .get(`/api/v1/POs/${poID}`)
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //       res.body.should.have
  //         .property("message")
  //         .eql("Please input a valid numeric value");
  //       done();
  //     });
  // });
  // it("It should update a PO", done => {
  //   const poID = 5;
  //   const updatedPO = {
  //     id: poID,
  //     poNumber: "Updated Awesome PO",
  //     customerID: 2,
  //     issuedDate: "1/1/2019",
  //     deliveryDate: "1/1/2020"
  //   };
  //   chai
  //     .request(app)
  //     .put(`/api/v1/POs/${poID}`)
  //     .set("Accept", "application/json")
  //     .send(updatedPO)
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.OK);
  //       expect(res.body.data.id).equal(updatedPO.id);
  //       expect(res.body.data.poNumber).equal(updatedPO.poNumber);
  //       expect(res.body.data.customerID).equal(updatedPO.customerID);
  //       expect(res.body.data.issuedDate).equal(updatedPO.issuedDate);
  //       expect(res.body.data.deliveryDate).equal(updatedPO.deliveryDate);
  //       done();
  //     });
  // });
  // it("It should not update a PO with invalid id", done => {
  //   const poID = "9999";
  //   const updatedPO = {
  //     id: poID,
  //     poNumber: "Updated Awesome PO again",
  //     customerID: 3,
  //     issuedDate: "1/1/1999"
  //   };
  //   chai
  //     .request(app)
  //     .put(`/api/v1/POs/${poID}`)
  //     .set("Accept", "application/json")
  //     .send(updatedPO)
  //     .end((err, res) => {
  //       // console.log("res:", res.body);
  //       expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //       res.body.should.have
  //         .property("message")
  //         .eql(`Cannot find PO with the id: ${poID}`);
  //       done();
  //     });
  // });
  // it("It should not update a PO with non-numeric id value", done => {
  //   const POId = "ggg";
  //   const updatedPO = {
  //     id: POId,
  //     poNumber: "Updated Awesome PO again",
  //     customerID: "1",
  //     issuedDate: "1/1/1999",
  //     deliveryDate: "1/1/1999"
  //   };
  //   chai
  //     .request(app)
  //     .put(`/api/v1/POs/${POId}`)
  //     .set("Accept", "application/json")
  //     .send(updatedPO)
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //       res.body.should.have
  //         .property("message")
  //         .eql("Please input a valid numeric value");
  //       done();
  //     });
  // });
  // it("It should delete a PO", done => {
  //   const POId = 14;
  //   chai
  //     .request(app)
  //     .delete(`/api/v1/POs/${POId}`)
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.OK);
  //       expect(res.body.data).to.include({});
  //       done();
  //     });
  // });
  // it("It should not delete a PO with invalid id", done => {
  //   const POId = 777;
  //   chai
  //     .request(app)
  //     .delete(`/api/v1/POs/${POId}`)
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //       res.body.should.have
  //         .property("message")
  //         .eql(`PO with the id ${POId} cannot be found`);
  //       done();
  //     });
  // });
  // it("It should not delete a PO with non-numeric id", done => {
  //   const POId = "bbb";
  //   chai
  //     .request(app)
  //     .delete(`/api/v1/POs/${POId}`)
  //     .set("Accept", "application/json")
  //     .end((err, res) => {
  //       expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //       res.body.should.have
  //         .property("message")
  //         .eql("Please provide a numeric value");
  //       done();
  //     });
  // });
});
//# sourceMappingURL=testPO.js.map