"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _chai = _interopRequireDefault(require("chai"));

var _chaiHttp = _interopRequireDefault(require("chai-http"));

var _chaiDatetime = _interopRequireDefault(require("chai-datetime"));

require("chai/register-should");

var _index = _interopRequireDefault(require("../index"));

var HTTP_CODE = require("../server/utils/HttpStatusCode");

_chai["default"].use(_chaiHttp["default"]);

_chai["default"].use(_chaiDatetime["default"]);

var expect = _chai["default"].expect;
describe("Testing the Customer endpoints:", function () {
  it("It should create a Customer", function (done) {
    var customer = {
      name: "Bridgestone LLC",
      address: "Land Plot CN3.6-CN4.1, Dinh Vu IZ, Dong Hai 2,Hai An, Hai Phong, Viet Nam",
      contact: "admin.btmv@bridgestone.com"
    }; // console.log(HTTP_CODE);

    _chai["default"].request(_index["default"]).post("/api/v1/Customers").set("Accept", "application/json").send(customer).end(function (err, res) {
      res.should.have.status(HTTP_CODE.CREATED);
      expect(res.body.data).to.include({
        name: customer.name,
        address: customer.address,
        contact: customer.contact
      });
      done();
    });
  });
  it("It should not create a Customer with incomplete parameters", function (done) {
    var customer = {
      name: customer.name,
      //   address: customer.address,
      contact: customer.contact
    };

    _chai["default"].request(_index["default"]).post("/api/v1/Customers").set("Accept", "application/json").send(customer).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      done();
    });
  });
  it("It should get all Customers", function (done) {
    _chai["default"].request(_index["default"]).get("/api/v1/Customers").set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(200);
      res.body.data[0].should.have.property("id");
      res.body.data[0].should.have.property("name");
      res.body.data[0].should.have.property("address");
      res.body.data[0].should.have.property("contact");
      done();
    });
  });
  it("It should get a particular Customer", function (done) {
    var customerID = "3";

    _chai["default"].request(_index["default"]).get("/api/v1/Customers/".concat(customerID)).set("Accept", "application/json").end(function (err, res) {
      // console.log("get a particular Customer: ", res.status);
      expect(res.status).to.equal(HTTP_CODE.OK);
      res.should.be.json;
      res.body.data.should.have.property("id");
      res.body.data[0].should.have.property("name");
      res.body.data[0].should.have.property("address");
      res.body.data[0].should.have.property("contact");
      done();
    });
  });
  it("It should not get a particular Customer with invalid id", function (done) {
    var customerID = 8888;

    _chai["default"].request(_index["default"]).get("/api/v1/Customers/".concat(customerID)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Cannot find Customer with the id ".concat(customerID));
      done();
    });
  });
  it("It should not get a particular Customer with non-numeric id", function (done) {
    var customerID = "aaa";

    _chai["default"].request(_index["default"]).get("/api/v1/Customers/".concat(customerID)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please input a valid numeric value");
      done();
    });
  });
  it("It should update a Customer", function (done) {
    var customerID = 5;
    var updatedCustomer = {
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/Customers/".concat(customerID)).set("Accept", "application/json").send(updatedCustomer).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.OK);
      expect(res.body.data.id).equal(updatedCustomer.id);
      expect(res.body.data.name).equal(updatedCustomer.name);
      expect(res.body.data.address).equal(updatedCustomer.address);
      expect(res.body.data.contact).equal(updatedCustomer.contact);
      done();
    });
  });
  it("It should not update a Customer with invalid id", function (done) {
    var customerID = "9999";
    var updatedCustomer = {
      id: customerID,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/Customers/".concat(customerID)).set("Accept", "application/json").send(updatedCustomer).end(function (err, res) {
      // console.log("res:", res.body);
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Cannot find Customer with the id: ".concat(customerID));
      done();
    });
  });
  it("It should not update a Customer with non-numeric id value", function (done) {
    var CustomerId = "ggg";
    var updatedCustomer = {
      id: CustomerId,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/Customers/".concat(CustomerId)).set("Accept", "application/json").send(updatedCustomer).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please input a valid numeric value");
      done();
    });
  });
  it("It should delete a Customer", function (done) {
    var CustomerId = 7;

    _chai["default"].request(_index["default"])["delete"]("/api/v1/Customers/".concat(CustomerId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.OK);
      expect(res.body.data).to.include({});
      done();
    });
  });
  it("It should not delete a Customer with invalid id", function (done) {
    var CustomerId = 777;

    _chai["default"].request(_index["default"])["delete"]("/api/v1/Customers/".concat(CustomerId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Customer with the id ".concat(CustomerId, " cannot be found"));
      done();
    });
  });
  it("It should not delete a Customer with non-numeric id", function (done) {
    var CustomerId = "bbb";

    _chai["default"].request(_index["default"])["delete"]("/api/v1/Customers/".concat(CustomerId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please provide a numeric value");
      done();
    });
  });
});
//# sourceMappingURL=testCustomer.js.map