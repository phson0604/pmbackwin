"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _chai = _interopRequireDefault(require("chai"));

var _chaiHttp = _interopRequireDefault(require("chai-http"));

require("chai/register-should");

var _index = _interopRequireDefault(require("../index"));

var HTTP_CODE = require("../server/utils/HttpStatusCode");

_chai["default"].use(_chaiHttp["default"]);

var expect = _chai["default"].expect;
describe("Testing the POItem endpoints:", function () {
  it("It should create a POItem", function (done) {
    var poDetail = {
      poNumber: "Second Test POItem",
      partNumber: "OP-0705",
      partname: "Dell Optiplex SFF",
      quantity: 4,
      unitPrice: 300,
      currency: "USD"
    }; // console.log(HTTP_CODE);

    _chai["default"].request(_index["default"]).post("/api/v1/POItems").set("Accept", "application/json").send(poDetail).end(function (err, res) {
      console.log("res:", res.body);
      res.should.have.status(HTTP_CODE.CREATED);
      expect(res.body.data).to.include({
        // id: 1,
        poNumber: poDetail.poNumber,
        partNumber: poDetail.partNumber,
        partname: poDetail.partname,
        quantity: poDetail.quantity,
        unitPrice: poDetail.unitPrice,
        currency: poDetail.currency
      });
      done();
    });
  });
  it("It should not create a POItem with incomplete parameters", function (done) {
    var poDetail = {
      poNumber: "Second Automation POItem",
      partNumber: "XDX-0101",
      quantity: 10,
      unitPrice: 2.1,
      currency: "USD"
    };

    _chai["default"].request(_index["default"]).post("/api/v1/POItems").set("Accept", "application/json").send(poDetail).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      done();
    });
  });
  it("It should get all POItems", function (done) {
    _chai["default"].request(_index["default"]).get("/api/v1/POItems").set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(200); // console.log("RequestReturn:", res.body.data);

      res.body.data[0].should.have.property("id");
      res.body.data[0].should.have.property("poNumber");
      res.body.data[0].should.have.property("partNumber");
      res.body.data[0].should.have.property("partname");
      res.body.data[0].should.have.property("quantity");
      res.body.data[0].should.have.property("unitPrice");
      res.body.data[0].should.have.property("currency");
      done();
    });
  });
  it("It should get a particular POItem", function (done) {
    var poID = "1";

    _chai["default"].request(_index["default"]).get("/api/v1/POItems/".concat(poID)).set("Accept", "application/json").end(function (err, res) {
      console.log("get a particular POItem: ", res.status, res.body);
      expect(res.status).to.equal(HTTP_CODE.OK);
      res.should.be.json;
      res.body.data.should.have.property("id");
      res.body.data.should.have.property("poNumber");
      res.body.data.should.have.property("partNumber");
      res.body.data.should.have.property("partname");
      res.body.data.should.have.property("quantity");
      res.body.data.should.have.property("unitPrice");
      res.body.data.should.have.property("currency");
      done();
    });
  });
  it("It should not get a particular POItem with invalid id", function (done) {
    var poID = 8888;

    _chai["default"].request(_index["default"]).get("/api/v1/POItems/".concat(poID)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Cannot find POItem with the id ".concat(poID));
      done();
    });
  });
  it("It should not get a particular POItem with non-numeric id", function (done) {
    var poID = "aaa";

    _chai["default"].request(_index["default"]).get("/api/v1/POItems/".concat(poID)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please input a valid numeric value");
      done();
    });
  });
  it("It should update a POItem", function (done) {
    var poID = 1;
    var updatedPOItem = {
      id: poID,
      poNumber: "Updated Awesome POItem",
      partNumber: "Updated partnumber",
      partname: "Updated partname",
      quantity: "101",
      unitPrice: 200000,
      currency: "VND"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/POItems/".concat(poID)).set("Accept", "application/json").send(updatedPOItem).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.OK);
      expect(res.body.data.id).equal(updatedPOItem.id);
      expect(res.body.data.poNumber).equal(updatedPOItem.poNumber);
      expect(res.body.data.PartNumer).equal(updatedPOItem.PartNumer);
      expect(res.body.data.partname).equal(updatedPOItem.partname);
      expect(res.body.data.quantity).equal(updatedPOItem.quantity);
      expect(res.body.data.unitPrice).equal(updatedPOItem.unitPrice);
      expect(res.body.data.currency).equal(updatedPOItem.currency);
      done();
    });
  });
  it("It should not update a POItem with invalid id", function (done) {
    var poID = "9999";
    var updatedPOItem = {
      id: poID,
      poNumber: "Updated Awesome POItem Again",
      partNumber: "Updated partnumber",
      partname: "Updated partname",
      quantity: "101",
      unitPrice: 1.1,
      currency: "USD"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/POItems/".concat(poID)).set("Accept", "application/json").send(updatedPOItem).end(function (err, res) {
      // console.log("res:", res.body);
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("Cannot find POItem with the id: ".concat(poID));
      done();
    });
  });
  it("It should not update a POItem with non-numeric id value", function (done) {
    var poID = "ggg";
    var updatedPO = {
      id: poID,
      poNumber: "Updated Awesome POItem Again",
      partNumber: "Updated partnumber",
      partname: "Updated partname",
      quantity: "101",
      unitPrice: 1.1,
      currency: "USD"
    };

    _chai["default"].request(_index["default"]).put("/api/v1/POItems/".concat(poID)).set("Accept", "application/json").send(updatedPO).end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please input a valid numeric value");
      done();
    });
  });
  it("It should delete a POItem", function (done) {
    var POId = 2;

    _chai["default"].request(_index["default"])["delete"]("/api/v1/POItems/".concat(POId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.OK);
      expect(res.body.data).to.include({});
      done();
    });
  });
  it("It should not delete a POItem with invalid id", function (done) {
    var POId = 777;

    _chai["default"].request(_index["default"])["delete"]("/api/v1/POItems/".concat(POId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
      res.body.should.have.property("message").eql("POItem with the id ".concat(POId, " cannot be found"));
      done();
    });
  });
  it("It should not delete a POItem with non-numeric id", function (done) {
    var POId = "bbb";

    _chai["default"].request(_index["default"])["delete"]("/api/v1/POItems/".concat(POId)).set("Accept", "application/json").end(function (err, res) {
      expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
      res.body.should.have.property("message").eql("Please provide a numeric value");
      done();
    });
  });
});
//# sourceMappingURL=testPODetail.js.map