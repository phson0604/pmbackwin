"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _chai = _interopRequireDefault(require("chai"));

var _chaiHttp = _interopRequireDefault(require("chai-http"));

var _chaiDatetime = _interopRequireDefault(require("chai-datetime"));

require("chai/register-should");

var _index = _interopRequireDefault(require("../index"));

var HTTP_CODE = require("../server/utils/HttpStatusCode");

_chai["default"].use(_chaiHttp["default"]);

_chai["default"].use(_chaiDatetime["default"]);

var expect = _chai["default"].expect;
describe("Testing the QuotationDetail endpoints:", function () {
  it("It should create a QuotationDetail", function (done) {
    var quotation = {
      quotationID: 1,
      partNumber: "93074RX",
      partname: "Rack & PDU",
      Manufacture: "Lenovo",
      quantity: 1,
      unitPrice: 2400,
      currency: "USD"
    };

    _chai["default"].request(_index["default"]).post("/api/v1/QuotationDetails").set("Accept", "application/json").send(quotation).end(function (err, res) {
      res.should.have.status(HTTP_CODE.CREATED);
      expect(res.body.data).to.include({
        // id: 1,
        quotationID: quotation.quotationID,
        partNumber: quotation.partNumber,
        partname: quotation.partname,
        Manufacture: quotation.Manufacture,
        quantity: quotation.quantity,
        unitPrice: quotation.unitPrice,
        currency: quotation.currency
      });
      done();
    });
  }); //   it("It should not create a QuotationDetail with incomplete parameters", done => {
  //     const quotation = {
  //       quotationID: "Second QuotationDetail",
  //       receivedDate: Date.now()
  //       //   validUntil: Date.now()
  //     };
  //     chai
  //       .request(app)
  //       .post("/api/v1/QuotationDetails")
  //       .set("Accept", "application/json")
  //       .send(quotation)
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         done();
  //       });
  //   });
  //   it("It should get all QuotationDetails", done => {
  //     chai
  //       .request(app)
  //       .get("/api/v1/QuotationDetails")
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(200);
  //         // console.log("RequestReturn:", res.body.data);
  //         res.body.data[0].should.have.property("id");
  //         res.body.data[0].should.have.property("quotationID");
  //         res.body.data[0].should.have.property("partNumber");
  //         res.body.data[0].should.have.property("receivedDate");
  //         res.body.data[0].should.have.property("validUntil");
  //         done();
  //       });
  //   });
  //   it("It should get a particular QuotationDetail", done => {
  //     const quotationID = "3";
  //     chai
  //       .request(app)
  //       .get(`/api/v1/QuotationDetails/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         // console.log("get a particular QuotationDetail: ", res.status);
  //         expect(res.status).to.equal(HTTP_CODE.OK);
  //         res.should.be.json;
  //         res.body.data.should.have.property("id");
  //         res.body.data.should.have.property("quotationID");
  //         res.body.data.should.have.property("partNumber");
  //         res.body.data.should.have.property("receivedDate");
  //         res.body.data.should.have.property("validUntil");
  //         done();
  //       });
  //   });
  //   it("It should not get a particular QuotationDetail with invalid id", done => {
  //     const quotationID = 8888;
  //     chai
  //       .request(app)
  //       .get(`/api/v1/QuotationDetails/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //         res.body.should.have
  //           .property("message")
  //           .eql(`Cannot find QuotationDetail with the id ${quotationID}`);
  //         done();
  //       });
  //   });
  //   it("It should not get a particular QuotationDetail with non-numeric id", done => {
  //     const quotationID = "aaa";
  //     chai
  //       .request(app)
  //       .get(`/api/v1/QuotationDetails/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         res.body.should.have
  //           .property("message")
  //           .eql("Please input a valid numeric value");
  //         done();
  //       });
  //   });
  //   it("It should update a QuotationDetail", done => {
  //     const quotationID = 5;
  //     const updatedQuotationDetail = {
  //       id: quotationID,
  //       quotationID: "Updated ten QuotationDetail",
  //       partNumber: 10,
  //       receivedDate: "1/1/2009",
  //       validUntil: "1/1/2020"
  //     };
  //     chai
  //       .request(app)
  //       .put(`/api/v1/QuotationDetails/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .send(updatedQuotationDetail)
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.OK);
  //         expect(res.body.data.id).equal(updatedQuotationDetail.id);
  //         expect(res.body.data.quotationID).equal(updatedQuotationDetail.quotationID);
  //         expect(res.body.data.partNumber).equal(updatedQuotationDetail.partNumber);
  //         expect(res.body.data.receivedDate).equal(updatedQuotationDetail.receivedDate);
  //         expect(res.body.data.validUntil).equal(updatedQuotationDetail.validUntil);
  //         done();
  //       });
  //   });
  //   it("It should not update a QuotationDetail with invalid id", done => {
  //     const quotationID = "9999";
  //     const updatedQuotationDetail = {
  //       id: quotationID,
  //       quotationID: "Updated Awesome QuotationDetail",
  //       partNumber: 2,
  //       receivedDate: "1/1/2019",
  //       validUntil: "1/1/2020"
  //     };
  //     chai
  //       .request(app)
  //       .put(`/api/v1/QuotationDetails/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .send(updatedQuotationDetail)
  //       .end((err, res) => {
  //         // console.log("res:", res.body);
  //         expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //         res.body.should.have
  //           .property("message")
  //           .eql(`Cannot find QuotationDetail with the id: ${quotationID}`);
  //         done();
  //       });
  //   });
  //   it("It should not update a QuotationDetail with non-numeric id value", done => {
  //     const QuotationDetailId = "ggg";
  //     const updatedQuotationDetail = {
  //       id: QuotationDetailId,
  //       quotationID: "Updated Awesome QuotationDetail again",
  //       partNumber: "1",
  //       receivedDate: "1/1/1999",
  //       validUntil: "1/1/1999"
  //     };
  //     chai
  //       .request(app)
  //       .put(`/api/v1/QuotationDetails/${QuotationDetailId}`)
  //       .set("Accept", "application/json")
  //       .send(updatedQuotationDetail)
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         res.body.should.have
  //           .property("message")
  //           .eql("Please input a valid numeric value");
  //         done();
  //       });
  //   });
  //   it("It should delete a QuotationDetail", done => {
  //     const QuotationDetailId = 7;
  //     chai
  //       .request(app)
  //       .delete(`/api/v1/QuotationDetails/${QuotationDetailId}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.OK);
  //         expect(res.body.data).to.include({});
  //         done();
  //       });
  //   });
  //   it("It should not delete a QuotationDetail with invalid id", done => {
  //     const QuotationDetailId = 777;
  //     chai
  //       .request(app)
  //       .delete(`/api/v1/QuotationDetails/${QuotationDetailId}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //         res.body.should.have
  //           .property("message")
  //           .eql(`QuotationDetail with the id ${QuotationDetailId} cannot be found`);
  //         done();
  //       });
  //   });
  //   it("It should not delete a QuotationDetail with non-numeric id", done => {
  //     const QuotationDetailId = "bbb";
  //     chai
  //       .request(app)
  //       .delete(`/api/v1/QuotationDetails/${QuotationDetailId}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         res.body.should.have
  //           .property("message")
  //           .eql("Please provide a numeric value");
  //         done();
  //       });
  //   });
});
//# sourceMappingURL=testQuotationDetail.js.map