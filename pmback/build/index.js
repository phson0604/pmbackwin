"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _QuotationRouters = _interopRequireDefault(require("./server/routers/QuotationRouters"));

var _QuotationItemRouters = _interopRequireDefault(require("./server/routers/QuotationItemRouters"));

var _CustomerRouters = _interopRequireDefault(require("./server/routers/CustomerRouters"));

var _SupplierRouters = _interopRequireDefault(require("./server/routers/SupplierRouters"));

var _UserRouters = _interopRequireDefault(require("./server/routers/UserRouters"));

var _RoleRouters = _interopRequireDefault(require("./server/routers/RoleRouters"));

var _PORouters = _interopRequireDefault(require("./server/routers/PORouters"));

var _POItemRouters = _interopRequireDefault(require("./server/routers/POItemRouters"));

var _POStepMasterRouters = _interopRequireDefault(require("./server/routers/POStepMasterRouters"));

var _POStatusMasterRouters = _interopRequireDefault(require("./server/routers/POStatusMasterRouters"));

var _POStepRouters = _interopRequireDefault(require("./server/routers/POStepRouters"));

var _POPICRouters = _interopRequireDefault(require("./server/routers/POPICRouters"));

var _AuthRouters = _interopRequireDefault(require("./server/routers/AuthRouters"));

// import express from "express";
// import cors from "./server/middleware/cors";
var express = require("express");

var app = express();

require("./server/startup/prod")(app); // console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
// console.log(`app: ${app.get("env")}`);


var helmet = require("helmet");

app.use(helmet());

var cors = require("cors");

app.use(cors());
app.use(_bodyParser["default"].json());
app.use(_bodyParser["default"].urlencoded({
  extended: false
}));

if (app.get("env") === "development") {
  var morgan = require("morgan");

  app.use(morgan("tiny"));
}

app.use("/api/v1/POs", _PORouters["default"]);
app.use("/api/v1/POPICs", _POPICRouters["default"]);
app.use("/api/v1/POItems", _POItemRouters["default"]);
app.use("/api/v1/Quotations", _QuotationRouters["default"]);
app.use("/api/v1/QuotationItems", _QuotationItemRouters["default"]);
app.use("/api/v1/Customers", _CustomerRouters["default"]);
app.use("/api/v1/Suppliers", _SupplierRouters["default"]);
app.use("/api/v1/POStepMasters", _POStepMasterRouters["default"]);
app.use("/api/v1/POStatusMasters", _POStatusMasterRouters["default"]);
app.use("/api/v1/POSteps", _POStepRouters["default"]);
app.use("/api/v1/Users", _UserRouters["default"]);
app.use("/api/v1/Roles", _RoleRouters["default"]);
app.use("/api/v1/Auth", _AuthRouters["default"]);
app.get("*", function (req, res) {
  return res.status(200).send({
    message: "Welcome/Xin chao/Ciao to this API."
  });
});
var port = process.env.PORT || 8000;
app.listen(port, function () {
  console.log("Server is running on PORT ".concat(port));
});
var _default = app;
exports["default"] = _default;
//# sourceMappingURL=index.js.map