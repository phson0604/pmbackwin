"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _POStepController = _interopRequireDefault(require("../controllers/POStepController"));

var router = (0, _express.Router)();
router.get("/", _POStepController["default"].getAllPOSteps);
router.post("/", _POStepController["default"].addPOStep);
router.get("/:id", _POStepController["default"].getAPOStep);
router.put("/:id", _POStepController["default"].updatedPOStep);
router["delete"]("/:id", _POStepController["default"].deletePOStep);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=POStepRouters.js.map