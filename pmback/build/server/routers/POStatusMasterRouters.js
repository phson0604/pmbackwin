"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _POStatusMasterController = _interopRequireDefault(require("../controllers/POStatusMasterController"));

var router = (0, _express.Router)();
router.get("/", _POStatusMasterController["default"].getAllPOStatusMasters);
router.post("/", _POStatusMasterController["default"].addPOStatusMaster);
router.get("/:id", _POStatusMasterController["default"].getAPOStatusMaster);
router.put("/:id", _POStatusMasterController["default"].updatedPOStatusMaster);
router["delete"]("/:id", _POStatusMasterController["default"].deletePOStatusMaster);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=POStatusMasterRouters.js.map