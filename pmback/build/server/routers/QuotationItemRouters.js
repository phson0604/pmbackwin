"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _QuotationItemController = _interopRequireDefault(require("../controllers/QuotationItemController"));

var router = (0, _express.Router)();
router.get("/", _QuotationItemController["default"].getAllQuotationItems);
router.post("/", _QuotationItemController["default"].addQuotationItem);
router.get("/:id", _QuotationItemController["default"].getAQuotationItem);
router.put("/:id", _QuotationItemController["default"].updatedQuotationItem);
router["delete"]("/:id", _QuotationItemController["default"].deleteQuotationItem);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=QuotationItemRouters.js.map