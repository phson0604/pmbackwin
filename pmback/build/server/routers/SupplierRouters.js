"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _SupplierController = _interopRequireDefault(require("../controllers/SupplierController"));

var router = (0, _express.Router)();
router.get("/", _SupplierController["default"].getAllSuppliers);
router.post("/", _SupplierController["default"].addSupplier);
router.get("/:id", _SupplierController["default"].getASupplier);
router.put("/:id", _SupplierController["default"].updatedSupplier);
router["delete"]("/:id", _SupplierController["default"].deleteSupplier);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=SupplierRouters.js.map