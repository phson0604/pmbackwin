"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _POItemController = _interopRequireDefault(require("../controllers/POItemController"));

var router = (0, _express.Router)();
router.get("/", _POItemController["default"].getAllPOItems);
router.post("/", _POItemController["default"].addPOItem);
router.get("/:id", _POItemController["default"].getAPOItem);
router.put("/:id", _POItemController["default"].updatedPOItem);
router["delete"]("/:id", _POItemController["default"].deletePOItem);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=POItemRouters.js.map