"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _POController = _interopRequireDefault(require("../controllers/POController"));

var router = (0, _express.Router)(); // console.log("PO Router called");

router.get("/", _POController["default"].getAllPOs);
router.post("/", _POController["default"].addPO);
router.get("/:id", _POController["default"].getAPO);
router.put("/:id", _POController["default"].updatedPO);
router["delete"]("/:id", _POController["default"].deletePO);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=PORouters.js.map