"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _UserController = _interopRequireDefault(require("../controllers/UserController"));

var auth = require("../middleware/auth");

var router = (0, _express.Router)();
router.get("/", auth, _UserController["default"].getAllUsers);
router.post("/", auth, _UserController["default"].addUser);
router.get("/:id", auth, _UserController["default"].getAUser);
router.put("/:id", auth, _UserController["default"].updatedUser);
router["delete"]("/:id", auth, _UserController["default"].deleteUser);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=UserRouters.js.map