"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _POPICController = _interopRequireDefault(require("../controllers/POPICController"));

var router = (0, _express.Router)();
router.get("/", _POPICController["default"].getAllPOPICs); //with parameter supported

router.get("/:id", _POPICController["default"].getAPOPIC);
router.post("/", _POPICController["default"].addPOPIC);
router.put("/:id", _POPICController["default"].updatedPOPIC);
router["delete"]("/:id", _POPICController["default"].deletePOPIC);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=POPICRouters.js.map