"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _QuotationController = _interopRequireDefault(require("./../controllers/QuotationController"));

var router = (0, _express.Router)();
router.get("/", _QuotationController["default"].getAllQuotations);
router.post("/", _QuotationController["default"].addQuotation);
router.get("/:id", _QuotationController["default"].getAQuotation);
router.put("/:id", _QuotationController["default"].updatedQuotation);
router["delete"]("/:id", _QuotationController["default"].deleteQuotation);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=QuotationRouters.js.map