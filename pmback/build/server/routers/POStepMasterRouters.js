"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _POStepMasterController = _interopRequireDefault(require("../controllers/POStepMasterController"));

var router = (0, _express.Router)();
router.get("/", _POStepMasterController["default"].getAllPOStepMasters);
router.post("/", _POStepMasterController["default"].addPOStepMaster);
router.get("/:id", _POStepMasterController["default"].getAPOStepMaster);
router.put("/:id", _POStepMasterController["default"].updatedPOStepMaster);
router["delete"]("/:id", _POStepMasterController["default"].deletePOStepMaster);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=POStepMasterRouters.js.map