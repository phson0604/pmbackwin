"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _CustomerController = _interopRequireDefault(require("../controllers/CustomerController"));

var router = (0, _express.Router)();
router.get("/", _CustomerController["default"].getAllCustomers);
router.post("/", _CustomerController["default"].addCustomer);
router.get("/:id", _CustomerController["default"].getACustomer);
router.put("/:id", _CustomerController["default"].updatedCustomer);
router["delete"]("/:id", _CustomerController["default"].deleteCustomer);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=CustomerRouters.js.map