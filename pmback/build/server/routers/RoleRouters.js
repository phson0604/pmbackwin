"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _express = require("express");

var _RoleController = _interopRequireDefault(require("../controllers/RoleController"));

var router = (0, _express.Router)();
router.get("/", _RoleController["default"].getAllRoles);
router.post("/", _RoleController["default"].addRole);
router.get("/:id", _RoleController["default"].getARole);
router.put("/:id", _RoleController["default"].updatedRole);
router["delete"]("/:id", _RoleController["default"].deleteRole);
var _default = router;
exports["default"] = _default;
//# sourceMappingURL=RoleRouters.js.map