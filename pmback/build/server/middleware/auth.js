"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _config = _interopRequireDefault(require("../src/config/config"));

var jwt = require("jsonwebtoken");

var env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
var config = _config["default"][env];
var jwtPrivateKey = env === "production" ? process.env["jwtPrivateKey"] : config.jwtPrivateKey;

module.exports = function (req, res, next) {
  //bypass authentication  for development environment
  //if (env === "development")
  return next();
  var token = req.header("x-auth-token");
  if (!token) return res.status(401).send("Access denied. No token provided.");

  try {
    var decoded = jwt.verify(token, jwtPrivateKey);
    req.user = decoded;
    next();
  } catch (ex) {
    res.status(400).send("Invalid token.");
  }
};
//# sourceMappingURL=auth.js.map