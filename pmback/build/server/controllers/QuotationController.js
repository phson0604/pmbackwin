"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _QuotationService = _interopRequireDefault(require("../services/QuotationService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var _QuotationItemService = _interopRequireDefault(require("./../services/QuotationItemService"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var QuotationController = /*#__PURE__*/function () {
  function QuotationController() {
    (0, _classCallCheck2["default"])(this, QuotationController);
  }

  (0, _createClass2["default"])(QuotationController, null, [{
    key: "getAllQuotations",
    value: function () {
      var _getAllQuotations = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allQuotations;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _QuotationService["default"].getAllQuotations(req.query);

              case 3:
                allQuotations = _context.sent;

                if (allQuotations.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "Quotations retrieved", allQuotations);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No Quotation found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllQuotations(_x, _x2) {
        return _getAllQuotations.apply(this, arguments);
      }

      return getAllQuotations;
    }()
  }, {
    key: "addQuotation",
    value: function () {
      var _addQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newQuotation, createdQuotation;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.poID || !req.body.supplierID || !req.body.issuedDate || !req.body.validUntil)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newQuotation = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _QuotationService["default"].addQuotation(newQuotation);

              case 7:
                createdQuotation = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "Quotation Added!", createdQuotation);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addQuotation(_x3, _x4) {
        return _addQuotation.apply(this, arguments);
      }

      return addQuotation;
    }()
  }, {
    key: "updatedQuotation",
    value: function () {
      var _updatedQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredQuotation, id, updateQuotation;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredQuotation = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _QuotationService["default"].updateQuotation(id, alteredQuotation);

              case 8:
                updateQuotation = _context3.sent;

                if (!updateQuotation) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Quotation with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Quotation updated", updateQuotation);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedQuotation(_x5, _x6) {
        return _updatedQuotation.apply(this, arguments);
      }

      return updatedQuotation;
    }()
  }, {
    key: "getAQuotation",
    value: function () {
      var _getAQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, theQuotation;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _QuotationService["default"].getAQuotation(id);

              case 7:
                theQuotation = _context4.sent;

                if (!theQuotation) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Quotation with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found Quotation", theQuotation);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getAQuotation(_x7, _x8) {
        return _getAQuotation.apply(this, arguments);
      }

      return getAQuotation;
    }()
  }, {
    key: "deleteQuotation",
    value: function () {
      var _deleteQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, quotationToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _QuotationService["default"].deleteQuotation(id);

              case 7:
                quotationToDelete = _context5.sent;

                if (!quotationToDelete) {
                  _context5.next = 14;
                  break;
                }

                _context5.next = 11;
                return _QuotationItemService["default"].deleteAllItemOfQuotation(id);

              case 11:
                util.setSuccess(HTTP_CODE.OK, "Quotation deleted");
                _context5.next = 15;
                break;

              case 14:
                util.setError(HTTP_CODE.NOT_FOUND, "Quotation with the id ".concat(id, " cannot be found"));

              case 15:
                return _context5.abrupt("return", util.send(res));

              case 18:
                _context5.prev = 18;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 22:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 18]]);
      }));

      function deleteQuotation(_x9, _x10) {
        return _deleteQuotation.apply(this, arguments);
      }

      return deleteQuotation;
    }()
  }]);
  return QuotationController;
}();

var _default = QuotationController;
exports["default"] = _default;
//# sourceMappingURL=QuotationController.js.map