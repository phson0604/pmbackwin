"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _CustomerService = _interopRequireDefault(require("../services/CustomerService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var CustomerController = /*#__PURE__*/function () {
  function CustomerController() {
    (0, _classCallCheck2["default"])(this, CustomerController);
  }

  (0, _createClass2["default"])(CustomerController, null, [{
    key: "getAllCustomers",
    value: function () {
      var _getAllCustomers = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allCustomers;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _CustomerService["default"].getAllCustomers();

              case 3:
                allCustomers = _context.sent;

                if (allCustomers.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "Customers retrieved", allCustomers);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No Customer found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllCustomers(_x, _x2) {
        return _getAllCustomers.apply(this, arguments);
      }

      return getAllCustomers;
    }()
  }, {
    key: "addCustomer",
    value: function () {
      var _addCustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newCustomer, createdCustomer;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.name || !req.body.address || !req.body.contact)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newCustomer = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _CustomerService["default"].addCustomer(newCustomer);

              case 7:
                createdCustomer = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "Customer Added!", createdCustomer);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addCustomer(_x3, _x4) {
        return _addCustomer.apply(this, arguments);
      }

      return addCustomer;
    }()
  }, {
    key: "updatedCustomer",
    value: function () {
      var _updatedCustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredCustomer, id, updateCustomer;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredCustomer = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _CustomerService["default"].updateCustomer(id, alteredCustomer);

              case 8:
                updateCustomer = _context3.sent;

                if (!updateCustomer) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Customer with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Customer updated", updateCustomer);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedCustomer(_x5, _x6) {
        return _updatedCustomer.apply(this, arguments);
      }

      return updatedCustomer;
    }()
  }, {
    key: "getACustomer",
    value: function () {
      var _getACustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, theCustomer;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _CustomerService["default"].getACustomer(id);

              case 7:
                theCustomer = _context4.sent;

                if (!theCustomer) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Customer with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found Customer", theCustomer);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getACustomer(_x7, _x8) {
        return _getACustomer.apply(this, arguments);
      }

      return getACustomer;
    }()
  }, {
    key: "deleteCustomer",
    value: function () {
      var _deleteCustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, customerToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _CustomerService["default"].deleteCustomer(id);

              case 7:
                customerToDelete = _context5.sent;

                if (customerToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "Customer deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "Customer with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deleteCustomer(_x9, _x10) {
        return _deleteCustomer.apply(this, arguments);
      }

      return deleteCustomer;
    }()
  }]);
  return CustomerController;
}();

var _default = CustomerController;
exports["default"] = _default;
//# sourceMappingURL=CustomerController.js.map