"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _POItemService = _interopRequireDefault(require("../services/POItemService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var POItemController = /*#__PURE__*/function () {
  function POItemController() {
    (0, _classCallCheck2["default"])(this, POItemController);
  }

  (0, _createClass2["default"])(POItemController, null, [{
    key: "getAllPOItems",
    value: function () {
      var _getAllPOItems = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allPOItems;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                console.log("POItem query:", req.query);
                _context.prev = 1;
                _context.next = 4;
                return _POItemService["default"].getAllPOItems(req.query);

              case 4:
                allPOItems = _context.sent;

                if (allPOItems.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "POItems retrieved", allPOItems);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No POItem found");
                }

                return _context.abrupt("return", util.send(res));

              case 9:
                _context.prev = 9;
                _context.t0 = _context["catch"](1);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 13:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 9]]);
      }));

      function getAllPOItems(_x, _x2) {
        return _getAllPOItems.apply(this, arguments);
      }

      return getAllPOItems;
    }()
  }, {
    key: "addPOItem",
    value: function () {
      var _addPOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newPOItem, createdPOItem;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.poID || !req.body.partNumber || !req.body.partName || !req.body.quantity || !req.body.unitPrice || !req.body.currency)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newPOItem = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _POItemService["default"].addPOItem(newPOItem);

              case 7:
                createdPOItem = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "POItem Added!", createdPOItem);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addPOItem(_x3, _x4) {
        return _addPOItem.apply(this, arguments);
      }

      return addPOItem;
    }()
  }, {
    key: "updatedPOItem",
    value: function () {
      var _updatedPOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredPOItem, id, updatePOItem;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredPOItem = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _POItemService["default"].updatePOItem(id, alteredPOItem);

              case 8:
                updatePOItem = _context3.sent;

                if (!updatePOItem) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POItem with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "POItem updated", updatePOItem);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedPOItem(_x5, _x6) {
        return _updatedPOItem.apply(this, arguments);
      }

      return updatedPOItem;
    }()
  }, {
    key: "getAPOItem",
    value: function () {
      var _getAPOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, thePOItem;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _POItemService["default"].getAPOItem(id);

              case 7:
                thePOItem = _context4.sent;

                if (!thePOItem) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POItem with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found POItem", thePOItem);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getAPOItem(_x7, _x8) {
        return _getAPOItem.apply(this, arguments);
      }

      return getAPOItem;
    }()
  }, {
    key: "deletePOItem",
    value: function () {
      var _deletePOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, poToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _POItemService["default"].deletePOItem(id);

              case 7:
                poToDelete = _context5.sent;

                if (poToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "POItem deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "POItem with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deletePOItem(_x9, _x10) {
        return _deletePOItem.apply(this, arguments);
      }

      return deletePOItem;
    }()
  }]);
  return POItemController;
}();

var _default = POItemController;
exports["default"] = _default;
//# sourceMappingURL=POItemController.js.map