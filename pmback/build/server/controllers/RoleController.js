"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _RoleService = _interopRequireDefault(require("../services/RoleService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var RoleController = /*#__PURE__*/function () {
  function RoleController() {
    (0, _classCallCheck2["default"])(this, RoleController);
  }

  (0, _createClass2["default"])(RoleController, null, [{
    key: "getAllRoles",
    value: function () {
      var _getAllRoles = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allRoles;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _RoleService["default"].getAllRoles();

              case 3:
                allRoles = _context.sent;

                if (allRoles.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "Roles retrieved", allRoles);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No Role found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllRoles(_x, _x2) {
        return _getAllRoles.apply(this, arguments);
      }

      return getAllRoles;
    }()
  }, {
    key: "addRole",
    value: function () {
      var _addRole = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newRole, createdRole;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.scope || !req.body.role || !req.body.description)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newRole = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _RoleService["default"].addRole(newRole);

              case 7:
                createdRole = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "Role Added!", createdRole);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addRole(_x3, _x4) {
        return _addRole.apply(this, arguments);
      }

      return addRole;
    }()
  }, {
    key: "updatedRole",
    value: function () {
      var _updatedRole = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredRole, id, updateRole;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredRole = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _RoleService["default"].updateRole(id, alteredRole);

              case 8:
                updateRole = _context3.sent;

                if (!updateRole) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Role with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Role updated", updateRole);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedRole(_x5, _x6) {
        return _updatedRole.apply(this, arguments);
      }

      return updatedRole;
    }()
  }, {
    key: "getARole",
    value: function () {
      var _getARole = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, theRole;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _RoleService["default"].getARole(id);

              case 7:
                theRole = _context4.sent;

                if (!theRole) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Role with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found Role", theRole);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getARole(_x7, _x8) {
        return _getARole.apply(this, arguments);
      }

      return getARole;
    }()
  }, {
    key: "deleteRole",
    value: function () {
      var _deleteRole = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, userToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _RoleService["default"].deleteRole(id);

              case 7:
                userToDelete = _context5.sent;

                if (userToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "Role deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "Role with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deleteRole(_x9, _x10) {
        return _deleteRole.apply(this, arguments);
      }

      return deleteRole;
    }()
  }]);
  return RoleController;
}();

var _default = RoleController;
exports["default"] = _default;
//# sourceMappingURL=RoleController.js.map