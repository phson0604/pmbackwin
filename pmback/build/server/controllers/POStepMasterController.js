"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _POStepMasterService = _interopRequireDefault(require("../services/POStepMasterService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var POStepMasterController = /*#__PURE__*/function () {
  function POStepMasterController() {
    (0, _classCallCheck2["default"])(this, POStepMasterController);
  }

  (0, _createClass2["default"])(POStepMasterController, null, [{
    key: "getAllPOStepMasters",
    value: function () {
      var _getAllPOStepMasters = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allPOStepMasters;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _POStepMasterService["default"].getAllPOStepMasters();

              case 3:
                allPOStepMasters = _context.sent;

                if (allPOStepMasters.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "POStepMasters retrieved", allPOStepMasters);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No POStepMaster found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllPOStepMasters(_x, _x2) {
        return _getAllPOStepMasters.apply(this, arguments);
      }

      return getAllPOStepMasters;
    }()
  }, {
    key: "addPOStepMaster",
    value: function () {
      var _addPOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newPOStepMaster, createdPOStepMaster;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.sequence || !req.body.stepName)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newPOStepMaster = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _POStepMasterService["default"].addPOStepMaster(newPOStepMaster);

              case 7:
                createdPOStepMaster = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "POStepMaster Added!", createdPOStepMaster);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addPOStepMaster(_x3, _x4) {
        return _addPOStepMaster.apply(this, arguments);
      }

      return addPOStepMaster;
    }()
  }, {
    key: "updatedPOStepMaster",
    value: function () {
      var _updatedPOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredPOStepMaster, id, updatePOStepMaster;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredPOStepMaster = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _POStepMasterService["default"].updatePOStepMaster(id, alteredPOStepMaster);

              case 8:
                updatePOStepMaster = _context3.sent;

                if (!updatePOStepMaster) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POStepMaster with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "POStepMaster updated", updatePOStepMaster);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedPOStepMaster(_x5, _x6) {
        return _updatedPOStepMaster.apply(this, arguments);
      }

      return updatedPOStepMaster;
    }()
  }, {
    key: "getAPOStepMaster",
    value: function () {
      var _getAPOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, thePOStepMaster;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _POStepMasterService["default"].getAPOStepMaster(id);

              case 7:
                thePOStepMaster = _context4.sent;

                if (!thePOStepMaster) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POStepMaster with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found POStepMaster", thePOStepMaster);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getAPOStepMaster(_x7, _x8) {
        return _getAPOStepMaster.apply(this, arguments);
      }

      return getAPOStepMaster;
    }()
  }, {
    key: "deletePOStepMaster",
    value: function () {
      var _deletePOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, quotationToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _POStepMasterService["default"].deletePOStepMaster(id);

              case 7:
                quotationToDelete = _context5.sent;

                if (quotationToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "POStepMaster deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "POStepMaster with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deletePOStepMaster(_x9, _x10) {
        return _deletePOStepMaster.apply(this, arguments);
      }

      return deletePOStepMaster;
    }()
  }]);
  return POStepMasterController;
}();

var _default = POStepMasterController;
exports["default"] = _default;
//# sourceMappingURL=POStepMasterController.js.map