"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _UserService = _interopRequireDefault(require("../services/UserService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var _config = _interopRequireDefault(require("../src/config/config"));

var jwt = require("jsonwebtoken");

var _ = require("lodash");

var util = new _Utils["default"]();

var HTTP_CODE = require("../utils/HttpStatusCode");

var env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
var config = _config["default"][env];
var jwtPrivateKey = env === "production" ? process.env["jwtPrivateKey"] : config.jwtPrivateKey; //avoid return password

var RETURNABLE_COLUMNS = ["id", "uuid", "email", "displayName", "tel", "address", "lastLogin"];

var UserController = /*#__PURE__*/function () {
  function UserController() {
    (0, _classCallCheck2["default"])(this, UserController);
  }

  (0, _createClass2["default"])(UserController, null, [{
    key: "getAllUsers",
    value: function () {
      var _getAllUsers = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allUsers;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _UserService["default"].getAllUsers();

              case 3:
                allUsers = _context.sent;

                if (allUsers.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "Users retrieved", _.map(allUsers, _.partialRight(_.pick, RETURNABLE_COLUMNS)));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No User found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllUsers(_x, _x2) {
        return _getAllUsers.apply(this, arguments);
      }

      return getAllUsers;
    }()
  }, {
    key: "getAUser",
    value: function () {
      var _getAUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var id, theUser;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context2.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context2.abrupt("return", util.send(res));

              case 4:
                _context2.prev = 4;
                _context2.next = 7;
                return _UserService["default"].getAUser(id);

              case 7:
                theUser = _context2.sent;

                if (!theUser) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find User with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found User", _.pick(theUser, RETURNABLE_COLUMNS));
                }

                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context2.t0);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function getAUser(_x3, _x4) {
        return _getAUser.apply(this, arguments);
      }

      return getAUser;
    }()
  }, {
    key: "addUser",
    value: function () {
      var _addUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var newUser, createdUser, jwtToken;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(!req.body.email || !req.body.displayName || !req.body.password)) {
                  _context3.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context3.abrupt("return", util.send(res));

              case 3:
                newUser = req.body; //Check if user registered
                //  (should await here because UserService return a promise)

                _context3.next = 6;
                return _UserService["default"].findUser(newUser);

              case 6:
                if (!_context3.sent) {
                  _context3.next = 9;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "User already registered");
                return _context3.abrupt("return", util.send(res));

              case 9:
                _context3.prev = 9;
                _context3.next = 12;
                return _UserService["default"].addUser(newUser);

              case 12:
                createdUser = _context3.sent;
                // generate jwt token
                jwtToken = jwt.sign({
                  name: newUser.displayName,
                  email: newUser.email
                }, jwtPrivateKey);
                util.setSuccess(HTTP_CODE.CREATED, "User Added!", _.pick(createdUser, RETURNABLE_COLUMNS)); //send jwt to client as in response header

                return _context3.abrupt("return", util.send(res.header("x-auth-token", jwtToken)));

              case 18:
                _context3.prev = 18;
                _context3.t0 = _context3["catch"](9);
                util.setError(HTTP_CODE.BAD_REQUEST, _context3.t0.message);
                return _context3.abrupt("return", util.send(res));

              case 22:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[9, 18]]);
      }));

      function addUser(_x5, _x6) {
        return _addUser.apply(this, arguments);
      }

      return addUser;
    }()
  }, {
    key: "updatedUser",
    value: function () {
      var _updatedUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var alteredUser, id, updateUser;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                alteredUser = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 5:
                _context4.prev = 5;
                _context4.next = 8;
                return _UserService["default"].updateUser(id, alteredUser);

              case 8:
                updateUser = _context4.sent;

                if (!updateUser) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find User with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "User updated", updateUser);
                }

                return _context4.abrupt("return", util.send(res));

              case 13:
                _context4.prev = 13;
                _context4.t0 = _context4["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[5, 13]]);
      }));

      function updatedUser(_x7, _x8) {
        return _updatedUser.apply(this, arguments);
      }

      return updatedUser;
    }()
  }, {
    key: "deleteUser",
    value: function () {
      var _deleteUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, userToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _UserService["default"].deleteUser(id);

              case 7:
                userToDelete = _context5.sent;

                if (userToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "User deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "User with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deleteUser(_x9, _x10) {
        return _deleteUser.apply(this, arguments);
      }

      return deleteUser;
    }()
  }]);
  return UserController;
}();

var _default = UserController;
exports["default"] = _default;
//# sourceMappingURL=UserController.js.map