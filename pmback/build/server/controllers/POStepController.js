"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _POStepService = _interopRequireDefault(require("../services/POStepService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var POStepController = /*#__PURE__*/function () {
  function POStepController() {
    (0, _classCallCheck2["default"])(this, POStepController);
  }

  (0, _createClass2["default"])(POStepController, null, [{
    key: "getAllPOSteps",
    value: function () {
      var _getAllPOSteps = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allPOSteps;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _POStepService["default"].getAllPOSteps(req.query);

              case 3:
                allPOSteps = _context.sent;

                if (allPOSteps.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "POSteps retrieved", allPOSteps);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No POStep found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllPOSteps(_x, _x2) {
        return _getAllPOSteps.apply(this, arguments);
      }

      return getAllPOSteps;
    }()
  }, {
    key: "getAPOStep",
    value: function () {
      var _getAPOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var id, thePOStep;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context2.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context2.abrupt("return", util.send(res));

              case 4:
                _context2.prev = 4;
                _context2.next = 7;
                return _POStepService["default"].getAPOStep(id);

              case 7:
                thePOStep = _context2.sent;

                if (!thePOStep) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POStep with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found POStep", thePOStep);
                }

                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context2.t0);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function getAPOStep(_x3, _x4) {
        return _getAPOStep.apply(this, arguments);
      }

      return getAPOStep;
    }()
  }, {
    key: "addPOStep",
    value: function () {
      var _addPOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var newPOStep, createdPOStep;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                if (!(!req.body.poID || !req.body.stepID || !req.body.status || !req.body.priority)) {
                  _context3.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context3.abrupt("return", util.send(res));

              case 3:
                newPOStep = req.body;
                _context3.prev = 4;
                _context3.next = 7;
                return _POStepService["default"].addPOStep(newPOStep);

              case 7:
                createdPOStep = _context3.sent;
                util.setSuccess(HTTP_CODE.CREATED, "POStep Added!", createdPOStep);
                return _context3.abrupt("return", util.send(res));

              case 12:
                _context3.prev = 12;
                _context3.t0 = _context3["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context3.t0.message);
                return _context3.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[4, 12]]);
      }));

      function addPOStep(_x5, _x6) {
        return _addPOStep.apply(this, arguments);
      }

      return addPOStep;
    }()
  }, {
    key: "updatedPOStep",
    value: function () {
      var _updatedPOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var alteredPOStep, id, updatePOStep;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                alteredPOStep = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 5:
                _context4.prev = 5;
                _context4.next = 8;
                return _POStepService["default"].updatePOStep(id, alteredPOStep);

              case 8:
                updatePOStep = _context4.sent;

                if (!updatePOStep) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POStep with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "POStep updated", updatePOStep);
                }

                return _context4.abrupt("return", util.send(res));

              case 13:
                _context4.prev = 13;
                _context4.t0 = _context4["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[5, 13]]);
      }));

      function updatedPOStep(_x7, _x8) {
        return _updatedPOStep.apply(this, arguments);
      }

      return updatedPOStep;
    }()
  }, {
    key: "deletePOStep",
    value: function () {
      var _deletePOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, quotationToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _POStepService["default"].deletePOStep(id);

              case 7:
                quotationToDelete = _context5.sent;

                if (quotationToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "POStep deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "POStep with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deletePOStep(_x9, _x10) {
        return _deletePOStep.apply(this, arguments);
      }

      return deletePOStep;
    }()
  }]);
  return POStepController;
}();

var _default = POStepController;
exports["default"] = _default;
//# sourceMappingURL=POStepController.js.map