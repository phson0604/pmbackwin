"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _UserService = _interopRequireDefault(require("../services/UserService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var _config = _interopRequireDefault(require("../src/config/config"));

var bcrypt = require("bcrypt");

var jwt = require("jsonwebtoken");

var _ = require("lodash");

var util = new _Utils["default"]();

var HTTP_CODE = require("../utils/HttpStatusCode");

var env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
var config = _config["default"][env];
var jwtPrivateKey = env === "production" ? process.env["jwtPrivateKey"] : config.jwtPrivateKey; //responsible for check login authenticator

var AuthController = /*#__PURE__*/function () {
  function AuthController() {
    (0, _classCallCheck2["default"])(this, AuthController);
  }

  (0, _createClass2["default"])(AuthController, null, [{
    key: "authenticateUser",
    value: function () {
      var _authenticateUser = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var user, validPassword, jwtToken;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _UserService["default"].findUser(req.body);

              case 3:
                user = _context.sent;

                if (user) {
                  _context.next = 7;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Invalid email or password.");
                return _context.abrupt("return", util.send(res));

              case 7:
                _context.next = 9;
                return bcrypt.compare(req.body.password, user.password);

              case 9:
                validPassword = _context.sent;

                if (validPassword) {
                  _context.next = 13;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Invalid email or password.");
                return _context.abrupt("return", util.send(res));

              case 13:
                // generate jwt token
                jwtToken = jwt.sign({
                  name: user.displayName,
                  email: user.email
                }, jwtPrivateKey);
                util.setSuccess(HTTP_CODE.OK, jwtToken); //send jwt to client as in response header
                //   return util.send(res.header("x-auth-token", jwtToken));

                return _context.abrupt("return", util.send(res));

              case 18:
                _context.prev = 18;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 22:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 18]]);
      }));

      function authenticateUser(_x, _x2) {
        return _authenticateUser.apply(this, arguments);
      }

      return authenticateUser;
    }()
  }]);
  return AuthController;
}();

var _default = AuthController;
exports["default"] = _default;
//# sourceMappingURL=AuthController.js.map