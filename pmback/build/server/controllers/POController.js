"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _POService = _interopRequireDefault(require("../services/POService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var _POStepService = _interopRequireDefault(require("../services/POStepService"));

var _POItemService = _interopRequireDefault(require("./../services/POItemService"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var POController = /*#__PURE__*/function () {
  function POController() {
    (0, _classCallCheck2["default"])(this, POController);
  }

  (0, _createClass2["default"])(POController, null, [{
    key: "getAllPOs",
    value: function () {
      var _getAllPOs = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allPOs;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _POService["default"].getAllPOs();

              case 3:
                allPOs = _context.sent;

                if (allPOs.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "POs retrieved", allPOs);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No PO found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllPOs(_x, _x2) {
        return _getAllPOs.apply(this, arguments);
      }

      return getAllPOs;
    }()
  }, {
    key: "addPO",
    value: function () {
      var _addPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newPO, createdPO, createdPOSteps;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                console.log(req.body);

                if (!(!req.body.poNumber || !req.body.customerID || !req.body.issuedDate || !req.body.deliveryDate || !req.body.paymentTerms)) {
                  _context2.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 4:
                newPO = req.body;
                _context2.prev = 5;
                _context2.next = 8;
                return _POService["default"].addPO(newPO);

              case 8:
                createdPO = _context2.sent;
                _context2.next = 11;
                return _POStepService["default"].createAllStepsOfNewPO(createdPO.id);

              case 11:
                createdPOSteps = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "PO Added!", createdPO, createdPOSteps);
                return _context2.abrupt("return", util.send(res));

              case 16:
                _context2.prev = 16;
                _context2.t0 = _context2["catch"](5);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 20:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[5, 16]]);
      }));

      function addPO(_x3, _x4) {
        return _addPO.apply(this, arguments);
      }

      return addPO;
    }()
  }, {
    key: "updatedPO",
    value: function () {
      var _updatedPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredPO, id, updatePO;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredPO = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _POService["default"].updatePO(id, alteredPO);

              case 8:
                updatePO = _context3.sent;

                if (!updatePO) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find PO with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "PO updated", updatePO);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedPO(_x5, _x6) {
        return _updatedPO.apply(this, arguments);
      }

      return updatedPO;
    }()
  }, {
    key: "getAPO",
    value: function () {
      var _getAPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, thePO;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _POService["default"].getAPO(id);

              case 7:
                thePO = _context4.sent;

                if (!thePO) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find PO with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found PO", thePO);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getAPO(_x7, _x8) {
        return _getAPO.apply(this, arguments);
      }

      return getAPO;
    }()
  }, {
    key: "deletePO",
    value: function () {
      var _deletePO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, poToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _POService["default"].deletePO(id);

              case 7:
                poToDelete = _context5.sent;

                if (!poToDelete) {
                  _context5.next = 16;
                  break;
                }

                _context5.next = 11;
                return _POStepService["default"].deleteAllStepsOfPO(id);

              case 11:
                _context5.next = 13;
                return _POItemService["default"].deleteAllItemOfPO(id);

              case 13:
                util.setSuccess(HTTP_CODE.OK, "PO deleted");
                _context5.next = 17;
                break;

              case 16:
                util.setError(HTTP_CODE.NOT_FOUND, "PO with the id ".concat(id, " cannot be found"));

              case 17:
                return _context5.abrupt("return", util.send(res));

              case 20:
                _context5.prev = 20;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 24:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 20]]);
      }));

      function deletePO(_x9, _x10) {
        return _deletePO.apply(this, arguments);
      }

      return deletePO;
    }()
  }]);
  return POController;
}();

var _default = POController;
exports["default"] = _default;
//# sourceMappingURL=POController.js.map