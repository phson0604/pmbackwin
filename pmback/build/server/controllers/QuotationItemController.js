"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _QuotationItemService = _interopRequireDefault(require("../services/QuotationItemService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var QuotationItemController = /*#__PURE__*/function () {
  function QuotationItemController() {
    (0, _classCallCheck2["default"])(this, QuotationItemController);
  }

  (0, _createClass2["default"])(QuotationItemController, null, [{
    key: "getAllQuotationItems",
    value: function () {
      var _getAllQuotationItems = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allQuotationItems;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _QuotationItemService["default"].getAllQuotationItems(req.query);

              case 3:
                allQuotationItems = _context.sent;

                if (allQuotationItems.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "QuotationItems retrieved", allQuotationItems);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No QuotationItem found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllQuotationItems(_x, _x2) {
        return _getAllQuotationItems.apply(this, arguments);
      }

      return getAllQuotationItems;
    }()
  }, {
    key: "addQuotationItem",
    value: function () {
      var _addQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newQuotationItem, createdQuotationItem;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.quotationID || !req.body.partNumber || !req.body.partName || !req.body.quantity || !req.body.unitPrice || !req.body.currency)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newQuotationItem = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _QuotationItemService["default"].addQuotationItem(newQuotationItem);

              case 7:
                createdQuotationItem = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "QuotationItem Added!", createdQuotationItem);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addQuotationItem(_x3, _x4) {
        return _addQuotationItem.apply(this, arguments);
      }

      return addQuotationItem;
    }()
  }, {
    key: "updatedQuotationItem",
    value: function () {
      var _updatedQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredQuotationItem, id, updateQuotationItem;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredQuotationItem = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _QuotationItemService["default"].updateQuotationItem(id, alteredQuotationItem);

              case 8:
                updateQuotationItem = _context3.sent;

                if (!updateQuotationItem) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find QuotationItem with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "QuotationItem updated", updateQuotationItem);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedQuotationItem(_x5, _x6) {
        return _updatedQuotationItem.apply(this, arguments);
      }

      return updatedQuotationItem;
    }()
  }, {
    key: "getAQuotationItem",
    value: function () {
      var _getAQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, theQuotationItem;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _QuotationItemService["default"].getAQuotationItem(id);

              case 7:
                theQuotationItem = _context4.sent;

                if (!theQuotationItem) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find QuotationItem with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found QuotationItem", theQuotationItem);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getAQuotationItem(_x7, _x8) {
        return _getAQuotationItem.apply(this, arguments);
      }

      return getAQuotationItem;
    }()
  }, {
    key: "deleteQuotationItem",
    value: function () {
      var _deleteQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, poToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _QuotationItemService["default"].deleteQuotationItem(id);

              case 7:
                poToDelete = _context5.sent;

                if (poToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "QuotationItem deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "QuotationItem with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deleteQuotationItem(_x9, _x10) {
        return _deleteQuotationItem.apply(this, arguments);
      }

      return deleteQuotationItem;
    }()
  }]);
  return QuotationItemController;
}();

var _default = QuotationItemController;
exports["default"] = _default;
//# sourceMappingURL=QuotationItemController.js.map