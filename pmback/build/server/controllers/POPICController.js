"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _POPICService = _interopRequireDefault(require("../services/POPICService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var POPICController = /*#__PURE__*/function () {
  function POPICController() {
    (0, _classCallCheck2["default"])(this, POPICController);
  }

  (0, _createClass2["default"])(POPICController, null, [{
    key: "getAllPOPICs",
    value: function () {
      var _getAllPOPICs = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allPOPICs;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _POPICService["default"].getAllPOPICs(req.query);

              case 3:
                allPOPICs = _context.sent;

                if (allPOPICs.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "POPICs retrieved", allPOPICs);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No POPIC found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllPOPICs(_x, _x2) {
        return _getAllPOPICs.apply(this, arguments);
      }

      return getAllPOPICs;
    }() // static async getPOPICbyPOID(req, res) {
    //   const { poID } = req.params;
    //   if (!Number(poID)) {
    //     util.setError(
    //       HTTP_CODE.BAD_REQUEST,
    //       "Please input a valid numeric value"
    //     );
    //     return util.send(res);
    //   }
    //   try {
    //     const picsOfPO = await POPICService.getPOPICbyPOID(poID);
    //     if (picsOfPO.length > 0) {
    //       util.setSuccess(HTTP_CODE.OK, "POPICs retrieved", picsOfPO);
    //     } else {
    //       util.setSuccess(HTTP_CODE.OK, "No POPIC found");
    //     }
    //     return util.send(res);
    //   } catch (error) {
    //     util.setError(HTTP_CODE.BAD_REQUEST, error);
    //     return util.send(res);
    //   }
    // }

  }, {
    key: "getAPOPIC",
    value: function () {
      var _getAPOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var id, thePOPIC;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context2.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context2.abrupt("return", util.send(res));

              case 4:
                _context2.prev = 4;
                _context2.next = 7;
                return _POPICService["default"].getAPOPIC(id);

              case 7:
                thePOPIC = _context2.sent;

                if (!thePOPIC) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POPIC with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found POPIC", thePOPIC);
                }

                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context2.t0);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function getAPOPIC(_x3, _x4) {
        return _getAPOPIC.apply(this, arguments);
      }

      return getAPOPIC;
    }()
  }, {
    key: "addPOPIC",
    value: function () {
      var _addPOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var newPOPIC, createdPOPIC;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                console.log(req.body);

                if (!(!req.body.poID || !req.body.picID || !req.body.roles)) {
                  _context3.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context3.abrupt("return", util.send(res));

              case 4:
                newPOPIC = req.body;
                _context3.prev = 5;
                _context3.next = 8;
                return _POPICService["default"].addPOPIC(newPOPIC);

              case 8:
                createdPOPIC = _context3.sent;
                util.setSuccess(HTTP_CODE.CREATED, "POPIC Added!", createdPOPIC);
                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.BAD_REQUEST, _context3.t0.message);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function addPOPIC(_x5, _x6) {
        return _addPOPIC.apply(this, arguments);
      }

      return addPOPIC;
    }()
  }, {
    key: "updatedPOPIC",
    value: function () {
      var _updatedPOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var alteredPOPIC, id, updatePOPIC;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                alteredPOPIC = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 5:
                _context4.prev = 5;
                _context4.next = 8;
                return _POPICService["default"].updatePOPIC(id, alteredPOPIC);

              case 8:
                updatePOPIC = _context4.sent;

                if (!updatePOPIC) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find POPIC with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "POPIC updated", updatePOPIC);
                }

                return _context4.abrupt("return", util.send(res));

              case 13:
                _context4.prev = 13;
                _context4.t0 = _context4["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[5, 13]]);
      }));

      function updatedPOPIC(_x7, _x8) {
        return _updatedPOPIC.apply(this, arguments);
      }

      return updatedPOPIC;
    }()
  }, {
    key: "deletePOPIC",
    value: function () {
      var _deletePOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, poToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _POPICService["default"].deletePOPIC(id);

              case 7:
                poToDelete = _context5.sent;

                if (poToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "POPIC deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "POPIC with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deletePOPIC(_x9, _x10) {
        return _deletePOPIC.apply(this, arguments);
      }

      return deletePOPIC;
    }()
  }]);
  return POPICController;
}();

var _default = POPICController;
exports["default"] = _default;
//# sourceMappingURL=POPICController.js.map