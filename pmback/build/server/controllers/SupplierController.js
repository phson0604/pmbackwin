"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _SupplierService = _interopRequireDefault(require("../services/SupplierService"));

var _Utils = _interopRequireDefault(require("../utils/Utils"));

var HTTP_CODE = require("../utils/HttpStatusCode");

var util = new _Utils["default"]();

var SupplierController = /*#__PURE__*/function () {
  function SupplierController() {
    (0, _classCallCheck2["default"])(this, SupplierController);
  }

  (0, _createClass2["default"])(SupplierController, null, [{
    key: "getAllSuppliers",
    value: function () {
      var _getAllSuppliers = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(req, res) {
        var allSuppliers;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _SupplierService["default"].getAllSuppliers();

              case 3:
                allSuppliers = _context.sent;

                if (allSuppliers.length > 0) {
                  util.setSuccess(HTTP_CODE.OK, "Suppliers retrieved", allSuppliers);
                } else {
                  util.setSuccess(HTTP_CODE.OK, "No Supplier found");
                }

                return _context.abrupt("return", util.send(res));

              case 8:
                _context.prev = 8;
                _context.t0 = _context["catch"](0);
                util.setError(HTTP_CODE.BAD_REQUEST, _context.t0);
                return _context.abrupt("return", util.send(res));

              case 12:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 8]]);
      }));

      function getAllSuppliers(_x, _x2) {
        return _getAllSuppliers.apply(this, arguments);
      }

      return getAllSuppliers;
    }()
  }, {
    key: "addSupplier",
    value: function () {
      var _addSupplier = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(req, res) {
        var newSupplier, createdSupplier;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                if (!(!req.body.name || !req.body.address || !req.body.contact)) {
                  _context2.next = 3;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
                return _context2.abrupt("return", util.send(res));

              case 3:
                newSupplier = req.body;
                _context2.prev = 4;
                _context2.next = 7;
                return _SupplierService["default"].addSupplier(newSupplier);

              case 7:
                createdSupplier = _context2.sent;
                util.setSuccess(HTTP_CODE.CREATED, "Supplier Added!", createdSupplier);
                return _context2.abrupt("return", util.send(res));

              case 12:
                _context2.prev = 12;
                _context2.t0 = _context2["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context2.t0.message);
                return _context2.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[4, 12]]);
      }));

      function addSupplier(_x3, _x4) {
        return _addSupplier.apply(this, arguments);
      }

      return addSupplier;
    }()
  }, {
    key: "updatedSupplier",
    value: function () {
      var _updatedSupplier = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(req, res) {
        var alteredSupplier, id, updateSupplier;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                alteredSupplier = req.body;
                id = req.params.id;

                if (Number(id)) {
                  _context3.next = 5;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context3.abrupt("return", util.send(res));

              case 5:
                _context3.prev = 5;
                _context3.next = 8;
                return _SupplierService["default"].updateSupplier(id, alteredSupplier);

              case 8:
                updateSupplier = _context3.sent;

                if (!updateSupplier) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Supplier with the id: ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Supplier updated", updateSupplier);
                }

                return _context3.abrupt("return", util.send(res));

              case 13:
                _context3.prev = 13;
                _context3.t0 = _context3["catch"](5);
                util.setError(HTTP_CODE.NOT_FOUND, _context3.t0);
                return _context3.abrupt("return", util.send(res));

              case 17:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[5, 13]]);
      }));

      function updatedSupplier(_x5, _x6) {
        return _updatedSupplier.apply(this, arguments);
      }

      return updatedSupplier;
    }()
  }, {
    key: "getASupplier",
    value: function () {
      var _getASupplier = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(req, res) {
        var id, theSupplier;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context4.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please input a valid numeric value");
                return _context4.abrupt("return", util.send(res));

              case 4:
                _context4.prev = 4;
                _context4.next = 7;
                return _SupplierService["default"].getASupplier(id);

              case 7:
                theSupplier = _context4.sent;

                if (!theSupplier) {
                  util.setError(HTTP_CODE.NOT_FOUND, "Cannot find Supplier with the id ".concat(id));
                } else {
                  util.setSuccess(HTTP_CODE.OK, "Found Supplier", theSupplier);
                }

                return _context4.abrupt("return", util.send(res));

              case 12:
                _context4.prev = 12;
                _context4.t0 = _context4["catch"](4);
                util.setError(HTTP_CODE.NOT_FOUND, _context4.t0);
                return _context4.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[4, 12]]);
      }));

      function getASupplier(_x7, _x8) {
        return _getASupplier.apply(this, arguments);
      }

      return getASupplier;
    }()
  }, {
    key: "deleteSupplier",
    value: function () {
      var _deleteSupplier = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(req, res) {
        var id, supplierToDelete;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                id = req.params.id;

                if (Number(id)) {
                  _context5.next = 4;
                  break;
                }

                util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
                return _context5.abrupt("return", util.send(res));

              case 4:
                _context5.prev = 4;
                _context5.next = 7;
                return _SupplierService["default"].deleteSupplier(id);

              case 7:
                supplierToDelete = _context5.sent;

                if (supplierToDelete) {
                  util.setSuccess(HTTP_CODE.OK, "Supplier deleted");
                } else {
                  util.setError(HTTP_CODE.NOT_FOUND, "Supplier with the id ".concat(id, " cannot be found"));
                }

                return _context5.abrupt("return", util.send(res));

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](4);
                util.setError(HTTP_CODE.BAD_REQUEST, _context5.t0);
                return _context5.abrupt("return", util.send(res));

              case 16:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[4, 12]]);
      }));

      function deleteSupplier(_x9, _x10) {
        return _deleteSupplier.apply(this, arguments);
      }

      return deleteSupplier;
    }()
  }]);
  return SupplierController;
}();

var _default = SupplierController;
exports["default"] = _default;
//# sourceMappingURL=SupplierController.js.map