"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var POItemService = /*#__PURE__*/function () {
  function POItemService() {
    (0, _classCallCheck2["default"])(this, POItemService);
  }

  (0, _createClass2["default"])(POItemService, null, [{
    key: "getAllPOItems",
    //get all by PO
    value: function () {
      var _getAllPOItems = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
        var poID;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                poID = _ref.poID;
                _context.prev = 1;

                if (!poID) {
                  _context.next = 8;
                  break;
                }

                _context.next = 5;
                return _models["default"].POItem.findAll({
                  where: {
                    poID: poID
                  }
                });

              case 5:
                return _context.abrupt("return", _context.sent);

              case 8:
                return _context.abrupt("return", {});

              case 9:
                _context.next = 14;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](1);
                throw _context.t0;

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 11]]);
      }));

      function getAllPOItems(_x) {
        return _getAllPOItems.apply(this, arguments);
      }

      return getAllPOItems;
    }()
  }, {
    key: "addPOItem",
    value: function () {
      var _addPOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(newPOItem) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].POItem.create(newPOItem);

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      function addPOItem(_x2) {
        return _addPOItem.apply(this, arguments);
      }

      return addPOItem;
    }()
  }, {
    key: "updatePOItem",
    value: function () {
      var _updatePOItem2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(id, _updatePOItem) {
        var poToUpdate;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].POItem.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                poToUpdate = _context3.sent;

                if (!poToUpdate) {
                  _context3.next = 8;
                  break;
                }

                _context3.next = 7;
                return _models["default"].POItem.update(_updatePOItem, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context3.abrupt("return", _updatePOItem);

              case 8:
                return _context3.abrupt("return", null);

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 11]]);
      }));

      function updatePOItem(_x3, _x4) {
        return _updatePOItem2.apply(this, arguments);
      }

      return updatePOItem;
    }()
  }, {
    key: "getAPOItem",
    value: function () {
      var _getAPOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id) {
        var thePOItem;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].POItem.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                thePOItem = _context4.sent;
                return _context4.abrupt("return", thePOItem);

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }));

      function getAPOItem(_x5) {
        return _getAPOItem.apply(this, arguments);
      }

      return getAPOItem;
    }()
  }, {
    key: "deletePOItem",
    value: function () {
      var _deletePOItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var poToDelete, deletedPOItem;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].POItem.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                poToDelete = _context5.sent;

                if (!poToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].POItem.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedPOItem = _context5.sent;
                return _context5.abrupt("return", deletedPOItem);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deletePOItem(_x6) {
        return _deletePOItem.apply(this, arguments);
      }

      return deletePOItem;
    }()
  }, {
    key: "deleteAllItemOfPO",
    value: function () {
      var _deleteAllItemOfPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(poID) {
        var _this = this;

        var allItems;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return this.getAllPOItems({
                  poID: poID
                });

              case 3:
                allItems = _context6.sent;
                _context6.next = 6;
                return allItems.forEach(function (_ref2) {
                  var dataValues = _ref2.dataValues;

                  _this.deletePOItem(dataValues.id);
                });

              case 6:
                _context6.next = 10;
                break;

              case 8:
                _context6.prev = 8;
                _context6.t0 = _context6["catch"](0);

              case 10:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[0, 8]]);
      }));

      function deleteAllItemOfPO(_x7) {
        return _deleteAllItemOfPO.apply(this, arguments);
      }

      return deleteAllItemOfPO;
    }()
  }]);
  return POItemService;
}();

var _default = POItemService;
exports["default"] = _default;
//# sourceMappingURL=POItemService.js.map