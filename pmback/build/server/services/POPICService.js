"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

// const Op = require("Sequelize").Op;
var POPICService = /*#__PURE__*/function () {
  function POPICService() {
    (0, _classCallCheck2["default"])(this, POPICService);
  }

  (0, _createClass2["default"])(POPICService, null, [{
    key: "getAllPOPICs",
    //get all by PO
    value: function () {
      var _getAllPOPICs = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
        var poID;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                poID = _ref.poID;
                _context.prev = 1;

                if (!poID) {
                  _context.next = 8;
                  break;
                }

                _context.next = 5;
                return _models["default"].User.findAll({
                  attributes: ["id", "email", "displayName"],
                  include: [{
                    model: _models["default"].POPIC,
                    attributes: ["id", "poID", "picID", "roles"],
                    where: {
                      poID: poID
                    },
                    required: false // effect as LEFT JOIN

                  }]
                });

              case 5:
                return _context.abrupt("return", _context.sent);

              case 8:
                _context.next = 10;
                return _models["default"].POPIC.findAll({
                  include: [{
                    model: _models["default"].PO,
                    attributes: ["poNumber"]
                  }, {
                    model: _models["default"].User,
                    attributes: ["email", "displayName"]
                  }]
                });

              case 10:
                return _context.abrupt("return", _context.sent);

              case 11:
                _context.next = 16;
                break;

              case 13:
                _context.prev = 13;
                _context.t0 = _context["catch"](1);
                throw _context.t0;

              case 16:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 13]]);
      }));

      function getAllPOPICs(_x) {
        return _getAllPOPICs.apply(this, arguments);
      }

      return getAllPOPICs;
    }()
  }, {
    key: "getAPOPIC",
    value: function () {
      var _getAPOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(id) {
        var thePOPIC;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].POPIC.findOne({
                  include: [{
                    model: _models["default"].PO,
                    attributes: ["poNumber"]
                  }, {
                    model: _models["default"].User
                  }],
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                thePOPIC = _context2.sent;
                return _context2.abrupt("return", thePOPIC);

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 7]]);
      }));

      function getAPOPIC(_x2) {
        return _getAPOPIC.apply(this, arguments);
      }

      return getAPOPIC;
    }()
  }, {
    key: "addPOPIC",
    value: function () {
      var _addPOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(newPOPIC) {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].POPIC.create(newPOPIC);

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 6:
                _context3.prev = 6;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 6]]);
      }));

      function addPOPIC(_x3) {
        return _addPOPIC.apply(this, arguments);
      }

      return addPOPIC;
    }()
  }, {
    key: "updatePOPIC",
    value: function () {
      var _updatePOPIC2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id, _updatePOPIC) {
        var poToUpdate;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].POPIC.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                poToUpdate = _context4.sent;

                if (!poToUpdate) {
                  _context4.next = 8;
                  break;
                }

                _context4.next = 7;
                return _models["default"].POPIC.update(_updatePOPIC, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context4.abrupt("return", _updatePOPIC);

              case 8:
                return _context4.abrupt("return", null);

              case 11:
                _context4.prev = 11;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 14:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 11]]);
      }));

      function updatePOPIC(_x4, _x5) {
        return _updatePOPIC2.apply(this, arguments);
      }

      return updatePOPIC;
    }()
  }, {
    key: "deletePOPIC",
    value: function () {
      var _deletePOPIC = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var popicToDelete, deletedPOPIC;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].POPIC.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                popicToDelete = _context5.sent;

                if (!popicToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].POPIC.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedPOPIC = _context5.sent;
                return _context5.abrupt("return", deletedPOPIC);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deletePOPIC(_x6) {
        return _deletePOPIC.apply(this, arguments);
      }

      return deletePOPIC;
    }()
  }]);
  return POPICService;
}();

var _default = POPICService;
exports["default"] = _default;
//# sourceMappingURL=POPICService.js.map