"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var QuotationItemService = /*#__PURE__*/function () {
  function QuotationItemService() {
    (0, _classCallCheck2["default"])(this, QuotationItemService);
  }

  (0, _createClass2["default"])(QuotationItemService, null, [{
    key: "getAllQuotationItems",
    value: function () {
      var _getAllQuotationItems = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
        var quotationID;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                quotationID = _ref.quotationID;
                _context.prev = 1;

                if (!quotationID) {
                  _context.next = 8;
                  break;
                }

                _context.next = 5;
                return _models["default"].QuotationItem.findAll({
                  where: {
                    quotationID: quotationID
                  }
                });

              case 5:
                return _context.abrupt("return", _context.sent);

              case 8:
                return _context.abrupt("return", {});

              case 9:
                _context.next = 14;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](1);
                throw _context.t0;

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 11]]);
      }));

      function getAllQuotationItems(_x) {
        return _getAllQuotationItems.apply(this, arguments);
      }

      return getAllQuotationItems;
    }()
  }, {
    key: "addQuotationItem",
    value: function () {
      var _addQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(newQuotationItem) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].QuotationItem.create(newQuotationItem);

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      function addQuotationItem(_x2) {
        return _addQuotationItem.apply(this, arguments);
      }

      return addQuotationItem;
    }()
  }, {
    key: "updateQuotationItem",
    value: function () {
      var _updateQuotationItem2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(id, _updateQuotationItem) {
        var QuotationItemToUpdate;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].QuotationItem.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                QuotationItemToUpdate = _context3.sent;

                if (!QuotationItemToUpdate) {
                  _context3.next = 8;
                  break;
                }

                _context3.next = 7;
                return _models["default"].QuotationItem.update(_updateQuotationItem, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context3.abrupt("return", _updateQuotationItem);

              case 8:
                return _context3.abrupt("return", null);

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 11]]);
      }));

      function updateQuotationItem(_x3, _x4) {
        return _updateQuotationItem2.apply(this, arguments);
      }

      return updateQuotationItem;
    }()
  }, {
    key: "getAQuotationItem",
    value: function () {
      var _getAQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id) {
        var theQuotationItem;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].QuotationItem.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                theQuotationItem = _context4.sent;
                return _context4.abrupt("return", theQuotationItem);

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }));

      function getAQuotationItem(_x5) {
        return _getAQuotationItem.apply(this, arguments);
      }

      return getAQuotationItem;
    }()
  }, {
    key: "deleteQuotationItem",
    value: function () {
      var _deleteQuotationItem = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var QuotationItemToDelete, deletedQuotationItem;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].QuotationItem.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                QuotationItemToDelete = _context5.sent;

                if (!QuotationItemToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].QuotationItem.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedQuotationItem = _context5.sent;
                return _context5.abrupt("return", deletedQuotationItem);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deleteQuotationItem(_x6) {
        return _deleteQuotationItem.apply(this, arguments);
      }

      return deleteQuotationItem;
    }()
  }, {
    key: "deleteAllItemOfQuotation",
    value: function () {
      var _deleteAllItemOfQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(quotationID) {
        var _this = this;

        var allItems;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return this.getAllQuotationItems({
                  quotationID: quotationID
                });

              case 3:
                allItems = _context6.sent;
                _context6.next = 6;
                return allItems.forEach(function (_ref2) {
                  var dataValues = _ref2.dataValues;

                  _this.deleteQuotationItem(dataValues.id);
                });

              case 6:
                _context6.next = 10;
                break;

              case 8:
                _context6.prev = 8;
                _context6.t0 = _context6["catch"](0);

              case 10:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, this, [[0, 8]]);
      }));

      function deleteAllItemOfQuotation(_x7) {
        return _deleteAllItemOfQuotation.apply(this, arguments);
      }

      return deleteAllItemOfQuotation;
    }()
  }]);
  return QuotationItemService;
}();

var _default = QuotationItemService;
exports["default"] = _default;
//# sourceMappingURL=QuotationItemService.js.map