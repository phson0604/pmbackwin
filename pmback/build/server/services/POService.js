"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var POService = /*#__PURE__*/function () {
  function POService() {
    (0, _classCallCheck2["default"])(this, POService);
  }

  (0, _createClass2["default"])(POService, null, [{
    key: "getAllPOs",
    value: function () {
      var _getAllPOs = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _models["default"].PO.findAll({
                  include: [{
                    model: _models["default"].Customer,
                    attributes: ["name"]
                  }, {
                    model: _models["default"].POPIC
                  }, {
                    model: _models["default"].POItem
                  }, {
                    model: _models["default"].Quotation
                  }]
                });

              case 3:
                return _context.abrupt("return", _context.sent);

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](0);
                throw _context.t0;

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 6]]);
      }));

      function getAllPOs() {
        return _getAllPOs.apply(this, arguments);
      }

      return getAllPOs;
    }()
  }, {
    key: "getAPO",
    value: function () {
      var _getAPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(id) {
        var thePO;
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].PO.findOne({
                  include: [{
                    model: _models["default"].Customer,
                    attributes: ["name"]
                  }, {
                    model: _models["default"].POPIC
                  }, {
                    model: _models["default"].POItem
                  }, {
                    model: _models["default"].Quotation
                  }],
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                thePO = _context2.sent;
                return _context2.abrupt("return", thePO);

              case 7:
                _context2.prev = 7;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 10:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 7]]);
      }));

      function getAPO(_x) {
        return _getAPO.apply(this, arguments);
      }

      return getAPO;
    }()
  }, {
    key: "addPO",
    value: function () {
      var _addPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(newPO) {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].PO.create(newPO);

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 6:
                _context3.prev = 6;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 6]]);
      }));

      function addPO(_x2) {
        return _addPO.apply(this, arguments);
      }

      return addPO;
    }()
  }, {
    key: "updatePO",
    value: function () {
      var _updatePO2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id, _updatePO) {
        var poToUpdate;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].PO.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                poToUpdate = _context4.sent;

                if (!poToUpdate) {
                  _context4.next = 8;
                  break;
                }

                _context4.next = 7;
                return _models["default"].PO.update(_updatePO, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context4.abrupt("return", _updatePO);

              case 8:
                return _context4.abrupt("return", null);

              case 11:
                _context4.prev = 11;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 14:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 11]]);
      }));

      function updatePO(_x3, _x4) {
        return _updatePO2.apply(this, arguments);
      }

      return updatePO;
    }()
  }, {
    key: "deletePO",
    value: function () {
      var _deletePO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var poToDelete, deletedPO;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].PO.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                poToDelete = _context5.sent;

                if (!poToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].PO.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedPO = _context5.sent;
                return _context5.abrupt("return", deletedPO);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deletePO(_x5) {
        return _deletePO.apply(this, arguments);
      }

      return deletePO;
    }()
  }]);
  return POService;
}();

var _default = POService;
exports["default"] = _default;
//# sourceMappingURL=POService.js.map