"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var _POStepMasterService = _interopRequireDefault(require("./POStepMasterService"));

/* !!!!!!!!!!
   Return only first row of each group (poID)
   Sequelize ORM not support => Must use raw sql query

   Should include { type: db.sequelize.QueryTypes.SELECT }
   then will return same structure as ORM query
 */
var POStepService = /*#__PURE__*/function () {
  function POStepService() {
    (0, _classCallCheck2["default"])(this, POStepService);
  }

  (0, _createClass2["default"])(POStepService, null, [{
    key: "getAllPOSteps",
    value: function () {
      var _getAllPOSteps = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        var STARTING;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                STARTING = "0";
                _context.prev = 1;
                _context.next = 4;
                return _models["default"].sequelize.query("select distinct on (\"POSteps\".\"poID\") \n        \"POSteps\".\"id\",\n        \"POs\".\"poNumber\",\"POStepMasters\".\"stepName\",\n        \"POSteps\".\"priority\",\n        \"POSteps\".\"photoURLs\",\n        \"POSteps\".\"comment\",\n        \"POs\".\"issuedDate\",\"POs\".\"deliveryDate\",\n        \"Customers\".\"name\"\n        from public.\"POSteps\"\n        left join public.\"POs\" on public.\"POSteps\".\"poID\"=public.\"POs\".\"id\"\n        left join public.\"Customers\" on \"POs\".\"customerID\"=\"Customers\".\"id\"\n        left join public.\"POStepMasters\" on \"POSteps\".\"stepID\"=\"POStepMasters\".\"id\"\n        where \"POSteps\".\"status\" = (:status)\n       ORDER BY \"POSteps\".\"poID\", \"POStepMasters\".\"sequence\" ASC\n        ", {
                  replacements: {
                    status: STARTING
                  },
                  type: _models["default"].sequelize.QueryTypes.SELECT
                });

              case 4:
                return _context.abrupt("return", _context.sent);

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](1);
                throw _context.t0;

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 7]]);
      }));

      function getAllPOSteps() {
        return _getAllPOSteps.apply(this, arguments);
      }

      return getAllPOSteps;
    }()
  }, {
    key: "addPOStep",
    value: function () {
      var _addPOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(newPOStep) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].POStep.create(newPOStep);

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      function addPOStep(_x) {
        return _addPOStep.apply(this, arguments);
      }

      return addPOStep;
    }()
  }, {
    key: "updatePOStep",
    value: function () {
      var _updatePOStep2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(id, _updatePOStep) {
        var quoteToUpdate;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].POStep.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                quoteToUpdate = _context3.sent;

                if (!quoteToUpdate) {
                  _context3.next = 8;
                  break;
                }

                _context3.next = 7;
                return _models["default"].POStep.update(_updatePOStep, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context3.abrupt("return", _updatePOStep);

              case 8:
                return _context3.abrupt("return", null);

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 11]]);
      }));

      function updatePOStep(_x2, _x3) {
        return _updatePOStep2.apply(this, arguments);
      }

      return updatePOStep;
    }()
  }, {
    key: "getAPOStep",
    value: function () {
      var _getAPOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id) {
        var thePOStep;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].POStep.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                thePOStep = _context4.sent;
                return _context4.abrupt("return", thePOStep);

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }));

      function getAPOStep(_x4) {
        return _getAPOStep.apply(this, arguments);
      }

      return getAPOStep;
    }()
  }, {
    key: "deletePOStep",
    value: function () {
      var _deletePOStep = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var quoteToDelete, deletedPOStep;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].POStep.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                quoteToDelete = _context5.sent;

                if (!quoteToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].POStep.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedPOStep = _context5.sent;
                return _context5.abrupt("return", deletedPOStep);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deletePOStep(_x5) {
        return _deletePOStep.apply(this, arguments);
      }

      return deletePOStep;
    }()
  }, {
    key: "createAllStepsOfNewPO",
    value: function () {
      var _createAllStepsOfNewPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee6(poID) {
        var allPOSteps;
        return _regenerator["default"].wrap(function _callee6$(_context6) {
          while (1) {
            switch (_context6.prev = _context6.next) {
              case 0:
                _context6.prev = 0;
                _context6.next = 3;
                return _POStepMasterService["default"].getAllPOStepMasters();

              case 3:
                allPOSteps = _context6.sent;
                _context6.next = 6;
                return allPOSteps.forEach(function (_ref) {
                  var dataValues = _ref.dataValues;
                  var newPOStep = {
                    poID: poID,
                    stepID: dataValues.id,
                    status: 0,
                    //"starting"
                    priority: "normal"
                  }; // console.log("newPOStep:", newPOStep);

                  _models["default"].POStep.create(newPOStep);
                });

              case 6:
                _context6.next = 11;
                break;

              case 8:
                _context6.prev = 8;
                _context6.t0 = _context6["catch"](0);
                throw _context6.t0;

              case 11:
              case "end":
                return _context6.stop();
            }
          }
        }, _callee6, null, [[0, 8]]);
      }));

      function createAllStepsOfNewPO(_x6) {
        return _createAllStepsOfNewPO.apply(this, arguments);
      }

      return createAllStepsOfNewPO;
    }()
  }, {
    key: "deleteAllStepsOfPO",
    value: function () {
      var _deleteAllStepsOfPO = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee7(poID) {
        var _this = this;

        var allPOSteps;
        return _regenerator["default"].wrap(function _callee7$(_context7) {
          while (1) {
            switch (_context7.prev = _context7.next) {
              case 0:
                _context7.prev = 0;
                _context7.next = 3;
                return _models["default"].POStep.findAll({
                  where: {
                    poID: Number(poID)
                  }
                });

              case 3:
                allPOSteps = _context7.sent;
                _context7.next = 6;
                return allPOSteps.forEach(function (_ref2) {
                  var dataValues = _ref2.dataValues;

                  _this.deletePOStep(dataValues.id);
                });

              case 6:
                _context7.next = 10;
                break;

              case 8:
                _context7.prev = 8;
                _context7.t0 = _context7["catch"](0);

              case 10:
              case "end":
                return _context7.stop();
            }
          }
        }, _callee7, null, [[0, 8]]);
      }));

      function deleteAllStepsOfPO(_x7) {
        return _deleteAllStepsOfPO.apply(this, arguments);
      }

      return deleteAllStepsOfPO;
    }()
  }]);
  return POStepService;
}();

var _default = POStepService;
exports["default"] = _default;
//# sourceMappingURL=POStepService.js.map