"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var CustomerService = /*#__PURE__*/function () {
  function CustomerService() {
    (0, _classCallCheck2["default"])(this, CustomerService);
  }

  (0, _createClass2["default"])(CustomerService, null, [{
    key: "getAllCustomers",
    value: function () {
      var _getAllCustomers = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee() {
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _models["default"].Customer.findAll();

              case 3:
                return _context.abrupt("return", _context.sent);

              case 6:
                _context.prev = 6;
                _context.t0 = _context["catch"](0);
                throw _context.t0;

              case 9:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 6]]);
      }));

      function getAllCustomers() {
        return _getAllCustomers.apply(this, arguments);
      }

      return getAllCustomers;
    }()
  }, {
    key: "addCustomer",
    value: function () {
      var _addCustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(newCustomer) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].Customer.create(newCustomer);

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      function addCustomer(_x) {
        return _addCustomer.apply(this, arguments);
      }

      return addCustomer;
    }()
  }, {
    key: "updateCustomer",
    value: function () {
      var _updateCustomer2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(id, _updateCustomer) {
        var customerToUpdate;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].Customer.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                customerToUpdate = _context3.sent;

                if (!customerToUpdate) {
                  _context3.next = 8;
                  break;
                }

                _context3.next = 7;
                return _models["default"].Customer.update(_updateCustomer, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context3.abrupt("return", _updateCustomer);

              case 8:
                return _context3.abrupt("return", null);

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 11]]);
      }));

      function updateCustomer(_x2, _x3) {
        return _updateCustomer2.apply(this, arguments);
      }

      return updateCustomer;
    }()
  }, {
    key: "getACustomer",
    value: function () {
      var _getACustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id) {
        var theCustomer;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].Customer.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                theCustomer = _context4.sent;
                return _context4.abrupt("return", theCustomer);

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }));

      function getACustomer(_x4) {
        return _getACustomer.apply(this, arguments);
      }

      return getACustomer;
    }()
  }, {
    key: "deleteCustomer",
    value: function () {
      var _deleteCustomer = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var customerToDelete, deletedCustomer;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].Customer.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                customerToDelete = _context5.sent;

                if (!customerToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].Customer.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedCustomer = _context5.sent;
                return _context5.abrupt("return", deletedCustomer);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deleteCustomer(_x5) {
        return _deleteCustomer.apply(this, arguments);
      }

      return deleteCustomer;
    }()
  }]);
  return CustomerService;
}();

var _default = CustomerService;
exports["default"] = _default;
//# sourceMappingURL=CustomerService.js.map