"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var POStepMasterService = /*#__PURE__*/function () {
  function POStepMasterService() {
    (0, _classCallCheck2["default"])(this, POStepMasterService);
  }

  (0, _createClass2["default"])(POStepMasterService, null, [{
    key: "getAPOStepMaster",
    value: function () {
      var _getAPOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(id) {
        var thePOStepMaster;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                _context.prev = 0;
                _context.next = 3;
                return _models["default"].POStepMaster.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                thePOStepMaster = _context.sent;
                return _context.abrupt("return", thePOStepMaster);

              case 7:
                _context.prev = 7;
                _context.t0 = _context["catch"](0);
                throw _context.t0;

              case 10:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[0, 7]]);
      }));

      function getAPOStepMaster(_x) {
        return _getAPOStepMaster.apply(this, arguments);
      }

      return getAPOStepMaster;
    }()
  }, {
    key: "getAllPOStepMasters",
    value: function () {
      var _getAllPOStepMasters = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2() {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].POStepMaster.findAll();

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      function getAllPOStepMasters() {
        return _getAllPOStepMasters.apply(this, arguments);
      }

      return getAllPOStepMasters;
    }()
  }, {
    key: "addPOStepMaster",
    value: function () {
      var _addPOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(newPOStepMaster) {
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].POStepMaster.create(newPOStepMaster);

              case 3:
                return _context3.abrupt("return", _context3.sent);

              case 6:
                _context3.prev = 6;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 9:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 6]]);
      }));

      function addPOStepMaster(_x2) {
        return _addPOStepMaster.apply(this, arguments);
      }

      return addPOStepMaster;
    }()
  }, {
    key: "updatePOStepMaster",
    value: function () {
      var _updatePOStepMaster2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id, _updatePOStepMaster) {
        var quoteToUpdate;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].POStepMaster.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                quoteToUpdate = _context4.sent;

                if (!quoteToUpdate) {
                  _context4.next = 8;
                  break;
                }

                _context4.next = 7;
                return _models["default"].POStepMaster.update(_updatePOStepMaster, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context4.abrupt("return", _updatePOStepMaster);

              case 8:
                return _context4.abrupt("return", null);

              case 11:
                _context4.prev = 11;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 14:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 11]]);
      }));

      function updatePOStepMaster(_x3, _x4) {
        return _updatePOStepMaster2.apply(this, arguments);
      }

      return updatePOStepMaster;
    }()
  }, {
    key: "deletePOStepMaster",
    value: function () {
      var _deletePOStepMaster = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var quoteToDelete, deletedPOStepMaster;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].POStepMaster.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                quoteToDelete = _context5.sent;

                if (!quoteToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].POStepMaster.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedPOStepMaster = _context5.sent;
                return _context5.abrupt("return", deletedPOStepMaster);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deletePOStepMaster(_x5) {
        return _deletePOStepMaster.apply(this, arguments);
      }

      return deletePOStepMaster;
    }()
  }]);
  return POStepMasterService;
}();

var _default = POStepMasterService;
exports["default"] = _default;
//# sourceMappingURL=POStepMasterService.js.map