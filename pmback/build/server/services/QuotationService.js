"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _asyncToGenerator2 = _interopRequireDefault(require("@babel/runtime/helpers/asyncToGenerator"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _models = _interopRequireDefault(require("../src/models"));

var QuotationService = /*#__PURE__*/function () {
  function QuotationService() {
    (0, _classCallCheck2["default"])(this, QuotationService);
  }

  (0, _createClass2["default"])(QuotationService, null, [{
    key: "getAllQuotations",
    value: function () {
      var _getAllQuotations = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee(_ref) {
        var poID;
        return _regenerator["default"].wrap(function _callee$(_context) {
          while (1) {
            switch (_context.prev = _context.next) {
              case 0:
                poID = _ref.poID;
                _context.prev = 1;

                if (!poID) {
                  _context.next = 8;
                  break;
                }

                _context.next = 5;
                return _models["default"].Quotation.findAll({
                  where: {
                    poID: poID
                  },
                  include: [{
                    model: _models["default"].PO,
                    attributes: ["poNumber"]
                  }, {
                    model: _models["default"].Supplier,
                    attributes: ["name"]
                  }, {
                    model: _models["default"].QuotationItem
                  }]
                });

              case 5:
                return _context.abrupt("return", _context.sent);

              case 8:
                return _context.abrupt("return", {});

              case 9:
                _context.next = 14;
                break;

              case 11:
                _context.prev = 11;
                _context.t0 = _context["catch"](1);
                throw _context.t0;

              case 14:
              case "end":
                return _context.stop();
            }
          }
        }, _callee, null, [[1, 11]]);
      }));

      function getAllQuotations(_x) {
        return _getAllQuotations.apply(this, arguments);
      }

      return getAllQuotations;
    }()
  }, {
    key: "addQuotation",
    value: function () {
      var _addQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee2(newQuotation) {
        return _regenerator["default"].wrap(function _callee2$(_context2) {
          while (1) {
            switch (_context2.prev = _context2.next) {
              case 0:
                _context2.prev = 0;
                _context2.next = 3;
                return _models["default"].Quotation.create(newQuotation);

              case 3:
                return _context2.abrupt("return", _context2.sent);

              case 6:
                _context2.prev = 6;
                _context2.t0 = _context2["catch"](0);
                throw _context2.t0;

              case 9:
              case "end":
                return _context2.stop();
            }
          }
        }, _callee2, null, [[0, 6]]);
      }));

      function addQuotation(_x2) {
        return _addQuotation.apply(this, arguments);
      }

      return addQuotation;
    }()
  }, {
    key: "updateQuotation",
    value: function () {
      var _updateQuotation2 = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee3(id, _updateQuotation) {
        var quoteToUpdate;
        return _regenerator["default"].wrap(function _callee3$(_context3) {
          while (1) {
            switch (_context3.prev = _context3.next) {
              case 0:
                _context3.prev = 0;
                _context3.next = 3;
                return _models["default"].Quotation.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                quoteToUpdate = _context3.sent;

                if (!quoteToUpdate) {
                  _context3.next = 8;
                  break;
                }

                _context3.next = 7;
                return _models["default"].Quotation.update(_updateQuotation, {
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                return _context3.abrupt("return", _updateQuotation);

              case 8:
                return _context3.abrupt("return", null);

              case 11:
                _context3.prev = 11;
                _context3.t0 = _context3["catch"](0);
                throw _context3.t0;

              case 14:
              case "end":
                return _context3.stop();
            }
          }
        }, _callee3, null, [[0, 11]]);
      }));

      function updateQuotation(_x3, _x4) {
        return _updateQuotation2.apply(this, arguments);
      }

      return updateQuotation;
    }()
  }, {
    key: "getAQuotation",
    value: function () {
      var _getAQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee4(id) {
        var theQuotation;
        return _regenerator["default"].wrap(function _callee4$(_context4) {
          while (1) {
            switch (_context4.prev = _context4.next) {
              case 0:
                _context4.prev = 0;
                _context4.next = 3;
                return _models["default"].Quotation.findOne({
                  where: {
                    id: Number(id)
                  },
                  include: [{
                    model: _models["default"].PO,
                    attributes: ["poNumber"]
                  }, {
                    model: _models["default"].Supplier,
                    attributes: ["name"]
                  }, {
                    model: _models["default"].QuotationItem
                  }]
                });

              case 3:
                theQuotation = _context4.sent;
                return _context4.abrupt("return", theQuotation);

              case 7:
                _context4.prev = 7;
                _context4.t0 = _context4["catch"](0);
                throw _context4.t0;

              case 10:
              case "end":
                return _context4.stop();
            }
          }
        }, _callee4, null, [[0, 7]]);
      }));

      function getAQuotation(_x5) {
        return _getAQuotation.apply(this, arguments);
      }

      return getAQuotation;
    }()
  }, {
    key: "deleteQuotation",
    value: function () {
      var _deleteQuotation = (0, _asyncToGenerator2["default"])( /*#__PURE__*/_regenerator["default"].mark(function _callee5(id) {
        var quoteToDelete, deletedQuotation;
        return _regenerator["default"].wrap(function _callee5$(_context5) {
          while (1) {
            switch (_context5.prev = _context5.next) {
              case 0:
                _context5.prev = 0;
                _context5.next = 3;
                return _models["default"].Quotation.findOne({
                  where: {
                    id: Number(id)
                  }
                });

              case 3:
                quoteToDelete = _context5.sent;

                if (!quoteToDelete) {
                  _context5.next = 9;
                  break;
                }

                _context5.next = 7;
                return _models["default"].Quotation.destroy({
                  where: {
                    id: Number(id)
                  }
                });

              case 7:
                deletedQuotation = _context5.sent;
                return _context5.abrupt("return", deletedQuotation);

              case 9:
                return _context5.abrupt("return", null);

              case 12:
                _context5.prev = 12;
                _context5.t0 = _context5["catch"](0);
                throw _context5.t0;

              case 15:
              case "end":
                return _context5.stop();
            }
          }
        }, _callee5, null, [[0, 12]]);
      }));

      function deleteQuotation(_x6) {
        return _deleteQuotation.apply(this, arguments);
      }

      return deleteQuotation;
    }()
  }]);
  return QuotationService;
}();

var _default = QuotationService;
exports["default"] = _default;
//# sourceMappingURL=QuotationService.js.map