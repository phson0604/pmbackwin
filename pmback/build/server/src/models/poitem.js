"use strict";

module.exports = function (sequelize, DataTypes) {
  var POItem = sequelize.define("POItem", {
    poID: DataTypes.INTEGER,
    partNumber: DataTypes.STRING,
    partName: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    unitPrice: DataTypes.FLOAT,
    currency: DataTypes.STRING
  }, {});

  POItem.associate = function (models) {
    POItem.belongsTo(models.PO, {
      sourceKey: "id",
      foreignKey: "poID"
    });
  };

  return POItem;
};
//# sourceMappingURL=poitem.js.map