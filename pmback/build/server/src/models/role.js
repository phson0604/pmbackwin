'use strict';

module.exports = function (sequelize, DataTypes) {
  var Role = sequelize.define('Role', {
    scope: DataTypes.STRING,
    role: DataTypes.STRING,
    description: DataTypes.STRING,
    descriptionVN: DataTypes.STRING
  }, {});

  Role.associate = function (models) {// associations can be defined here
  };

  return Role;
};
//# sourceMappingURL=role.js.map