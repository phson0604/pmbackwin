"use strict";

module.exports = function (sequelize, DataTypes) {
  var Customer = sequelize.define("Customer", {
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    contact: DataTypes.STRING
  }, {});

  Customer.associate = function (models) {
    Customer.hasMany(models.PO, {
      sourceKey: "id",
      foreignKey: "customerID"
    });
  };

  return Customer;
};
//# sourceMappingURL=customer.js.map