'use strict';

module.exports = function (sequelize, DataTypes) {
  var ApprovalStatus = sequelize.define('ApprovalStatus', {
    description: DataTypes.STRING,
    descriptionVN: DataTypes.STRING
  }, {});

  ApprovalStatus.associate = function (models) {// associations can be defined here
  };

  return ApprovalStatus;
};
//# sourceMappingURL=approvalstatus.js.map