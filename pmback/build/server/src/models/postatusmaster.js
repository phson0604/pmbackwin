'use strict';

module.exports = function (sequelize, DataTypes) {
  var POStatusMaster = sequelize.define('POStatusMaster', {
    description: DataTypes.STRING,
    descriptionVN: DataTypes.STRING
  }, {});

  POStatusMaster.associate = function (models) {// associations can be defined here
  };

  return POStatusMaster;
};
//# sourceMappingURL=postatusmaster.js.map