"use strict";

module.exports = function (sequelize, DataTypes) {
  var POPIC = sequelize.define("POPIC", {
    poID: DataTypes.INTEGER,
    picID: DataTypes.INTEGER,
    roles: DataTypes.STRING
  }, {});

  POPIC.associate = function (models) {
    POPIC.belongsTo(models.PO, {
      sourceKey: "id",
      foreignKey: "poID"
    });
    POPIC.belongsTo(models.User, {
      sourceKey: "id",
      foreignKey: "picID"
    });
  };

  return POPIC;
};
//# sourceMappingURL=popic.js.map