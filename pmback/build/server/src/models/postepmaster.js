'use strict';

module.exports = function (sequelize, DataTypes) {
  var POStepMaster = sequelize.define('POStepMaster', {
    sequence: DataTypes.INTEGER,
    stepName: DataTypes.STRING
  }, {});

  POStepMaster.associate = function (models) {// associations can be defined here
  };

  return POStepMaster;
};
//# sourceMappingURL=postepmaster.js.map