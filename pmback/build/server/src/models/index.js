"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _path = _interopRequireDefault(require("path"));

var _config = _interopRequireDefault(require("../config/config"));

// import Sequelize from "sequelize";
var Sequelize = require("sequelize");

var basename = _path["default"].basename(__filename);

var env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
var config = _config["default"][env];
console.log("this is the environment: ", env);
console.log("actual config: ", config);
var db = {};
var sequelize;

if (env === "production") {
  //this is to persist connection to Heroku Postgres
  //because heroku will change db credential, and update to DATABASE_URL environment variable
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

_fs["default"].readdirSync(__dirname).filter(function (file) {
  return file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js";
}).forEach(function (file) {
  var model = sequelize["import"](_path["default"].join(__dirname, file));
  db[model.name] = model;
});

Object.keys(db).forEach(function (modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});
db.sequelize = sequelize;
db.Sequelize = Sequelize;
var _default = db;
exports["default"] = _default;
//# sourceMappingURL=index.js.map