'use strict';

module.exports = function (sequelize, DataTypes) {
  var Supplier = sequelize.define('Supplier', {
    name: DataTypes.STRING,
    address: DataTypes.STRING,
    contact: DataTypes.STRING
  }, {});

  Supplier.associate = function (models) {// associations can be defined here
  };

  return Supplier;
};
//# sourceMappingURL=supplier.js.map