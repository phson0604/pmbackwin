"use strict";

module.exports = function (sequelize, DataTypes) {
  var POStep = sequelize.define("POStep", {
    poID: DataTypes.INTEGER,
    stepID: DataTypes.INTEGER,
    status: DataTypes.INTEGER,
    priority: DataTypes.STRING,
    photoURLs: DataTypes.STRING,
    comment: DataTypes.STRING
  }, {});

  POStep.associate = function (models) {
    // associations can be defined here
    POStep.belongsTo(models.PO, {
      foreignKey: "poID"
    });
  };

  return POStep;
};
//# sourceMappingURL=postep.js.map