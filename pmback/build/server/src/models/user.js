// var Joi = require("joi-full");
"use strict";

var _this = void 0;

module.exports = function (sequelize, DataTypes) {
  var User = sequelize.define("User", {
    uuid: {
      allowNull: false,
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4
    },
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    displayName: DataTypes.STRING,
    tel: DataTypes.STRING,
    address: DataTypes.STRING,
    lastLogin: DataTypes.DATE
  }, {});

  User.associate = function (models) {
    // User.belongsToMany(models.User, { through: models.POPIC });
    User.hasMany(models.POPIC, {
      sourceKey: "id",
      foreignKey: "picID"
    });
  }; //Instance method, to return jwt of obj


  User.prototype.generateAuthToken = function () {
    var token = jwt.sign({
      email: _this.email
    }, jwtPrivateKey);
    return token;
  }; // validate data before call db.saving
  // User.prototype.validate = () => {
  //   const schema = {
  //     email: Joi.string().min(5).max(50).required().email(),
  //     password: Joi.string().min(5).max(50).required(),
  //     displayName: Joi.required(),
  //   };
  //   return Joi.validate(this, schema);
  // };


  return User;
};
//# sourceMappingURL=user.js.map