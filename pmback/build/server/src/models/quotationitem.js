"use strict";

module.exports = function (sequelize, DataTypes) {
  var QuotationItem = sequelize.define("QuotationItem", {
    quotationID: DataTypes.INTEGER,
    partNumber: DataTypes.STRING,
    partName: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    unitPrice: DataTypes.INTEGER,
    currency: DataTypes.STRING
  }, {});

  QuotationItem.associate = function (models) {
    QuotationItem.belongsTo(models.Quotation, {
      sourceKey: "id",
      foreignKey: "quotationID"
    });
  };

  return QuotationItem;
};
//# sourceMappingURL=quotationitem.js.map