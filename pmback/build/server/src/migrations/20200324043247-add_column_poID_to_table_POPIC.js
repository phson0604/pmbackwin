"use strict";

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Promise.all([queryInterface.addColumn("POPICs", "poID", {
        type: Sequelize.STRING
      }, {
        transaction: t
      })]);
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Promise.all([queryInterface.removeColumn("POPICs", "poID", {
        transaction: t
      })]);
    });
  }
};
//# sourceMappingURL=20200324043247-add_column_poID_to_table_POPIC.js.map