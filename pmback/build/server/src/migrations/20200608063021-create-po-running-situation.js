'use strict';

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.createTable('PORunningSituations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poNumber: {
        type: Sequelize.STRING
      },
      stepID: {
        type: Sequelize.INTEGER
      },
      statusID: {
        type: Sequelize.STRING
      },
      priority: {
        type: Sequelize.STRING
      },
      photoURLs: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.dropTable('PORunningSituations');
  }
};
//# sourceMappingURL=20200608063021-create-po-running-situation.js.map