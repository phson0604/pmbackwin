"use strict";

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";').then(function () {
      return queryInterface.addColumn("Users", "uuid", {
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal("uuid_generate_v4()"),
        allowNull: false
      });
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.removeColumn("Users", "uuid");
  }
};
//# sourceMappingURL=20200325065817-userWithUUIDColumn.js.map