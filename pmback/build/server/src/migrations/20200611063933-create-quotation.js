'use strict';

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.createTable('Quotations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poID: {
        type: Sequelize.INTEGER
      },
      supplierID: {
        type: Sequelize.INTEGER
      },
      issuedDate: {
        type: Sequelize.DATE
      },
      validUntil: {
        type: Sequelize.DATE
      },
      approved: {
        type: Sequelize.BOOLEAN
      },
      approvedBy: {
        type: Sequelize.INTEGER
      },
      approvedTime: {
        type: Sequelize.DATE
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.dropTable('Quotations');
  }
};
//# sourceMappingURL=20200611063933-create-quotation.js.map