"use strict"; // import { Sequelize } from "sequelize";

module.exports = {
  up: function up(queryInterface, Sequelize) {
    /*
      Add altering commands here.
      Return a promise to correctly handle asynchronicity.
       Example:
      */
    return queryInterface.createTable("user2", {
      id: {
        primaryKey: true,
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
      email: Sequelize.STRING,
      password: Sequelize.STRING
    });
  },
  down: function down(queryInterface, Sequelize) {
    /*
      Add reverting commands here.
      Return a promise to correctly handle asynchronicity.
       Example:*/
    return queryInterface.dropTable("user2");
  }
};
//# sourceMappingURL=20200325063901-newTableWithUUID.js.map