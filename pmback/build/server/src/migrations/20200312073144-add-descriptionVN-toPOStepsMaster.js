"use strict";

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Promise.all([queryInterface.addColumn("POStepMasters", "stepNameVN", {
        type: Sequelize.STRING
      }, {
        transaction: t
      })]);
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.sequelize.transaction(function (t) {
      return Promise.all([queryInterface.removeColumn("POStepMasters", "stepNameVN", {
        transaction: t
      })]);
    });
  }
};
//# sourceMappingURL=20200312073144-add-descriptionVN-toPOStepsMaster.js.map