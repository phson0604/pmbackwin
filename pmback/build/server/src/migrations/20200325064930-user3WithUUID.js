"use strict"; // import { Sequelize } from "sequelize";

module.exports = {
  up: function up(queryInterface, Sequelize) {
    return queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";').then(function () {
      return queryInterface.addColumn("user2", "uuid", {
        type: Sequelize.UUID,
        defaultValue: Sequelize.literal("uuid_generate_v4()"),
        allowNull: false
      });
    });
  },
  down: function down(queryInterface, Sequelize) {
    return queryInterface.removeColumn("user2", "uuid");
  }
};
//# sourceMappingURL=20200325064930-user3WithUUID.js.map