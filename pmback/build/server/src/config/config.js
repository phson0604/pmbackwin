"use strict";

require("dotenv").config();

module.exports = {
  // If using online database
  // development: {
  //   use_env_variable: "DATABASE_URL",
  // },
  // development: {
  //   use_env_variable: "DATABASE_URL",
  //   // jwtPrivateKey: "jwtPrivateKey",
  //   dialect: "postgres",
  // },
  development: {
    database: "ERP",
    username: "postgres",
    password: 123456,
    host: "127.0.0.1",
    port: 5433,
    dialect: "postgres"
  },
  test: {
    database: "ERP",
    username: "postgres",
    password: 123456,
    host: "127.0.0.1",
    port: 5433,
    dialect: "postgres"
  },
  // production: {
  //   database: "d8cntti4usergh",
  //   username: "idsfrkrabzmosm",
  //   password: "2da49b468c1704000fdfe7f8c3d67f6ca7985820af620cebeb7ffd1edcefbec1",
  //   host: "ec2-34-197-141-7.compute-1.amazonaws.com",
  //   port: 5432,
  //   dialect: "postgres",
  //   jwtPrivateKey: "jwtPrivateKey",
  // },
  production: {
    use_env_variable: "DATABASE_URL",
    // jwtPrivateKey: "jwtPrivateKey",
    dialect: "postgres"
  }
};
//# sourceMappingURL=config.js.map