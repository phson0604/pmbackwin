// import express from "express";
import bodyParser from "body-parser";

// import cors from "./server/middleware/cors";

import quotationRouters from "./server/routers/QuotationRouters";
import quotationItemRouters from "./server/routers/QuotationItemRouters";
import customerRouters from "./server/routers/CustomerRouters";
import supplierRouters from "./server/routers/SupplierRouters";
import userRouters from "./server/routers/UserRouters";
import roleRouters from "./server/routers/RoleRouters";
import poRouters from "./server/routers/PORouters";
import poItemRouters from "./server/routers/POItemRouters";
import poStepMasterRouters from "./server/routers/POStepMasterRouters";
import poStatusMasterRouters from "./server/routers/POStatusMasterRouters";
import poStepRouters from "./server/routers/POStepRouters";
import poPICRouters from "./server/routers/POPICRouters";
import authRouters from "./server/routers/AuthRouters";
import dailyRouters from "./server/routers/DaiyReportsRoute";
import inventoryRouters from "./server/routers/InventoryRouters";
import stockInRouters from "./server/routers/StockInRouters";
import stockOutRouters from "./server/routers/StockOutRouters";
const express = require("express");
const app = express();
require("./server/startup/prod")(app);

// console.log(`NODE_ENV: ${process.env.NODE_ENV}`);
// console.log(`app: ${app.get("env")}`);

const helmet = require("helmet");
app.use(helmet());

const cors = require("cors");
app.use(cors());

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

if (app.get("env") === "development") {
  const morgan = require("morgan");
  app.use(morgan("tiny"));
}

app.use("/api/v1/POs", poRouters);
app.use("/api/v1/POPICs", poPICRouters);
app.use("/api/v1/POItems", poItemRouters);
app.use("/api/v1/Quotations", quotationRouters);
app.use("/api/v1/QuotationItems", quotationItemRouters);
app.use("/api/v1/Customers", customerRouters);
app.use("/api/v1/Suppliers", supplierRouters);
app.use("/api/v1/POStepMasters", poStepMasterRouters);
app.use("/api/v1/POStatusMasters", poStatusMasterRouters);
app.use("/api/v1/POSteps", poStepRouters);
app.use("/api/v1/Users", userRouters);
app.use("/api/v1/Roles", roleRouters);
app.use("/api/v1/Auth", authRouters);
app.use("/api/v1/DailyReports", dailyRouters);
app.use("/api/v1/Inventorys",inventoryRouters);
app.use("/api/v1/StockIns",stockInRouters);
app.use("/api/v1/StockOuts",stockOutRouters);
app.get("*", (req, res) =>
  res.status(200).send({
    message: "Welcome/Xin chao/Ciao to this API.",
  })
);

const port = process.env.PORT || 8000;

app.listen(port, () => {
  console.log(`Server is running on PORT ${port}`);
});
export default app;
