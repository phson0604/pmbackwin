import POStatusMasterService from "../services/POStatusMasterService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");
const util = new Util();

class POStatusMasterController {
  static async getAllPOStatusMasters(req, res) {
    try {
      const allPOStatusMasters = await POStatusMasterService.getAllPOStatusMasters();
      if (allPOStatusMasters.length > 0) {
        util.setSuccess(
          HTTP_CODE.OK,
          "POStatusMasters retrieved",
          allPOStatusMasters
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "No POStatusMaster found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addPOStatusMaster(req, res) {
    if (!req.body.description) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newPOStatusMaster = req.body;
    try {
      const createdPOStatusMaster = await POStatusMasterService.addPOStatusMaster(
        newPOStatusMaster
      );
      util.setSuccess(
        HTTP_CODE.CREATED,
        "POStatusMaster Added!",
        createdPOStatusMaster
      );
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedPOStatusMaster(req, res) {
    const alteredPOStatusMaster = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updatePOStatusMaster = await POStatusMasterService.updatePOStatusMaster(
        id,
        alteredPOStatusMaster
      );
      if (!updatePOStatusMaster) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POStatusMaster with the id: ${id}`
        );
      } else {
        util.setSuccess(
          HTTP_CODE.OK,
          "POStatusMaster updated",
          updatePOStatusMaster
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAPOStatusMaster(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePOStatusMaster = await POStatusMasterService.getAPOStatusMaster(
        id
      );

      if (!thePOStatusMaster) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POStatusMaster with the id ${id}`
        );
      } else {
        util.setSuccess(
          HTTP_CODE.OK,
          "Found POStatusMaster",
          thePOStatusMaster
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deletePOStatusMaster(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const quotationToDelete = await POStatusMasterService.deletePOStatusMaster(
        id
      );

      if (quotationToDelete) {
        util.setSuccess(HTTP_CODE.OK, "POStatusMaster deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `POStatusMaster with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default POStatusMasterController;
