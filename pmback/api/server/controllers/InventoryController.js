import InventoryService from "../services/InventoryService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class InventoryController {
  static async getAllInventoryss(req, res) {
    try {
      const allInventorys = await InventoryService.getAllInventorys();
      if (allInventorys.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Item retrieved", allInventorys);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Item found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addInventory(req, res) {
    const newInventory = req.body;
    try {
      const createdInventory = await InventoryService.addInventory(
        newInventory
      );
      util.setSuccess(HTTP_CODE.CREATED, "Item Added!", createdInventory);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedInventory(req, res) {
    const alteredCustomer = req.body;
    const { partNumber } = req.params;
    if (!String(partNumber)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateCustomer = await InventoryService.updateItemInventory(
        partNumber,
        alteredCustomer
      );
      if (!updateCustomer) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Item with the id: ${partNumber}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Item updated", updateCustomer);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getOneInventory(req, res) {
    const { partNumber } = req.params;

    if (!String(partNumber)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theInventory = await InventoryService.getOneInventorys(partNumber);

      if (!theInventory) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Item with the id ${partNumber}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Item", theInventory);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAllItemInventorys(req, res) {
    try {
      const allItemInventorys = await InventoryService.GetAllItemInventorys();
      if (allItemInventorys.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Item retrieved", allItemInventorys);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Item found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default InventoryController;
