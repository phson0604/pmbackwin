import UserService from "../services/UserService";
import Util from "../utils/Utils";
import configJson from "../src/config/config";

const jwt = require("jsonwebtoken");
const _ = require("lodash");

const util = new Util();
const HTTP_CODE = require("../utils/HttpStatusCode");

const env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
const config = configJson[env];
const jwtPrivateKey =
  env === "production" ? process.env["jwtPrivateKey"] : config.jwtPrivateKey;

//avoid return password
const RETURNABLE_COLUMNS = [
  "id",
  "uuid",
  "email",
  "displayName",
  "tel",
  "address",
  "lastLogin",
];
class UserController {
  static async getAllUsers(req, res) {
    try {
      const allUsers = await UserService.getAllUsers();
      if (allUsers.length > 0) {
        util.setSuccess(
          HTTP_CODE.OK,
          "Users retrieved",
          _.map(allUsers, _.partialRight(_.pick, RETURNABLE_COLUMNS))
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "No User found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAUser(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theUser = await UserService.getAUser(id);

      if (!theUser) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find User with the id ${id}`
        );
      } else {
        util.setSuccess(
          HTTP_CODE.OK,
          "Found User",
          _.pick(theUser, RETURNABLE_COLUMNS)
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getOneUsers(req, res) {
    const { name } = req.params;

    if (!String(name)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid name value"
      );
      return util.send(res);
    }

    try {
      const theUser = await UserService.getOneUser(name);

      if (!theUser) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find User with the id ${name}`
        );
      } else {
        util.setSuccess(
          HTTP_CODE.OK,
          "Found User",
          _.pick(theUser, RETURNABLE_COLUMNS)
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }


  static async addUser(req, res) {
    //Validate
    if (!req.body.email || !req.body.displayName || !req.body.password) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newUser = req.body;

    //Check if user registered
    //  (should await here because UserService return a promise)
    if (await UserService.findUser(newUser)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "User already registered");
      return util.send(res);
    }

    try {
      const createdUser = await UserService.addUser(newUser);

      // generate jwt token
      const jwtToken = jwt.sign(
        { name: newUser.displayName, email: newUser.email },
        jwtPrivateKey
      );

      util.setSuccess(
        HTTP_CODE.CREATED,
        "User Added!",
        _.pick(createdUser, RETURNABLE_COLUMNS)
      );

      //send jwt to client as in response header
      return util.send(res.header("x-auth-token", jwtToken));
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedUser(req, res) {
    const alteredUser = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const updateUser = await UserService.updateUser(id, alteredUser);
      if (!updateUser) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find User with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "User updated", updateUser);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deleteUser(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const userToDelete = await UserService.deleteUser(id);

      if (userToDelete) {
        util.setSuccess(HTTP_CODE.OK, "User deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `User with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default UserController;
