import CustomerService from "../services/CustomerService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class CustomerController {
  static async getAllCustomers(req, res) {
    try {
      const allCustomers = await CustomerService.getAllCustomers();
      if (allCustomers.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Customers retrieved", allCustomers);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Customer found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addCustomer(req, res) {
    if (!req.body.name || !req.body.address || !req.body.contact) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newCustomer = req.body;
    try {
      const createdCustomer = await CustomerService.addCustomer(newCustomer);
      util.setSuccess(HTTP_CODE.CREATED, "Customer Added!", createdCustomer);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedCustomer(req, res) {
    const alteredCustomer = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateCustomer = await CustomerService.updateCustomer(
        id,
        alteredCustomer
      );
      if (!updateCustomer) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Customer with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Customer updated", updateCustomer);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getACustomer(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theCustomer = await CustomerService.getACustomer(id);

      if (!theCustomer) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Customer with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Customer", theCustomer);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deleteCustomer(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const customerToDelete = await CustomerService.deleteCustomer(id);

      if (customerToDelete) {
        util.setSuccess(HTTP_CODE.OK, "Customer deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Customer with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default CustomerController;
