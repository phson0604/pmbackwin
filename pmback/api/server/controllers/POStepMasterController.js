import POStepMasterService from "../services/POStepMasterService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");
const util = new Util();

class POStepMasterController {
  static async getAllPOStepMasters(req, res) {
    try {
      const allPOStepMasters = await POStepMasterService.getAllPOStepMasters();
      if (allPOStepMasters.length > 0) {
        util.setSuccess(
          HTTP_CODE.OK,
          "POStepMasters retrieved",
          allPOStepMasters
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "No POStepMaster found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addPOStepMaster(req, res) {
    if (!req.body.sequence || !req.body.stepName) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newPOStepMaster = req.body;
    try {
      const createdPOStepMaster = await POStepMasterService.addPOStepMaster(
        newPOStepMaster
      );
      util.setSuccess(
        HTTP_CODE.CREATED,
        "POStepMaster Added!",
        createdPOStepMaster
      );
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedPOStepMaster(req, res) {
    const alteredPOStepMaster = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updatePOStepMaster = await POStepMasterService.updatePOStepMaster(
        id,
        alteredPOStepMaster
      );
      if (!updatePOStepMaster) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POStepMaster with the id: ${id}`
        );
      } else {
        util.setSuccess(
          HTTP_CODE.OK,
          "POStepMaster updated",
          updatePOStepMaster
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAPOStepMaster(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePOStepMaster = await POStepMasterService.getAPOStepMaster(id);

      if (!thePOStepMaster) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POStepMaster with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found POStepMaster", thePOStepMaster);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deletePOStepMaster(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const quotationToDelete = await POStepMasterService.deletePOStepMaster(
        id
      );

      if (quotationToDelete) {
        util.setSuccess(HTTP_CODE.OK, "POStepMaster deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `POStepMaster with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default POStepMasterController;
