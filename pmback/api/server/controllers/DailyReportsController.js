import DailyReportsService from "../services/DailyReportsService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class DailyReportsController {
  static async getAllReports(req, res) {
    try {
      const allReports = await DailyReportsService.getAllReport();
      if (allReports.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Report retrieved", allReports);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Report found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
  static async addReport(req, res) {
    if (!req.body.plan || !req.body.picId) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newReport = req.body;

    try {
      const createdReport = await DailyReportsService.addReport(newReport);
      util.setSuccess(HTTP_CODE.CREATED, "Report Added!", createdReport);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }
  static async updatedReport(req, res) {
    const alteredReport = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateReport = await DailyReportsService.updateDailyReport(
        id,
        alteredReport
      );
      if (!updateReport) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Reports with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Report updated", updateReport);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }
  static async getAReport(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theReport = await DailyReportsService.getAReport(id);

      if (!theReport) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find report with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Report", theReport);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAllPicIdReport(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theReport = await DailyReportsService.getAllReportPicId(id);

      if (!theReport) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find report with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Report", theReport);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }
  static async deleteReport(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const ReportToDelete = await DailyReportsService.deleteReport(id);

      if (ReportToDelete) {
        util.setSuccess(HTTP_CODE.OK, "Report deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Report with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}
export default DailyReportsController;
