import StockInService from "../services/StockInService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class StockInController {
  static async getAllStockIns(req, res) {
    try {
      const allStockIn = await StockInService.getAllStockIns();
      if (allStockIn.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Stock in retrieved", allStockIn);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Stock in found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

 
    static async getAllItemStockIn(req, res) {
      try {
        const allStockIn = await StockInService.getAllItemStockIn();
        if (allStockIn.length > 0) {
          util.setSuccess(HTTP_CODE.OK, "Stock in retrieved", allStockIn);
        } else {
          util.setSuccess(HTTP_CODE.OK, "No Stock in found");
        }
        return util.send(res);
      } catch (error) {
        util.setError(HTTP_CODE.BAD_REQUEST, error);
        return util.send(res);
      }
    }

  static async getOneStockIn(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theStockIn = await StockInService.getOneStockIn(id);

      if (!theStockIn) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Stock in with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Stock in", theStockIn);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getOneItemStockIn(req, res) {
    const { partNumber } = req.params;

    //if (!String(partNumber)) {
    //  util.setError(
     //   HTTP_CODE.BAD_REQUEST,
      //  "Please input a valid numeric value"
     // );
     // return util.send(res);
   // }

    try {
      const theStockIn = await StockInService.getOneItemStockIn(partNumber);

      if (!theStockIn) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Stock in with the id ${partNumber}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Stock in", theStockIn);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAllItemStockIns(req, res) {
    const { receiptNo } = req.params;

    try {
      const allStockIn = await StockInService.getAllItemStockIns(receiptNo);
      if (allStockIn.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Stock in retrieved", allStockIn);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Stock in found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllItemQuationStockIns(req, res) {
    const { quotationID } = req.params;

    try {
      const allStockIn = await StockInService.getAllItemQuationStockIns(quotationID);
      if (allStockIn.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Stock in retrieved", allStockIn);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Stock in found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }


  static async addStockIn(req, res) {
    const newStockIn = req.body;
    try {
      const createdStockIn = await StockInService.addNewStockIn(newStockIn);
      util.setSuccess(HTTP_CODE.CREATED, "Stock in Added!", createdStockIn);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async deleteStockIn(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const StockInToDelete = await StockInService.deleteStockIn(id);

      if (StockInToDelete) {
        util.setSuccess(HTTP_CODE.OK, "Stock in deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Stock in with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default StockInController;
