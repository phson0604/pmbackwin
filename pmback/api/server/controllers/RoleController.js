import RoleService from "../services/RoleService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class RoleController {
  static async getAllRoles(req, res) {
    try {
      const allRoles = await RoleService.getAllRoles();
      if (allRoles.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Roles retrieved", allRoles);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Role found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addRole(req, res) {
    if (!req.body.scope || !req.body.role || !req.body.description) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newRole = req.body;
    try {
      const createdRole = await RoleService.addRole(newRole);
      util.setSuccess(HTTP_CODE.CREATED, "Role Added!", createdRole);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedRole(req, res) {
    const alteredRole = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateRole = await RoleService.updateRole(id, alteredRole);
      if (!updateRole) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Role with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Role updated", updateRole);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getARole(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theRole = await RoleService.getARole(id);

      if (!theRole) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Role with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Role", theRole);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deleteRole(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const userToDelete = await RoleService.deleteRole(id);

      if (userToDelete) {
        util.setSuccess(HTTP_CODE.OK, "Role deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Role with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default RoleController;
