import QuotationItemService from "../services/QuotationItemService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class QuotationItemController {
  static async getAllQuotationItems(req, res) {
    try {
      const allQuotationItems = await QuotationItemService.getAllQuotationItems(
        req.query
      );
      if (allQuotationItems.length > 0) {
        util.setSuccess(
          HTTP_CODE.OK,
          "QuotationItems retrieved",
          allQuotationItems
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "No QuotationItem found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllQuotationItemss(req, res) {
    const { id } = req.params;

    try {
      const theQuotationItem = await QuotationItemService.getAllQuotationItem(id);

      if (!theQuotationItem) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find QuotationItem with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found QuotationItem", theQuotationItem);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async addQuotationItem(req, res) {
    if (
      !req.body.quotationID ||
      !req.body.partNumber ||
      !req.body.partName ||
      !req.body.quantity ||
      !req.body.unitPrice ||
      !req.body.currency
    ) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newQuotationItem = req.body;
    try {
      const createdQuotationItem = await QuotationItemService.addQuotationItem(
        newQuotationItem
      );
      util.setSuccess(
        HTTP_CODE.CREATED,
        "QuotationItem Added!",
        createdQuotationItem
      );
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedQuotationItem(req, res) {
    const alteredQuotationItem = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateQuotationItem = await QuotationItemService.updateQuotationItem(
        id,
        alteredQuotationItem
      );
      if (!updateQuotationItem) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find QuotationItem with the id: ${id}`
        );
      } else {
        util.setSuccess(
          HTTP_CODE.OK,
          "QuotationItem updated",
          updateQuotationItem
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAQuotationItem(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theQuotationItem = await QuotationItemService.getAQuotationItem(id);

      if (!theQuotationItem) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find QuotationItem with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found QuotationItem", theQuotationItem);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deleteQuotationItem(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const poToDelete = await QuotationItemService.deleteQuotationItem(id);

      if (poToDelete) {
        util.setSuccess(HTTP_CODE.OK, "QuotationItem deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `QuotationItem with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default QuotationItemController;
