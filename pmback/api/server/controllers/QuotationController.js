import QuotationService from "../services/QuotationService";
import Util from "../utils/Utils";
import QuotationItemService from "./../services/QuotationItemService";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class QuotationController {
  static async getAllQuotations(req, res) {
    try {
      const allQuotations = await QuotationService.getAllQuotations(req.query);
      if (allQuotations.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Quotations retrieved", allQuotations);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Quotation found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllSuppliesQuotations(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theQuotation = await QuotationService.getAllSuppliesQuotations(id);

      if (!theQuotation) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Quotation with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Quotation", theQuotation);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAllQuotation(req, res) {
    try {
      const allQuotations = await QuotationService.getAllQuotation(req.query);
      if (allQuotations.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Quotations retrieved", allQuotations);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Quotation found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addQuotation(req, res) {
    if (
      !req.body.poID ||
      !req.body.supplierID ||
      !req.body.issuedDate ||
      !req.body.validUntil
    ) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newQuotation = req.body;
    try {
      const createdQuotation = await QuotationService.addQuotation(
        newQuotation
      );
      util.setSuccess(HTTP_CODE.CREATED, "Quotation Added!", createdQuotation);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedQuotation(req, res) {
    const alteredQuotation = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateQuotation = await QuotationService.updateQuotation(
        id,
        alteredQuotation
      );
      if (!updateQuotation) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Quotation with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Quotation updated", updateQuotation);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAQuotation(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theQuotation = await QuotationService.getAQuotation(id);

      if (!theQuotation) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Quotation with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Quotation", theQuotation);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deleteQuotation(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const quotationToDelete = await QuotationService.deleteQuotation(id);

      if (quotationToDelete) {
        await QuotationItemService.deleteAllItemOfQuotation(id);

        util.setSuccess(HTTP_CODE.OK, "Quotation deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Quotation with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default QuotationController;
