import POPICService from "../services/POPICService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");
const util = new Util();

class POPICController {
  static async getAllPOPICs(req, res) {
    try {
      const allPOPICs = await POPICService.getAllPOPICs(req.query);
      if (allPOPICs.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "POPICs retrieved", allPOPICs);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No POPIC found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  // static async getPOPICbyPOID(req, res) {
  //   const { poID } = req.params;

  //   if (!Number(poID)) {
  //     util.setError(
  //       HTTP_CODE.BAD_REQUEST,
  //       "Please input a valid numeric value"
  //     );
  //     return util.send(res);
  //   }

  //   try {
  //     const picsOfPO = await POPICService.getPOPICbyPOID(poID);
  //     if (picsOfPO.length > 0) {
  //       util.setSuccess(HTTP_CODE.OK, "POPICs retrieved", picsOfPO);
  //     } else {
  //       util.setSuccess(HTTP_CODE.OK, "No POPIC found");
  //     }
  //     return util.send(res);
  //   } catch (error) {
  //     util.setError(HTTP_CODE.BAD_REQUEST, error);
  //     return util.send(res);
  //   }
  // }

  static async getAPOPIC(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePOPIC = await POPICService.getAPOPIC(id);

      if (!thePOPIC) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POPIC with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found POPIC", thePOPIC);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async addPOPIC(req, res) {
    console.log(req.body);
    if (!req.body.poID || !req.body.picID || !req.body.roles) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newPOPIC = req.body;
    try {
      const createdPOPIC = await POPICService.addPOPIC(newPOPIC);

      util.setSuccess(HTTP_CODE.CREATED, "POPIC Added!", createdPOPIC);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedPOPIC(req, res) {
    const alteredPOPIC = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updatePOPIC = await POPICService.updatePOPIC(id, alteredPOPIC);
      if (!updatePOPIC) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POPIC with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "POPIC updated", updatePOPIC);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deletePOPIC(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const poToDelete = await POPICService.deletePOPIC(id);

      if (poToDelete) {
        util.setSuccess(HTTP_CODE.OK, "POPIC deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `POPIC with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default POPICController;
