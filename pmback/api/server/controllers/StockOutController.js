import StockOutService from "../services/StockOutService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class StockOutController {
  static async getAllStockOuts(req, res) {
    try {
      const allStockOut = await StockOutService.getAllStockOuts();
      if (allStockOut.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Stock out retrieved", allStockOut);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Stock out found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllItemStockOut(req, res) {
    try {
      const allStockOut = await StockOutService.getAllItemStockOut();
      if (allStockOut.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Stock out retrieved", allStockOut);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Stock out found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllItemStockOuts(req, res) {
    const { receiptNo } = req.params;

    try {
      const allStockOut = await StockOutService.getAllItemStockOuts(receiptNo);
      if (allStockOut.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Stock out retrieved", allStockOut);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Stock out found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getOneStockOut(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theStockOut = await StockOutService.getOneStockOut(id);

      if (!theStockOut) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find stock out with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found stock out", theStockOut);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }


  static async getOneItemStockOut(req, res) {
    const { partNumber } = req.params;

    //if (!String(partNumber)) {
    //  util.setError(
     //   HTTP_CODE.BAD_REQUEST,
      //  "Please input a valid numeric value"
     // );
     // return util.send(res);
   // }

    try {
      const theStockOut = await StockOutService.getOneItemStockOut(partNumber);

      if (!theStockOut) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Stock out with the id ${partNumber}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Stock out", theStockOut);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async addStockOut(req, res) {
    const newStockOut = req.body;
    try {
      const createdStockOut = await StockOutService.addNewStockOut(newStockOut);
      util.setSuccess(HTTP_CODE.CREATED, "Stock out Added!", createdStockOut);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async deleteStockOut(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const StockOutToDelete = await StockOutService.deleteStockOut(id);

      if (StockOutToDelete) {
        util.setSuccess(HTTP_CODE.OK, "Stock out deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Stock out with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default StockOutController;
