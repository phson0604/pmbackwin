import UserService from "../services/UserService";
import Util from "../utils/Utils";
import configJson from "../src/config/config";

const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const _ = require("lodash");

const util = new Util();
const HTTP_CODE = require("../utils/HttpStatusCode");

const env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
const config = configJson[env];
const jwtPrivateKey =
  env === "production" ? process.env["jwtPrivateKey"] : config.jwtPrivateKey;

//responsible for check login authenticator
class AuthController {
  static async authenticateUser(req, res) {
    try {
      let { dataValues: user } = await UserService.findUser(req.body);
      if (!user) {
        util.setError(HTTP_CODE.BAD_REQUEST, "Invalid email or password.");
        return util.send(res);
      }

      const validPassword = await bcrypt.compare(
        req.body.password,
        user.password
      );
      if (!validPassword) {
        util.setError(HTTP_CODE.BAD_REQUEST, "Invalid email or password.");
        return util.send(res);
      }

      // generate jwt token
      const jwtToken = jwt.sign(
        { name: user.displayName, email: user.email ,id:user.id},
        jwtPrivateKey
      );
      util.setSuccess(HTTP_CODE.OK, jwtToken);

      //send jwt to client as in response header
      //   return util.send(res.header("x-auth-token", jwtToken));
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default AuthController;
