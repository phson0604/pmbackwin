import SupplierService from "../services/SupplierService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class SupplierController {
  static async getAllSuppliers(req, res) {
    try {
      const allSuppliers = await SupplierService.getAllSuppliers();
      if (allSuppliers.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "Suppliers retrieved", allSuppliers);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No Supplier found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async addSupplier(req, res) {
    if (!req.body.name || !req.body.address || !req.body.contact) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newSupplier = req.body;
    try {
      const createdSupplier = await SupplierService.addSupplier(newSupplier);
      util.setSuccess(HTTP_CODE.CREATED, "Supplier Added!", createdSupplier);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedSupplier(req, res) {
    const alteredSupplier = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updateSupplier = await SupplierService.updateSupplier(
        id,
        alteredSupplier
      );
      if (!updateSupplier) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Supplier with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Supplier updated", updateSupplier);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getASupplier(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const theSupplier = await SupplierService.getASupplier(id);

      if (!theSupplier) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Supplier with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Supplier", theSupplier);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deleteSupplier(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const supplierToDelete = await SupplierService.deleteSupplier(id);

      if (supplierToDelete) {
        util.setSuccess(HTTP_CODE.OK, "Supplier deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Supplier with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default SupplierController;
