import POItemService from "../services/POItemService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");

const util = new Util();

class POItemController {
  static async getAllPOItems(req, res) {
    console.log("POItem query:", req.query);
    try {
      const allPOItems = await POItemService.getAllPOItems(req.query);
      if (allPOItems.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "POItems retrieved", allPOItems);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No POItem found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllPOItem(req, res) {
    const { id } = req.params;

    try {
      const thePOItem = await POItemService.getAllPOItem(id);

      if (!thePOItem) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find QuotationItem with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found POItem", thePOItem);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async addPOItem(req, res) {
    if (
      !req.body.poID ||
      !req.body.partNumber ||
      !req.body.partName ||
      !req.body.quantity ||
      !req.body.unitPrice ||
      !req.body.currency
    ) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newPOItem = req.body;
    try {
      const createdPOItem = await POItemService.addPOItem(newPOItem);
      util.setSuccess(HTTP_CODE.CREATED, "POItem Added!", createdPOItem);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedPOItem(req, res) {
    const alteredPOItem = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updatePOItem = await POItemService.updatePOItem(id, alteredPOItem);
      if (!updatePOItem) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POItem with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "POItem updated", updatePOItem);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAPOItem(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePOItem = await POItemService.getAPOItem(id);

      if (!thePOItem) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POItem with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found POItem", thePOItem);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deletePOItem(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const poToDelete = await POItemService.deletePOItem(id);

      if (poToDelete) {
        util.setSuccess(HTTP_CODE.OK, "POItem deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `POItem with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default POItemController;
