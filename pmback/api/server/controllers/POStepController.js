import POStepService from "../services/POStepService";
import Util from "../utils/Utils";

const HTTP_CODE = require("../utils/HttpStatusCode");
const util = new Util();

class POStepController {
  static async getAllPOSteps(req, res) {
    try {
      const allPOSteps = await POStepService.getAllPOSteps(req.query);
      if (allPOSteps.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "POSteps retrieved", allPOSteps);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No POStep found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAPOStep(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePOStep = await POStepService.getAPOStep(id);

      if (!thePOStep) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POStep with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found POStep", thePOStep);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async addPOStep(req, res) {
    if (
      !req.body.poID ||
      !req.body.stepID ||
      !req.body.status ||
      !req.body.priority
    ) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newPOStep = req.body;
    try {
      const createdPOStep = await POStepService.addPOStep(newPOStep);
      util.setSuccess(HTTP_CODE.CREATED, "POStep Added!", createdPOStep);
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedPOStep(req, res) {
    const alteredPOStep = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updatePOStep = await POStepService.updatePOStep(id, alteredPOStep);
      if (!updatePOStep) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find POStep with the id: ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "POStep updated", updatePOStep);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deletePOStep(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const quotationToDelete = await POStepService.deletePOStep(id);

      if (quotationToDelete) {
        util.setSuccess(HTTP_CODE.OK, "POStep deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `POStep with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default POStepController;
