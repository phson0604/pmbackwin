import POService from "../services/POService";
import Util from "../utils/Utils";
import POStepService from "../services/POStepService";
import POItemService from "./../services/POItemService";

const HTTP_CODE = require("../utils/HttpStatusCode");
const util = new Util();

class POController {
  static async getAllPOs(req, res) {
    try {
      const allPOs = await POService.getAllPOs();
      if (allPOs.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "POs retrieved", allPOs);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No PO found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllPO(req, res) {
    try {
      const allPOs = await POService.getAllPO();
      if (allPOs.length > 0) {
        util.setSuccess(HTTP_CODE.OK, "POs retrieved", allPOs);
      } else {
        util.setSuccess(HTTP_CODE.OK, "No PO found");
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }

  static async getAllItemPoNumber(req, res) {
    const { poNumber } = req.params;


    try {
      const theReport = await POService.getAllPONumber(poNumber);

      if (!theReport) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find report with the id ${poNumber}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Report", theReport);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }
  

  static async getAllCustomerPOs(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePO = await POService.getAllCustomerPos(id);

      if (!thePO) {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `Cannot find Quotation with the id ${id}`
        );
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found Name Customer", thePO);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }


  static async addPO(req, res) {
    console.log(req.body);
    if (
      !req.body.poNumber ||
      !req.body.customerID ||
      !req.body.issuedDate ||
      !req.body.deliveryDate ||
      !req.body.paymentTerms
    ) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Missing required fields");
      return util.send(res);
    }
    const newPO = req.body;
    try {
      const createdPO = await POService.addPO(newPO);
      // console.log("new PO added:", createdPO);
      const createdPOSteps = await POStepService.createAllStepsOfNewPO(
        createdPO.id
      );

      util.setSuccess(
        HTTP_CODE.CREATED,
        "PO Added!",
        createdPO,
        createdPOSteps
      );
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error.message);
      return util.send(res);
    }
  }

  static async updatedPO(req, res) {
    const alteredPO = req.body;
    const { id } = req.params;
    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }
    try {
      const updatePO = await POService.updatePO(id, alteredPO);
      if (!updatePO) {
        util.setError(HTTP_CODE.NOT_FOUND, `Cannot find PO with the id: ${id}`);
      } else {
        util.setSuccess(HTTP_CODE.OK, "PO updated", updatePO);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async getAPO(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(
        HTTP_CODE.BAD_REQUEST,
        "Please input a valid numeric value"
      );
      return util.send(res);
    }

    try {
      const thePO = await POService.getAPO(id);

      if (!thePO) {
        util.setError(HTTP_CODE.NOT_FOUND, `Cannot find PO with the id ${id}`);
      } else {
        util.setSuccess(HTTP_CODE.OK, "Found PO", thePO);
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.NOT_FOUND, error);
      return util.send(res);
    }
  }

  static async deletePO(req, res) {
    const { id } = req.params;

    if (!Number(id)) {
      util.setError(HTTP_CODE.BAD_REQUEST, "Please provide a numeric value");
      return util.send(res);
    }

    try {
      const poToDelete = await POService.deletePO(id);

      if (poToDelete) {
        // console.log("poToDelete:", poToDelete);
        await POStepService.deleteAllStepsOfPO(id);
        await POItemService.deleteAllItemOfPO(id);

        util.setSuccess(HTTP_CODE.OK, "PO deleted");
      } else {
        util.setError(
          HTTP_CODE.NOT_FOUND,
          `PO with the id ${id} cannot be found`
        );
      }
      return util.send(res);
    } catch (error) {
      util.setError(HTTP_CODE.BAD_REQUEST, error);
      return util.send(res);
    }
  }
}

export default POController;
