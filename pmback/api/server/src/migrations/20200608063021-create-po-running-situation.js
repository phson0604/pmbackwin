'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PORunningSituations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poNumber: {
        type: Sequelize.STRING
      },
      stepID: {
        type: Sequelize.INTEGER
      },
      statusID: {
        type: Sequelize.STRING
      },
      priority: {
        type: Sequelize.STRING
      },
      photoURLs: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PORunningSituations');
  }
};