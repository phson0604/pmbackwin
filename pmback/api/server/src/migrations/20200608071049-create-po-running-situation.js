'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('PORunningSituations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poID: {
        type: Sequelize.INTEGER
      },
      stepID: {
        type: Sequelize.INTEGER
      },
      statusID: {
        type: Sequelize.INTEGER
      },
      priority: {
        type: Sequelize.STRING
      },
      photoURLs: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('PORunningSituations');
  }
};