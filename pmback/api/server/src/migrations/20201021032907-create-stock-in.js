'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('StockIns', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      receiptNo: {
        type: Sequelize.STRING
      },
      partNumber: {
        type: Sequelize.STRING
      },
      unit: {
        type: Sequelize.STRING
      },
      quantity: {
        type: Sequelize.INTEGER
      },
      quotationID: {
        type: Sequelize.INTEGER
      },
      date: {
        type: Sequelize.DATE
      },
      note: {
        type: Sequelize.STRING
      },
      actualNumber: {
        type: Sequelize.INTEGER
      },
      store: {
        type: Sequelize.STRING
      },
      position: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('StockIns');
  }
};