'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Quotations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      poNumber: {
        type: Sequelize.STRING
      },
      supplierID: {
        type: Sequelize.INTEGER
      },
      receivedDate: {
        type: Sequelize.DATE
      },
      validUntil: {
        type: Sequelize.DATE
      },
      approvalID: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Quotations');
  }
};