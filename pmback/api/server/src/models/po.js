"use strict";
import { Sequelize } from "sequelize";
module.exports = (sequelize, DataTypes) => {
  const PO = sequelize.define(
    "PO",
    {
      poNumber: {
        type: DataTypes.STRING,
        unique: true,
        allowNull: false,
      },
      customerID: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: "Customer",
          key: "id",
        },
        onUpdate: "CASCADE",
        onDelete: "SET NULL",
      },
      issuedDate: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
          isDate: true,
          notNull: true,
        },
      },
      deliveryDate: {
        type: DataTypes.DATE,
        allowNull: false,
        validate: {
          isDate: true,
          notNull: true,
        },
      },
      paymentTerms: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      poStatus: {
        type: DataTypes.STRING,
        allowNull: true,
      },
      createdBy: {
        type: DataTypes.STRING,
        allowNull: true,
      },
    },
    {}
  );
  PO.associate = function (models) {
    PO.belongsTo(models.Customer, { foreignKey: "customerID" });
    PO.hasMany(models.POPIC, { sourceKey: "id", foreignKey: "poID" });
    PO.hasMany(models.POItem, { sourceKey: "id", foreignKey: "poID" });
    PO.hasMany(models.Quotation, { sourceKey: "id", foreignKey: "poID" });
    
  };
  return PO;
};
