"use strict";
module.exports = (sequelize, DataTypes) => {
  const Quotation = sequelize.define(
    "Quotation",
    {
      poID: DataTypes.INTEGER,
      supplierID: DataTypes.INTEGER,
      issuedDate: DataTypes.DATE,
      validUntil: DataTypes.DATE,
      approved: DataTypes.BOOLEAN,
      approvedBy: DataTypes.INTEGER,
      approvedTime: DataTypes.DATE,
    },
    {}
  );
  Quotation.associate = function (models) {
    // associations can be defined here
    Quotation.belongsTo(models.PO, { foreignKey: "poID" });
    Quotation.belongsTo(models.Supplier, { foreignKey: "supplierID" });
    Quotation.hasMany(models.QuotationItem, {
      sourceKey: "id",
      foreignKey: "quotationID",
    });
    Quotation.hasMany(models.StockIn, {
      sourceKey: "id",
      foreignKey: "quotationID",
    });
  };
  return Quotation;
};
