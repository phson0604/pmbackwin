'use strict';
module.exports = (sequelize, DataTypes) => {
  const StockIn = sequelize.define('StockIn', {
    receiptNo: DataTypes.STRING,
    partNumber: DataTypes.STRING,
    unit: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    quotationID: DataTypes.INTEGER,
    date: DataTypes.DATE,
    note: DataTypes.STRING,
    actualNumber: DataTypes.INTEGER,
    store: DataTypes.STRING,
    position: DataTypes.STRING
  }, {});
  StockIn.associate = function(models) {
    // associations can be defined here
    StockIn.belongsTo(models.Inventorys, { foreignKey: "partNumber" });
    StockIn.belongsTo(models.Quotation, {  sourceKey: "id",foreignKey: "quotationID" });
    StockIn.belongsTo(models.QuotationItem, { foreignKey: "quotationID" });
  };
  return StockIn;
};