'use strict';
module.exports = (sequelize, DataTypes) => {
  const POStepMaster = sequelize.define('POStepMaster', {
    sequence: DataTypes.INTEGER,
    stepName: DataTypes.STRING
  }, {});
  POStepMaster.associate = function(models) {
    // associations can be defined here
  };
  return POStepMaster;
};