// var Joi = require("joi-full");
"use strict";
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      uuid: {
        allowNull: false,
        type: DataTypes.UUID,
        defaultValue: DataTypes.UUIDV4,
      },
      email: DataTypes.STRING,
      password: DataTypes.STRING,
      displayName: DataTypes.STRING,
      tel: DataTypes.STRING,
      address: DataTypes.STRING,
      lastLogin: DataTypes.DATE,
    },
    {}
  );
  User.associate = function (models) {
    // User.belongsToMany(models.User, { through: models.POPIC });
    User.hasMany(models.POPIC, { sourceKey: "id", foreignKey: "picID" });
    User.hasMany(models.DailyReports, { sourceKey: "id", foreignKey: "picId" });
  };

  //Instance method, to return jwt of obj
  User.prototype.generateAuthToken = () => {
    const token = jwt.sign({ email: this.email }, jwtPrivateKey);
    return token;
  };

  // validate data before call db.saving
  // User.prototype.validate = () => {
  //   const schema = {
  //     email: Joi.string().min(5).max(50).required().email(),
  //     password: Joi.string().min(5).max(50).required(),
  //     displayName: Joi.required(),
  //   };

  //   return Joi.validate(this, schema);
  // };

  return User;
};
