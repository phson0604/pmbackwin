'use strict';
module.exports = (sequelize, DataTypes) => {
  const Inventorys = sequelize.define('Inventorys', {
    partNumber: DataTypes.STRING,
    partName: DataTypes.STRING,
    price: DataTypes.FLOAT,
    currency: DataTypes.STRING,
    store: DataTypes.STRING,
    position: DataTypes.STRING,
    unit: DataTypes.STRING,
    actualQty: DataTypes.INTEGER,
    note: DataTypes.STRING
  }, {});
  Inventorys.associate = function(models) {
    // associations can be defined here
    Inventorys.hasMany(models.StockIn, { sourceKey: "partNumber", foreignKey: "partNumber" });
    Inventorys.hasMany(models.StockOut, { sourceKey: "partNumber", foreignKey: "partNumber" });
  };
  return Inventorys;
};