'use strict';
module.exports = (sequelize, DataTypes) => {
  const POStatusMaster = sequelize.define('POStatusMaster', {
    description: DataTypes.STRING,
    descriptionVN: DataTypes.STRING
  }, {});
  POStatusMaster.associate = function(models) {
    // associations can be defined here
  };
  return POStatusMaster;
};