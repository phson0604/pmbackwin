'use strict';
module.exports = (sequelize, DataTypes) => {
  const ApprovalStatus = sequelize.define('ApprovalStatus', {
    description: DataTypes.STRING,
    descriptionVN: DataTypes.STRING
  }, {});
  ApprovalStatus.associate = function(models) {
    // associations can be defined here
  };
  return ApprovalStatus;
};