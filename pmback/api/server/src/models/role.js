'use strict';
module.exports = (sequelize, DataTypes) => {
  const Role = sequelize.define('Role', {
    scope: DataTypes.STRING,
    role: DataTypes.STRING,
    description: DataTypes.STRING,
    descriptionVN: DataTypes.STRING
  }, {});
  Role.associate = function(models) {
    // associations can be defined here
  };
  return Role;
};