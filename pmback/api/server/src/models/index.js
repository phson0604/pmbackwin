import fs from "fs";
import path from "path";
// import Sequelize from "sequelize";
import configJson from "../config/config";

const Sequelize = require("sequelize");
const basename = path.basename(__filename);

const env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
const config = configJson[env];

console.log("this is the environment: ", env);
console.log("actual config: ", config);

const db = {};

let sequelize;
if (env === "production") {
  //this is to persist connection to Heroku Postgres
  //because heroku will change db credential, and update to DATABASE_URL environment variable
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 && file !== basename && file.slice(-3) === ".js"
    );
  })
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

export default db;
