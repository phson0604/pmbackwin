'use strict';
module.exports = (sequelize, DataTypes) => {
  const DailyReports = sequelize.define('DailyReports', {
    plan: DataTypes.STRING,
    result: DataTypes.STRING,
    picId: DataTypes.INTEGER,
    reportDate: DataTypes.DATE
  }, {});
  DailyReports.associate = function(models) {
    // associations can be defined here
    DailyReports.belongsTo(models.User, {sourceKey: "id", foreignKey: "picId" });
  };
  return DailyReports;
};