'use strict';
module.exports = (sequelize, DataTypes) => {
  const StockOut = sequelize.define('StockOut', {
    receiptNo: DataTypes.STRING,
    partNumber: DataTypes.STRING,
    unit: DataTypes.STRING,
    quantity: DataTypes.INTEGER,
    poID: DataTypes.INTEGER,
    date: DataTypes.DATE,
    note: DataTypes.STRING,
    actualNumber: DataTypes.INTEGER,
    store: DataTypes.STRING,
    position: DataTypes.STRING
  }, {});
  StockOut.associate = function(models) {
    // associations can be defined here
    StockOut.belongsTo(models.Inventorys, { foreignKey: "partNumber" });
    StockOut.belongsTo(models.PO, { foreignKey: "poID" });
  };
  return StockOut;
};