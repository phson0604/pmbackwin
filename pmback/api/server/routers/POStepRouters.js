import { Router } from "express";
import POStepController from "../controllers/POStepController";

const router = Router();

router.get("/", POStepController.getAllPOSteps);
router.post("/", POStepController.addPOStep);
router.get("/:id", POStepController.getAPOStep);

router.put("/:id", POStepController.updatedPOStep);
router.delete("/:id", POStepController.deletePOStep);

export default router;
