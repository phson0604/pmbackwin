import { Router } from "express";
import POItemController from "../controllers/POItemController";

const router = Router();

router.get("/", POItemController.getAllPOItems);
router.post("/", POItemController.addPOItem);
router.get("/:id", POItemController.getAPOItem);
router.put("/:id", POItemController.updatedPOItem);
router.delete("/:id", POItemController.deletePOItem);
router.get("/getPo/:id", POItemController.getAllPOItem);

export default router;
