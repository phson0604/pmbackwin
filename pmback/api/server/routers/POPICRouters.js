import { Router } from "express";
import POPICController from "../controllers/POPICController";

const router = Router();

router.get("/", POPICController.getAllPOPICs); //with parameter supported
router.get("/:id", POPICController.getAPOPIC);
router.post("/", POPICController.addPOPIC);
router.put("/:id", POPICController.updatedPOPIC);
router.delete("/:id", POPICController.deletePOPIC);

export default router;
