import { Router } from "express";
import StockInController from "../controllers/StockInController";

const router = Router();

router.get("/",  StockInController.getAllStockIns);
router.get("/getAll/",  StockInController.getAllItemStockIn);
router.get("/getItem/:receiptNo", StockInController.getAllItemStockIns);
router.get("/getItems/:partNumber", StockInController.getOneItemStockIn);
router.get("/getItems/id/:quotationID", StockInController.getAllItemQuationStockIns);
router.post("/", StockInController.addStockIn);
router.delete("/:id", StockInController.deleteStockIn);
//router.delete("/:id", StockInController.deleteStockIn);

export default router;