import { Router } from "express";
import POStepMasterController from "../controllers/POStepMasterController";

const router = Router();

router.get("/", POStepMasterController.getAllPOStepMasters);
router.post("/", POStepMasterController.addPOStepMaster);
router.get("/:id", POStepMasterController.getAPOStepMaster);
router.put("/:id", POStepMasterController.updatedPOStepMaster);
router.delete("/:id", POStepMasterController.deletePOStepMaster);

export default router;
