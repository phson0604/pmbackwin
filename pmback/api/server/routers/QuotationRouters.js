import { Router } from "express";
import QuotationController from "./../controllers/QuotationController";

const router = Router();

router.get("/", QuotationController.getAllQuotations);
router.get("/getAll/:id", QuotationController.getAllSuppliesQuotations);
router.get("/getAll/", QuotationController.getAllQuotation);
router.post("/", QuotationController.addQuotation);
router.get("/:id", QuotationController.getAQuotation);
router.put("/:id", QuotationController.updatedQuotation);
router.delete("/:id", QuotationController.deleteQuotation);

export default router;
