import { Router } from "express";
import DailyReportsController from "../controllers/DailyReportsController";

const router = Router();

router.get("/", DailyReportsController.getAllReports);
router.get("/picId/:id", DailyReportsController.getAllPicIdReport);
router.post("/", DailyReportsController.addReport);
router.get("/:id", DailyReportsController.getAReport);
router.put("/:id", DailyReportsController.updatedReport);
router.delete("/:id", DailyReportsController.deleteReport);

export default router;