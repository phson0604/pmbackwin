import { Router } from "express";
import InventoryController from "../controllers/InventoryController";

const router = Router();

router.get("/", InventoryController.getAllInventoryss);
router.get("/getItem", InventoryController.getAllItemInventorys);
router.post("/", InventoryController.addInventory);
router.get("/getId/:partNumber", InventoryController.getOneInventory);
router.put("/:partNumber", InventoryController.updatedInventory);
//router.delete("/:id", InventoryController.deleteReport);

export default router;