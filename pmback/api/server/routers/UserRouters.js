import { Router } from "express";
import UserController from "../controllers/UserController";

const auth = require("../middleware/auth");
const router = Router();

router.get("/", auth, UserController.getAllUsers);
router.post("/", auth, UserController.addUser);
router.get("/:id", auth, UserController.getAUser);
router.put("/:id", auth, UserController.updatedUser);
router.delete("/:id", auth, UserController.deleteUser);
router.get("/id/:name", auth, UserController.getOneUsers);

export default router;
