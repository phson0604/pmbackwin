import { Router } from "express";
import QuotationItemController from "../controllers/QuotationItemController";

const router = Router();

router.get("/", QuotationItemController.getAllQuotationItems);
router.post("/", QuotationItemController.addQuotationItem);
router.get("/:id", QuotationItemController.getAQuotationItem);
router.put("/:id", QuotationItemController.updatedQuotationItem);
router.delete("/:id", QuotationItemController.deleteQuotationItem);
router.get("/getQo/:id", QuotationItemController.getAllQuotationItemss);
export default router;
