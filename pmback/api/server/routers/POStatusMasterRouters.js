import { Router } from "express";
import POStatusMasterController from "../controllers/POStatusMasterController";

const router = Router();

router.get("/", POStatusMasterController.getAllPOStatusMasters);
router.post("/", POStatusMasterController.addPOStatusMaster);
router.get("/:id", POStatusMasterController.getAPOStatusMaster);
router.put("/:id", POStatusMasterController.updatedPOStatusMaster);
router.delete("/:id", POStatusMasterController.deletePOStatusMaster);

export default router;
