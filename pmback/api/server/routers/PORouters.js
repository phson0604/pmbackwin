import { Router } from "express";
import POController from "../controllers/POController";

const router = Router();

// console.log("PO Router called");

router.get("/", POController.getAllPOs);
router.get("/getAll", POController.getAllPO);
router.post("/", POController.addPO);
router.get("/:id", POController.getAPO);
router.put("/:id", POController.updatedPO);
router.delete("/:id", POController.deletePO);
router.get("/getPoNumber/:poNumber", POController.getAllItemPoNumber);
router.get("/getAll/:id", POController.getAllCustomerPOs);
export default router;
