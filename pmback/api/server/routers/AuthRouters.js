import { Router } from "express";
import AuthController from "../controllers/AuthController";

const router = Router();

router.post("/", AuthController.authenticateUser);

export default router;
