import { Router } from "express";
import StockOutController from "../controllers/StockOutController";

const router = Router();

router.get("/", StockOutController.getAllStockOuts);
router.get("/getAll", StockOutController.getAllItemStockOut);
router.get("/getItem/:receiptNo", StockOutController.getAllItemStockOuts);
router.post("/",  StockOutController.addStockOut);
router.get("/getItems/:partNumber", StockOutController.getOneItemStockOut);
router.delete("/:id", StockOutController.deleteStockOut);

export default router;