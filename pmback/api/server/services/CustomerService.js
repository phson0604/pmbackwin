import db from "../src/models";

class CustomerService {
  static async getAllCustomers() {
    try {
      return await db.Customer.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addCustomer(newCustomer) {
    try {
      return await db.Customer.create(newCustomer);
    } catch (error) {
      throw error;
    }
  }

  static async updateCustomer(id, updateCustomer) {
    try {
      const customerToUpdate = await db.Customer.findOne({
        where: { id: Number(id) }
      });

      if (customerToUpdate) {
        await db.Customer.update(updateCustomer, {
          where: { id: Number(id) }
          
        });

        return updateCustomer;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getACustomer(id) {
    try {
      const theCustomer = await db.Customer.findOne({
        where: { id: Number(id) }
      });

      return theCustomer;
    } catch (error) {
      throw error;
    }
  }

  static async deleteCustomer(id) {
    try {
      const customerToDelete = await db.Customer.findOne({
        where: { id: Number(id) }
      });

      if (customerToDelete) {
        const deletedCustomer = await db.Customer.destroy({
          where: { id: Number(id) }
        });
        return deletedCustomer;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default CustomerService;
