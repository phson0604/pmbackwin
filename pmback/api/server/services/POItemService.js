import db from "../src/models";

class POItemService {
  //get all by PO
  static async getAllPOItems({ poID }) {
    try {
      if (poID) {
        return await db.POItem.findAll({
          where: { poID: poID },
        });
      } else return {}; //await db.POItem.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async getAllPOItem(id) {
    try {    
      const thePO= await db.POItem.findAll({
          where: { poID: Number(id) },
        });
        return thePO;
    } catch (error) {
      throw error;
    }
  }

  static async getAPOItem( poID,partNumber) {
    console.log("Get PO item");
    try {
        const items=await db.POItem.findOne({
          where: { 
            poID: poID ,
            partNumber:partNumber
          },
        });
        console.log('POI:',items);
        return items;
    } catch (error) {
      throw error;
    }
  }
  static async addPOItem(newPOItem) {
    try {
      return await db.POItem.create(newPOItem);
    } catch (error) {
      throw error;
    }
  }

  static async updatePOItem(id, updatePOItem) {
    try {
      const poToUpdate = await db.POItem.findOne({
        where: { id: Number(id) },
      });

      if (poToUpdate) {
        await db.POItem.update(updatePOItem, { where: { id: Number(id) } });

        return updatePOItem;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  // static async getAPOItem(id) {
  //   try {
  //     const thePOItem = await db.POItem.findOne({
  //       where: { id: Number(id) },
  //     });

  //     return thePOItem;
  //   } catch (error) {
  //     throw error;
  //   }
  // }

  static async deletePOItem(id) {
    try {
      const poToDelete = await db.POItem.findOne({
        where: { id: Number(id) },
      });

      if (poToDelete) {
        const deletedPOItem = await db.POItem.destroy({
          where: { id: Number(id) },
        });
        return deletedPOItem;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async deleteAllItemOfPO(poID) {
    // console.log("delete allITemOfPO");
    try {
      const allItems = await this.getAllPOItems({ poID });
      await allItems.forEach(({ dataValues }) => {
        this.deletePOItem(dataValues.id);
      });
    } catch (error) {}
  }
}

export default POItemService;
