import db from "../src/models";
import {getAllReportPicId} from "./DailyReportsService"

class QuotationService {
  static async getAllQuotations({ poID }) {
    try {
      if (poID) {
        return await db.Quotation.findAll({
          where: { poID: poID },
          include: [
            { model: db.PO, attributes: ["poNumber"] },
            { model: db.Supplier, attributes: ["name"] },
            {model: db.QuotationItem}
          ],
        });
      } else return {}; //await db.POItem.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async getAllSuppliesQuotations( id ) {
    try {
      
        return await db.Quotation.findAll({
          where: { id: Number(id) },
          include: [
           // { model: db.PO, attributes: ["poNumber"] },
            { model: db.Supplier, attributes: ["name"] },
            //{model: db.QuotationItem}
            
          ],
          
        });
      //await db.POItem.findAll();
      
    } catch (error) {
      throw error;
    }
  }
  
  static async getAllQuotation() {
    try {     
        return await db.Quotation.findAll({ 
        });    
    } catch (error) {
      throw error;
    }
  }

  static async addQuotation(newQuotation) {
    try {
      return await db.Quotation.create(newQuotation);
    } catch (error) {
      throw error;
    }
  }

  static async updateQuotation(id, updateQuotation) {
    try {
      const quoteToUpdate = await db.Quotation.findOne({
        where: { id: Number(id) },
      });

      if (quoteToUpdate) {
        await db.Quotation.update(updateQuotation, {
          where: { id: Number(id) },
          
        });

        return updateQuotation;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getAQuotation(id) {
    try {
      const theQuotation = await db.Quotation.findOne({
        where: { id: Number(id) },
                  include: [
            { model: db.PO, attributes: ["poNumber"] },
            { model: db.Supplier, attributes: ["name"] },
            {model: db.QuotationItem}
          ],
      });

      return theQuotation;
    } catch (error) {
      throw error;
    }
  }

  static async deleteQuotation(id) {
    try {
      const quoteToDelete = await db.Quotation.findOne({
        where: { id: Number(id) },
      });

      if (quoteToDelete) {
        const deletedQuotation = await db.Quotation.destroy({
          where: { id: Number(id) },
        });
        return deletedQuotation;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default QuotationService;
