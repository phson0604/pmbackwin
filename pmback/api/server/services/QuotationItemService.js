import db from "../src/models";

class QuotationItemService {
  static async getAllQuotationItems({quotationID}) {
    try {
      if (quotationID) {
        return await db.QuotationItem.findAll({
          where: { quotationID: quotationID },
        });
      } else return {};
    } catch (error) {
      throw error;
    }
  }

  static async getAllQuotationItem(id) {
    try {    
      const theReport= await db.QuotationItem.findAll({
          where: { quotationID: Number(id) },
        });
        return theReport;
    } catch (error) {
      throw error;
    }
  }

  static async addQuotationItem(newQuotationItem) {
    try {
      return await db.QuotationItem.create(newQuotationItem);
    } catch (error) {
      // error.message += " hello ";
      throw error;
    }
  }

  static async updateQuotationItem(id, updateQuotationItem) {
    try {
      const QuotationItemToUpdate = await db.QuotationItem.findOne({
        where: { id: Number(id) },
      });

      if (QuotationItemToUpdate) {
        await db.QuotationItem.update(updateQuotationItem, {
          where: { id: Number(id) },
        });

        return updateQuotationItem;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getAQuotationItem(partNumber,quotationID) {
    try {
      const theQuotationItem = await db.QuotationItem.findOne({
        where: { 
          quotationID: quotationID,
          partNumber: partNumber      
        },
      });

      return theQuotationItem;
    } catch (error) {
      throw error;
    }
  }

  static async deleteQuotationItem(id) {
    try {
      const QuotationItemToDelete = await db.QuotationItem.findOne({
        where: { id: Number(id) },
      });

      if (QuotationItemToDelete) {
        const deletedQuotationItem = await db.QuotationItem.destroy({
          where: { id: Number(id) },
        });
        return deletedQuotationItem;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async deleteAllItemOfQuotation(quotationID) {
    try {
      const allItems = await this.getAllQuotationItems({ quotationID });
      await allItems.forEach(({ dataValues }) => {
        this.deleteQuotationItem(dataValues.id);
      });
    } catch (error) {}
  }
}

export default QuotationItemService;
