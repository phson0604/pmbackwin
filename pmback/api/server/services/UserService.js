import db from "../src/models";

class UserService {
  static async getAllUsers() {
    try {
      console.log("getAllUsers");
      return await db.User.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addUser(newUser) {
    try {
      return await db.User.create(newUser);
    } catch (error) {
      throw error;
    }
    
  }

  static async findUser(newUser) {
    try {
      return await db.User.findOne({
        where: { email: newUser.email },
      });
    } catch (error) {
      throw error;
    }
  }

  static async updateUser(id, updateUser) {
    try {
      const userToUpdate = await db.User.findOne({
        where: { id: Number(id) },
      });

      if (userToUpdate) {
        await db.User.update(updateUser, {
          where: { id: Number(id) },
        });

        return updateUser;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getAUser(id) {
    try {
      const theUser = await db.User.findOne({
        where: { id: Number(id) },
      });

      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async getOneUser(name) {
    try {
      const theUser = await db.User.findOne({
        where: { email: String(name) },
        attributes: ["id"]
      });

      return theUser;
    } catch (error) {
      throw error;
    }
  }

  static async deleteUser(id) {
    try {
      const userToDelete = await db.User.findOne({
        where: { id: Number(id) },
      });

      if (userToDelete) {
        const deletedUser = await db.User.destroy({
          where: { id: Number(id) },
        });
        return deletedUser;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static generateAuthToken(user, jwtPrivateKey) {
    const token = jwt.sign(
      { uuid: user.uuid, email: user.email ,id:user.id},
      jwtPrivateKey
    );
    return token;
  }
}

export default UserService;
