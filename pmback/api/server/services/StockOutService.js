import db from "../src/models";
import sv from "../services/InventoryService";
import po from "../services/POItemService";
const _ = require("lodash");
import { async } from "@babel/runtime/regenerator";
class StockOutService {
  static async getAllStockOuts() {
    try {
      return await db.StockOut.findAll({
        attributes: ["receiptNo", "date"],
        group: ["receiptNo", "date"],
      });
    } catch (error) {
      throw error;
    }
  }

  static async getAllItemStockOut() {
    try {
      return await db.StockOut.findAll({
        
      });
    } catch (error) {
      throw error;
    }
  }


  static async getAllItemStockOuts(receiptNo) {
    try {
      return await db.StockOut.findAll({
        where: { receiptNo: String(receiptNo) },
      });
    } catch (error) {
      throw error;
    }
  }

  static async getOneStockOut(id) {
    try {
      const theStockOut = await db.StockOut.findOne({
        where: { id: Number(id) },
      });

      return theStockOut;
    } catch (error) {
      throw error;
    }
  }

  static async getOneItemStockOut(partNumber) {
    try {
      const theStockOut = await db.StockOut.findAll({
        where: { partNumber: String(partNumber) },
      });

      return theStockOut;
    } catch (error) {
      throw error;
    }
  }

  static async addNewStockOut(newStockOut) {
    console.log("newstockout", newStockOut.length);
    try {
      const time = new Date().toISOString();
      let poID=newStockOut[0].poID;
      let newVal={};
      newStockOut = newStockOut.map(async (p) => {
        let prod={
          receiptNo: p.receiptNo,
          currency: p.currency,
          partName: p.partName,
          partNumber: p.partNumber,
          quantity: p.quantity,
          actualNumber:p.actualNumber,
          poID: p.poID,
          unit: p.unit,
          note: p.note,
          date: time,
          store:p.store,
position:p.position
        };
        console.log("prod",prod);
        await db.StockOut.create(prod);
        let qu = await po.getAPOItem(poID, prod.partNumber);
        console.log("qu",qu);
          const iv = await sv.getOneInventorys(qu.partNumber,prod.store,prod.position);
          newVal={
            partNumber: qu.partNumber,
            partName: qu.partName,
            price: qu.unitPrice,
            actualQty: -prod.actualNumber,
            currency: qu.currency,
            store:prod.store,
            position:prod.position
          };
          console.log("iv:",iv);
          if (iv) {
            sv.updateItemInventory(newVal.partNumber, newVal);
          } else {
            _.merge(newVal,{actualQty: newVal.actualQty});
            await sv.addInventory(newVal);
          }
      });
      // //   return await db.StockIn.create(newStockIn);
      return newStockOut;
    } catch (error) {
      throw error;
    }
  }

  static async deleteStockOut(id) {
    try {
      let StockOutToDelete = await db.StockOut.findAll({
        where: { receiptNo: id },
      });

      // console.log(StockInToDelete);
      StockOutToDelete = Array.from(StockOutToDelete);
      let tmp = StockOutToDelete.map(async (p) => {
        let iv = await sv.getOneInventorys(p.partNumber,prod.store,prod.position);
        let newVal = {
          partNumber: p.partNumber,
          partName: p.partName,
          actualQty: p.actualNumber,
          price: p.unitPrice,
          currency: p.currency,
          store:prod.store,
            position:prod.position,
            store:p.store,
position:p.position
        };

        sv.updateItemInventory(p.partNumber, newVal);
      });
      if (StockOutToDelete) {
        const DeletedStockOut = await db.StockOut.destroy({
          where: { receiptNo: id },
        });
        return DeletedStockOut;
      }
      return null;
    } catch (error) {}
    throw error;
  }
}

export default StockOutService;
