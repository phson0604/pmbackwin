import db from "../src/models";
import { STRING } from "sequelize";
const Sequelize = require("sequelize");
const Op = Sequelize.Op;
class InventoryService {
  static async getAllInventorys() {
    try {
      return await db.Inventorys.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addInventory(newInventory) {
    try {
      return await db.Inventorys.create(newInventory);
    } catch (error) {
      throw error;
    }
  }

  static async updateInventory(partNumber, updateInventory) {
    try {
      const InventoryToUpdate = await db.Inventorys.findOne({
        where: { partNumber: String(partNumber) },
      });

      if (InventoryToUpdate) {
        await db.Inventorys.update(updateInventory, {
          where: { partNumber: String(partNumber) },
        });

        return updateInventory;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async updateItemInventory(partNumber, updateInventory) {
    console.log("okkkk",partNumber, updateInventory);
    try {
      let InventoryToUpdate = await db.Inventorys.findOne({
        where: { partNumber: String(partNumber),
          store:String(updateInventory.store),
          position:String(updateInventory.position),
         },

      });

      if (InventoryToUpdate) {
        (updateInventory.actualQty =
          parseInt(InventoryToUpdate.actualQty) + parseInt(updateInventory.actualQty)),
          await db.Inventorys.update(updateInventory, {
            where: { partNumber: String(partNumber),
              store:String(updateInventory.store),
          position:String(updateInventory.position), },
          });

        return updateInventory;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  
 

  static async getOneInventorys(partNumber,store,position) {
    try {
      const theInventory = await db.Inventorys.findOne({
        where: { partNumber: String(partNumber),
          store:String(store),
          position:String(position), },
      });

      return theInventory;
    } catch (error) {
      throw error;
    }
  }

  static async GetAllItemInventorys() {
    try {
      return await db.Inventorys.findAll({
        where: {
          actualQty: {
            [Op.gt]: 0,
          },
        },
      });
    } catch (error) {
      throw error;
    }
  }
}

export default InventoryService;
