import db from "../src/models";
import sv from "../services/InventoryService";
import qo from "../services/QuotationItemService";
import { async } from "@babel/runtime/regenerator";
class StockInService {
  static async getAllStockIns() {
    try {
      return await db.StockIn.findAll({
       attributes: ["receiptNo", "date"],
        group: ["receiptNo", "date"],
      });
    } catch (error) {
      throw error;
    }
  }

  static async getAllItemStockIn() {
    try {
      return await db.StockIn.findAll({
      
      });
    } catch (error) {
      throw error;
    }
  }


  static async getAllItemStockIns( receiptNo ) {
    try {
      return await db.StockIn.findAll({
        where: { receiptNo: String(receiptNo) },
      });
    } catch (error) {
      throw error;
    }
  }

  static async getAllItemQuationStockIns( quotationID ) {
    try {
      return await db.StockIn.findAll({
        where: { quotationID: Number(quotationID) },
      });
    } catch (error) {
      throw error;
    }
  }


  static async getOneStockIn(id) {
    try {
      const theStockIn = await db.StockIn.findOne({
        where: { id: Number(id) },
      });

      return theStockIn;
    } catch (error) {
      throw error;
    }
  }

  static async getOneItemStockIn(partNumber) {
    try {
      const theStockIn = await db.StockIn.findAll({
        where: { partNumber: String(partNumber) },
      });

      return theStockIn;
    } catch (error) {
      throw error;
    }
  }

  static async addNewStockIn(newStockIn) {
    try {
      
      const time = new Date().toISOString();
      let quotationID=newStockIn[0].quotationID;
      let newVal={};
      newStockIn = newStockIn.map(async (p) => {
        let prod={
          receiptNo: p.receiptNo,
          currency: p.currency,
          partName: p.partName,
          partNumber: p.partNumber,
          quantity: p.quantity,
          actualNumber :p.actualNumber,
          quotationID: p.quotationID,
          unit: p.unit,
          note: p.note,
          date: time,
          store:p.store,
          position:p.position
        };
        console.log(newStockIn);
        await db.StockIn.create(prod);
        console.log(prod);
        let qu = await qo.getAQuotationItem(prod.partNumber,quotationID);
        console.log("qu",qu);
          const iv = await sv.getOneInventorys(qu.partNumber,prod.store,prod.position);
          newVal={
            partNumber: qu.partNumber,
            partName: qu.partName,
            price: qu.unitPrice,
            actualQty: prod.actualNumber,
            currency: qu.currency,
            store:prod.store,
            position:prod.position
          };
          console.log("iv:",iv);
          if (iv) {
            sv.updateItemInventory(newVal.partNumber, newVal);
          } else {
            await sv.addInventory(newVal);
          }
      });
      // //   return await db.StockIn.create(newStockIn);
      return newStockIn;
    } catch (error) {
      throw error;
    }
  }

  static async deleteStockIn(id) {
    try {
      let StockInToDelete = await db.StockIn.findAll({
        where: { receiptNo: id },
      });

      // console.log(StockInToDelete);
      StockInToDelete = Array.from(StockInToDelete);
      let tmp = StockInToDelete.map(async (p) => {
        let iv = await sv.getOneInventorys(p.partNumber);
        let newVal = {
          partNumber: p.partNumber,
          partName: p.partName,
          actualQty: -p.actualNumber,
          price: p.unitPrice,
          currency: p.currency,
        };

        sv.updateItemInventory(p.partNumber, newVal);
      });
      if (StockInToDelete) {
        const DeletedStockIn = await db.StockIn.destroy({
          where: { receiptNo: id },
        });
        return DeletedStockIn;
      }
      return null;
    } catch (error) {}
    throw error;
  }
}

export default StockInService;
