import db from "../src/models";

class RoleService {
  static async getAllRoles() {
    try {
      return await db.Role.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addRole(newRole) {
    try {
      return await db.Role.create(newRole);
    } catch (error) {
      throw error;
    }
  }

  static async updateRole(id, updateRole) {
    try {
      const roleToUpdate = await db.Role.findOne({
        where: { id: Number(id) }
      });

      if (roleToUpdate) {
        await db.Role.update(updateRole, {
          where: { id: Number(id) }
        });

        return updateRole;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getARole(id) {
    try {
      const theRole = await db.Role.findOne({
        where: { id: Number(id) }
      });

      return theRole;
    } catch (error) {
      throw error;
    }
  }

  static async deleteRole(id) {
    try {
      const roleToDelete = await db.Role.findOne({
        where: { id: Number(id) }
      });

      if (roleToDelete) {
        const deletedRole = await db.Role.destroy({
          where: { id: Number(id) }
        });
        return deletedRole;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default RoleService;
