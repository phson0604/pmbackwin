import db from "../src/models";
import POStepMasterService from "./POStepMasterService";

/* !!!!!!!!!!
   Return only first row of each group (poID)
   Sequelize ORM not support => Must use raw sql query

   Should include { type: db.sequelize.QueryTypes.SELECT }
   then will return same structure as ORM query
 */
class POStepService {
  static async getAllPOSteps() {
    const STARTING = "0";

    try {
      return await db.sequelize.query(
        `select distinct on ("POSteps"."poID") 
        "POSteps"."id",
        "POs"."poNumber","POStepMasters"."stepName",
        "POSteps"."priority",
        "POSteps"."photoURLs",
        "POSteps"."comment",
        "POs"."issuedDate","POs"."deliveryDate",
        "Customers"."name"
        from public."POSteps"
        left join public."POs" on public."POSteps"."poID"=public."POs"."id"
        left join public."Customers" on "POs"."customerID"="Customers"."id"
        left join public."POStepMasters" on "POSteps"."stepID"="POStepMasters"."id"
        where "POSteps"."status" = (:status)
       ORDER BY "POSteps"."poID", "POStepMasters"."sequence" ASC
        `,
        {
          replacements: { status: STARTING },
          type: db.sequelize.QueryTypes.SELECT,
        }
      );
    } catch (error) {
      throw error;
    }
  }

  static async addPOStep(newPOStep) {
    try {
      return await db.POStep.create(newPOStep);
    } catch (error) {
      throw error;
    }
  }

  static async updatePOStep(id, updatePOStep) {
    try {
      const quoteToUpdate = await db.POStep.findOne({
        where: { id: Number(id) },
      });

      if (quoteToUpdate) {
        await db.POStep.update(updatePOStep, {
          where: { id: Number(id) },
        });

        return updatePOStep;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getAPOStep(id) {
    try {
      const thePOStep = await db.POStep.findOne({
        where: { id: Number(id) },
      });

      return thePOStep;
    } catch (error) {
      throw error;
    }
  }

  static async deletePOStep(id) {
    try {
      const quoteToDelete = await db.POStep.findOne({
        where: { id: Number(id) },
      });

      if (quoteToDelete) {
        const deletedPOStep = await db.POStep.destroy({
          where: { id: Number(id) },
        });
        return deletedPOStep;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async createAllStepsOfNewPO(poID) {
    try {
      const allPOSteps = await POStepMasterService.getAllPOStepMasters();
      // console.log("allPOSteps:",allPOSteps);

      await allPOSteps.forEach(({ dataValues }) => {
        const newPOStep = {
          poID: poID,
          stepID: dataValues.id,
          status: 0, //"starting"
          priority: "normal",
        };

        // console.log("newPOStep:", newPOStep);
        db.POStep.create(newPOStep);
      });
      // return createdPOSteps;
    } catch (error) {
      throw error;
    }
  }

  static async deleteAllStepsOfPO(poID) {
    try {
      const allPOSteps = await db.POStep.findAll({
        where: {
          poID: Number(poID),
        },
      });

      await allPOSteps.forEach(({ dataValues }) => {
        this.deletePOStep(dataValues.id);
      });
    } catch (error) {}
  }
}

export default POStepService;
