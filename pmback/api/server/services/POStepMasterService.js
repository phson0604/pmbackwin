import db from "../src/models";

class POStepMasterService {
  static async getAPOStepMaster(id) {
    try {
      const thePOStepMaster = await db.POStepMaster.findOne({
        where: { id: Number(id) }
      });

      return thePOStepMaster;
    } catch (error) {
      throw error;
    }
  }

  static async getAllPOStepMasters() {
    try {
      return await db.POStepMaster.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addPOStepMaster(newPOStepMaster) {
    try {
      return await db.POStepMaster.create(newPOStepMaster);
    } catch (error) {
      throw error;
    }
  }

  static async updatePOStepMaster(id, updatePOStepMaster) {
    try {
      const quoteToUpdate = await db.POStepMaster.findOne({
        where: { id: Number(id) }
      });

      if (quoteToUpdate) {
        await db.POStepMaster.update(updatePOStepMaster, {
          where: { id: Number(id) }
        });

        return updatePOStepMaster;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async deletePOStepMaster(id) {
    try {
      const quoteToDelete = await db.POStepMaster.findOne({
        where: { id: Number(id) }
      });

      if (quoteToDelete) {
        const deletedPOStepMaster = await db.POStepMaster.destroy({
          where: { id: Number(id) }
        });
        return deletedPOStepMaster;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default POStepMasterService;
