import db from "../src/models";

class POStatusMasterService {
  static async getAllPOStatusMasters() {
    try {
      return await db.POStatusMaster.findAll();
    } catch (error) {
      throw error;
    }
  }

  static async addPOStatusMaster(newPOStatusMaster) {
    try {
      return await db.POStatusMaster.create(newPOStatusMaster);
    } catch (error) {
      throw error;
    }
  }

  static async updatePOStatusMaster(id, updatePOStatusMaster) {
    try {
      const quoteToUpdate = await db.POStatusMaster.findOne({
        where: { id: Number(id) }
      });

      if (quoteToUpdate) {
        await db.POStatusMaster.update(updatePOStatusMaster, {
          where: { id: Number(id) }
        });

        return updatePOStatusMaster;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async getAPOStatusMaster(id) {
    try {
      const thePOStatusMaster = await db.POStatusMaster.findOne({
        where: { id: Number(id) }
      });

      return thePOStatusMaster;
    } catch (error) {
      throw error;
    }
  }

  static async deletePOStatusMaster(id) {
    try {
      const quoteToDelete = await db.POStatusMaster.findOne({
        where: { id: Number(id) }
      });

      if (quoteToDelete) {
        const deletedPOStatusMaster = await db.POStatusMaster.destroy({
          where: { id: Number(id) }
        });
        return deletedPOStatusMaster;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default POStatusMasterService;
