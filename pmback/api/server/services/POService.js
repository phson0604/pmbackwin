import db from "../src/models";

class POService {
  static async getAllPOs() {
    try {
      return await db.PO.findAll({
        include: [
          { model: db.Customer, attributes: ["name"] },
          { model: db.POPIC },
          { model: db.POItem },
          { model: db.Quotation },
        ],
      });
    } catch (error) {
      throw error;
    }
  }

  static async getAllPO() {
    try {
      return await db.PO.findAll({
        
      });
    } catch (error) {
      throw error;
    }
  }

  static async getAllPONumber(poNumber) {
    try {
      const poToUpdate= await db.PO.findAll({
        where:{poNumber:String (poNumber)}
      });
      return poToUpdate;
    } catch (error) {
      throw error;
    }
  }

  static async getAllCustomerPos( id ) {
    try {
      
        return await db.PO.findAll({
          where: { id: Number(id) },
          include: [
           // { model: db.PO, attributes: ["poNumber"] },
            { model: db.Customer, attributes: ["name"] },
            //{model: db.QuotationItem}
            
          ],
          
        });
      //await db.POItem.findAll();
      
    } catch (error) {
      throw error;
    }
  }
  

  static async getAPO(id) {
    try {
      const thePO = await db.PO.findOne({
        include: [
          { model: db.Customer, attributes: ["name"] },
          { model: db.POPIC },
          { model: db.POItem },
          { model: db.Quotation },
        ],
        where: { id: Number(id) },
      });

      return thePO;
    } catch (error) {
      throw error;
    }
  }

  static async addPO(newPO) {
    try {
      return await db.PO.create(newPO);
    } catch (error) {
      throw error;
    }
  }

  static async updatePO(id, updatePO) {
    try {
      const poToUpdate = await db.PO.findOne({
        where: { id: Number(id) },
      });

      if (poToUpdate) {
        await db.PO.update(updatePO, { where: { id: Number(id) } });

        return updatePO;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async deletePO(id) {
    try {
      const poToDelete = await db.PO.findOne({
        where: { id: Number(id) },
      });

      if (poToDelete) {
        const deletedPO = await db.PO.destroy({
          where: { id: Number(id) },
        });
        return deletedPO;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default POService;
