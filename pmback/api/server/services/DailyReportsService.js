import db from "../src/models";

class DailyReportsService {
  static async getAllReport() {
    try {
      return await db.DailyReports.findAll({});
    } catch (error) {
      throw error;
    }
  }
  static async addReport(newReport) {
    try {
      
      return await db.DailyReports.create(newReport);
    } catch (error) {
      throw error;
    }
  }
  static async updateDailyReport(id, updateDailyReport) {
    try {
      const DailyReportToUpdate = await db.DailyReports.findOne({
        where: { id: Number(id) },
        attributes: ["id", "plan", "result"],
      });

      if (DailyReportToUpdate) {
        await db.DailyReports.update(updateDailyReport, {
          where: { id: Number(id) },
        });

        return updateDailyReport;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
  static async getAReport(id) {
    try {
      const theReport = await db.DailyReports.findOne({
        where: { id: Number(id) },
        attributes: ["id", "plan", "result", "picId"],
      });

      return theReport;
    } catch (error) {
      throw error;
    }
  }
  static async getAllReportPicId(id) {
    try {
      
      const theReport = await db.DailyReports.findAll({
        include: [{ model: db.User, attributes: ["email"] }],
        where: { picId: Number(id) },
      });

      return theReport;
    } catch (error) {
      throw error;
    }
  }
  
  static async deleteReport(id) {
    try {
      const ReportToDelete = await db.DailyReports.findOne({
        where: { id: Number(id) },
      });

      if (ReportToDelete) {
        const DeletedReport = await db.DailyReports.destroy({
          where: { id: Number(id) },
        });
        return DeletedReport;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default DailyReportsService;
