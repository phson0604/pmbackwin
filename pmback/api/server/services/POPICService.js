import db from "../src/models";
// const Op = require("Sequelize").Op;

class POPICService {
  //get all by PO
  static async getAllPOPICs({ poID }) {
    try {
      if (poID) {
        /*  !!!!
            WILL return all User, and who take in charge for this PO !!!
            result should same as {User left join POPIC}
        */
        return await db.User.findAll({
          attributes: ["id", "email", "displayName"],
          include: [
            {
              model: db.POPIC,
              attributes: ["id", "poID", "picID", "roles"],
              where: {
                poID: poID,
              },
              required: false, // effect as LEFT JOIN
            },
          ],
        });
      } else {
        return await db.POPIC.findAll({
          include: [
            { model: db.PO, attributes: ["poNumber"] },
            { model: db.User, attributes: ["email", "displayName"] },
          ],
        });
      }
    } catch (error) {
      throw error;
    }
  }

  static async getAPOPIC(id) {
    try {
      const thePOPIC = await db.POPIC.findOne({
        include: [
          { model: db.PO, attributes: ["poNumber"] },
          { model: db.User },
        ],
        where: { id: Number(id) },
      });

      return thePOPIC;
    } catch (error) {
      throw error;
    }
  }

  static async addPOPIC(newPOPIC) {
    try {
      return await db.POPIC.create(newPOPIC);
    } catch (error) {
      throw error;
    }
  }

  static async updatePOPIC(id, updatePOPIC) {
    try {
      const poToUpdate = await db.POPIC.findOne({
        where: { id: Number(id) },
      });

      if (poToUpdate) {
        await db.POPIC.update(updatePOPIC, { where: { id: Number(id) } });

        return updatePOPIC;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }

  static async deletePOPIC(id) {
    try {
      const popicToDelete = await db.POPIC.findOne({
        where: { id: Number(id) },
      });

      if (popicToDelete) {
        const deletedPOPIC = await db.POPIC.destroy({
          where: { id: Number(id) },
        });
        return deletedPOPIC;
      }
      return null;
    } catch (error) {
      throw error;
    }
  }
}

export default POPICService;
