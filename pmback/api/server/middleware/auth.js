import configJson from "../src/config/config";

const jwt = require("jsonwebtoken");

const env = process.env.NODE_ENV ? process.env.NODE_ENV : "development";
const config = configJson[env];
const jwtPrivateKey =
  env === "production" ? process.env["jwtPrivateKey"] : config.jwtPrivateKey;

module.exports = function (req, res, next) {
  //bypass authentication  for development environment

  //if (env === "development")
  return next();

  const token = req.header("x-auth-token");
  if (!token) return res.status(401).send("Access denied. No token provided.");

  try {
    const decoded = jwt.verify(token, jwtPrivateKey);
    req.user = decoded;
    next();
  } catch (ex) {
    res.status(400).send("Invalid token.");
  }
};
