import chai from "chai";
import chaiHttp from "chai-http";
import chaiDateTime from "chai-datetime";

import "chai/register-should";
import app from "../index";

const HTTP_CODE = require("../server/utils/HttpStatusCode");

chai.use(chaiHttp);
chai.use(chaiDateTime);

const { expect } = chai;

describe("Testing the Supplier endpoints:", () => {
  it("It should create a Supplier", done => {
    const supplier = {
      name: "DELL LLC",
      address: "Malaysia",
      contact: "ThienCang"
    };
    // console.log(HTTP_CODE);
    chai
      .request(app)
      .post("/api/v1/Suppliers")
      .set("Accept", "application/json")
      .send(supplier)
      .end((err, res) => {
        res.should.have.status(HTTP_CODE.CREATED);
        expect(res.body.data).to.include({
          name: supplier.name,
          address: supplier.address,
          contact: supplier.contact
        });
        done();
      });
  });
  it("It should not create a Supplier with incomplete parameters", done => {
    const supplier = {
      name: supplier.name,
      //   address: supplier.address,
      contact: supplier.contact
    };
    chai
      .request(app)
      .post("/api/v1/Suppliers")
      .set("Accept", "application/json")
      .send(supplier)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        done();
      });
  });
  it("It should get all Suppliers", done => {
    chai
      .request(app)
      .get("/api/v1/Suppliers")
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        res.body.data[0].should.have.property("id");
        res.body.data[0].should.have.property("name");
        res.body.data[0].should.have.property("address");
        res.body.data[0].should.have.property("contact");
        done();
      });
  });
  it("It should get a particular Supplier", done => {
    const supplierID = "3";
    chai
      .request(app)
      .get(`/api/v1/Suppliers/${supplierID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        // console.log("get a particular Supplier: ", res.status);
        expect(res.status).to.equal(HTTP_CODE.OK);
        res.should.be.json;
        res.body.data.should.have.property("id");
        res.body.data[0].should.have.property("name");
        res.body.data[0].should.have.property("address");
        res.body.data[0].should.have.property("contact");
        done();
      });
  });
  it("It should not get a particular Supplier with invalid id", done => {
    const supplierID = 8888;
    chai
      .request(app)
      .get(`/api/v1/Suppliers/${supplierID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Cannot find Supplier with the id ${supplierID}`);
        done();
      });
  });
  it("It should not get a particular Supplier with non-numeric id", done => {
    const supplierID = "aaa";
    chai
      .request(app)
      .get(`/api/v1/Suppliers/${supplierID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please input a valid numeric value");
        done();
      });
  });
  it("It should update a Supplier", done => {
    const supplierID = 5;
    const updatedSupplier = {
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };
    chai
      .request(app)
      .put(`/api/v1/Suppliers/${supplierID}`)
      .set("Accept", "application/json")
      .send(updatedSupplier)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.OK);
        expect(res.body.data.id).equal(updatedSupplier.id);
        expect(res.body.data.name).equal(updatedSupplier.name);
        expect(res.body.data.address).equal(updatedSupplier.address);
        expect(res.body.data.contact).equal(updatedSupplier.contact);
        done();
      });
  });
  it("It should not update a Supplier with invalid id", done => {
    const supplierID = "9999";
    const updatedSupplier = {
      id: supplierID,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };
    chai
      .request(app)
      .put(`/api/v1/Suppliers/${supplierID}`)
      .set("Accept", "application/json")
      .send(updatedSupplier)
      .end((err, res) => {
        // console.log("res:", res.body);
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Cannot find Supplier with the id: ${supplierID}`);
        done();
      });
  });
  it("It should not update a Supplier with non-numeric id value", done => {
    const SupplierId = "ggg";
    const updatedSupplier = {
      id: SupplierId,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };
    chai
      .request(app)
      .put(`/api/v1/Suppliers/${SupplierId}`)
      .set("Accept", "application/json")
      .send(updatedSupplier)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please input a valid numeric value");
        done();
      });
  });
  it("It should delete a Supplier", done => {
    const SupplierId = 7;
    chai
      .request(app)
      .delete(`/api/v1/Suppliers/${SupplierId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.OK);
        expect(res.body.data).to.include({});
        done();
      });
  });
  it("It should not delete a Supplier with invalid id", done => {
    const SupplierId = 777;
    chai
      .request(app)
      .delete(`/api/v1/Suppliers/${SupplierId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Supplier with the id ${SupplierId} cannot be found`);
        done();
      });
  });
  it("It should not delete a Supplier with non-numeric id", done => {
    const SupplierId = "bbb";
    chai
      .request(app)
      .delete(`/api/v1/Suppliers/${SupplierId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please provide a numeric value");
        done();
      });
  });
});
