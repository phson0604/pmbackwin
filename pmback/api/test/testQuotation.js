import chai from "chai";
import chaiHttp from "chai-http";
import chaiDateTime from "chai-datetime";

import "chai/register-should";
import app from "../index";

const HTTP_CODE = require("../server/utils/HttpStatusCode");

chai.use(chaiHttp);
chai.use(chaiDateTime);

const { expect } = chai;

describe("Testing the Quotation endpoints:", () => {
  it("It should create a Quotation", done => {
    const quotation = {
      poNumber: "Forteen Automation Quotation",
      supplierID: 14,
      receivedDate: "1/30/2020",
      validUntil: "3/1/2020"
    };
    // console.log(HTTP_CODE);
    chai
      .request(app)
      .post("/api/v1/Quotations")
      .set("Accept", "application/json")
      .send(quotation)
      .end((err, res) => {
        const receivedDate = new Date(res.body.data.receivedDate).getDate();
        const validUntil = new Date(res.body.data.validUntil).getDate();
        // console.log(
        //   "res.body.data:",
        //   receivedDate.toDateString(),
        //   res.body.data.receivedDate
        // );
        res.should.have.status(HTTP_CODE.CREATED);
        expect(res.body.data).to.include({
          // id: 1,
          poNumber: quotation.poNumber,
          supplierID: quotation.supplierID
        });
        // expect(receivedDate).equal(quotation.receivedDate);
        // expect(validUntil).equal(quotation.validUntil);
        // expect(res.body.data.receivedDate).equal(quotation.receivedDate);
        // expect(res.body.data.validUntil).equal(quotation.validUntil);
        done();
      });
  });
  //   it("It should not create a Quotation with incomplete parameters", done => {
  //     const quotation = {
  //       poNumber: "Second Quotation",
  //       receivedDate: Date.now()
  //       //   validUntil: Date.now()
  //     };
  //     chai
  //       .request(app)
  //       .post("/api/v1/Quotations")
  //       .set("Accept", "application/json")
  //       .send(quotation)
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         done();
  //       });
  //   });
  //   it("It should get all Quotations", done => {
  //     chai
  //       .request(app)
  //       .get("/api/v1/Quotations")
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(200);
  //         // console.log("RequestReturn:", res.body.data);
  //         res.body.data[0].should.have.property("id");
  //         res.body.data[0].should.have.property("poNumber");
  //         res.body.data[0].should.have.property("supplierID");
  //         res.body.data[0].should.have.property("receivedDate");
  //         res.body.data[0].should.have.property("validUntil");
  //         done();
  //       });
  //   });
  //   it("It should get a particular Quotation", done => {
  //     const quotationID = "3";
  //     chai
  //       .request(app)
  //       .get(`/api/v1/Quotations/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         // console.log("get a particular Quotation: ", res.status);
  //         expect(res.status).to.equal(HTTP_CODE.OK);
  //         res.should.be.json;
  //         res.body.data.should.have.property("id");
  //         res.body.data.should.have.property("poNumber");
  //         res.body.data.should.have.property("supplierID");
  //         res.body.data.should.have.property("receivedDate");
  //         res.body.data.should.have.property("validUntil");
  //         done();
  //       });
  //   });
  //   it("It should not get a particular Quotation with invalid id", done => {
  //     const quotationID = 8888;
  //     chai
  //       .request(app)
  //       .get(`/api/v1/Quotations/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //         res.body.should.have
  //           .property("message")
  //           .eql(`Cannot find Quotation with the id ${quotationID}`);
  //         done();
  //       });
  //   });
  //   it("It should not get a particular Quotation with non-numeric id", done => {
  //     const quotationID = "aaa";
  //     chai
  //       .request(app)
  //       .get(`/api/v1/Quotations/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         res.body.should.have
  //           .property("message")
  //           .eql("Please input a valid numeric value");
  //         done();
  //       });
  //   });
  //   it("It should update a Quotation", done => {
  //     const quotationID = 5;
  //     const updatedQuotation = {
  //       id: quotationID,
  //       poNumber: "Updated ten Quotation",
  //       supplierID: 10,
  //       receivedDate: "1/1/2009",
  //       validUntil: "1/1/2020"
  //     };
  //     chai
  //       .request(app)
  //       .put(`/api/v1/Quotations/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .send(updatedQuotation)
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.OK);
  //         expect(res.body.data.id).equal(updatedQuotation.id);
  //         expect(res.body.data.poNumber).equal(updatedQuotation.poNumber);
  //         expect(res.body.data.supplierID).equal(updatedQuotation.supplierID);
  //         expect(res.body.data.receivedDate).equal(updatedQuotation.receivedDate);
  //         expect(res.body.data.validUntil).equal(updatedQuotation.validUntil);
  //         done();
  //       });
  //   });
  //   it("It should not update a Quotation with invalid id", done => {
  //     const quotationID = "9999";
  //     const updatedQuotation = {
  //       id: quotationID,
  //       poNumber: "Updated Awesome Quotation",
  //       supplierID: 2,
  //       receivedDate: "1/1/2019",
  //       validUntil: "1/1/2020"
  //     };
  //     chai
  //       .request(app)
  //       .put(`/api/v1/Quotations/${quotationID}`)
  //       .set("Accept", "application/json")
  //       .send(updatedQuotation)
  //       .end((err, res) => {
  //         // console.log("res:", res.body);
  //         expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //         res.body.should.have
  //           .property("message")
  //           .eql(`Cannot find Quotation with the id: ${quotationID}`);
  //         done();
  //       });
  //   });
  //   it("It should not update a Quotation with non-numeric id value", done => {
  //     const QuotationId = "ggg";
  //     const updatedQuotation = {
  //       id: QuotationId,
  //       poNumber: "Updated Awesome Quotation again",
  //       supplierID: "1",
  //       receivedDate: "1/1/1999",
  //       validUntil: "1/1/1999"
  //     };
  //     chai
  //       .request(app)
  //       .put(`/api/v1/Quotations/${QuotationId}`)
  //       .set("Accept", "application/json")
  //       .send(updatedQuotation)
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         res.body.should.have
  //           .property("message")
  //           .eql("Please input a valid numeric value");
  //         done();
  //       });
  //   });
  //   it("It should delete a Quotation", done => {
  //     const QuotationId = 7;
  //     chai
  //       .request(app)
  //       .delete(`/api/v1/Quotations/${QuotationId}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.OK);
  //         expect(res.body.data).to.include({});
  //         done();
  //       });
  //   });
  //   it("It should not delete a Quotation with invalid id", done => {
  //     const QuotationId = 777;
  //     chai
  //       .request(app)
  //       .delete(`/api/v1/Quotations/${QuotationId}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
  //         res.body.should.have
  //           .property("message")
  //           .eql(`Quotation with the id ${QuotationId} cannot be found`);
  //         done();
  //       });
  //   });
  //   it("It should not delete a Quotation with non-numeric id", done => {
  //     const QuotationId = "bbb";
  //     chai
  //       .request(app)
  //       .delete(`/api/v1/Quotations/${QuotationId}`)
  //       .set("Accept", "application/json")
  //       .end((err, res) => {
  //         expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
  //         res.body.should.have
  //           .property("message")
  //           .eql("Please provide a numeric value");
  //         done();
  //       });
  //   });
});
