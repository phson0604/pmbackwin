import chai from "chai";
import chaiHttp from "chai-http";
import chaiDateTime from "chai-datetime";

import "chai/register-should";
import app from "../index";


const HTTP_CODE = require("../server/utils/HttpStatusCode");

chai.use(chaiHttp);
chai.use(chaiDateTime);

const { expect } = chai;

describe("Testing the Daily endpoints:", () => {
    it("It should create a daily", done => {
        const daily = {
          id:"3",
          plan: "1",
          resuilt:"OK1",
          createdAt:
            "13/10/2020",
          updatedAt: "12/10/2020",
          picId:4
        };
        // console.log(HTTP_CODE);
        chai
          .request(app)
          .post("/api/v1/DailyReports")
          .set("Accept", "application/json")
          .send(daily)
          .end((err, res) => {
            res.should.have.status(HTTP_CODE.CREATED);
            expect(res.body.data).to.include({
              id:daily.id,
              plan: daily.plan,
              resuilt:daily.resuilt,
              createdAt:daily.createdAt,
              updatedAt:daily.updatedAt,
              
              picId:daily.picId
            });
            done();
          });
      });
      
      it("It should get all Plan", done => {
        chai
          .request(app)
          .get("/api/v1/DailyReports")
          .set("Accept", "application/json")
          .end((err, res) => {
            expect(res.status).to.equal(200);
            res.body.data[0].should.have.property("id");
            res.body.data[0].should.have.property("plan");
            res.body.data[0].should.have.property("resuilt");
            res.body.data[0].should.have.property("createdAt");
            res.body.data[0].should.have.property("updatedAt");
            res.body.data[0].should.have.property("picId");
            done();
          });
      });  
});