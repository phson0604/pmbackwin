import chai from "chai";
import chatHttp from "chai-http";
import "chai/register-should";
import app from "../index";

const HTTP_CODE = require("../server/utils/HttpStatusCode");

chai.use(chatHttp);
const { expect } = chai;

describe("Testing the POItem endpoints:", () => {
  it("It should create a POItem", (done) => {
    const poDetail = {
      poNumber: "Second Test POItem",
      partNumber: "OP-0705",
      partname: "Dell Optiplex SFF",
      quantity: 4,
      unitPrice: 300,
      currency: "USD",
    };
    // console.log(HTTP_CODE);
    chai
      .request(app)
      .post("/api/v1/POItems")
      .set("Accept", "application/json")
      .send(poDetail)
      .end((err, res) => {
        console.log("res:", res.body);
        res.should.have.status(HTTP_CODE.CREATED);
        expect(res.body.data).to.include({
          // id: 1,
          poNumber: poDetail.poNumber,
          partNumber: poDetail.partNumber,
          partname: poDetail.partname,
          quantity: poDetail.quantity,
          unitPrice: poDetail.unitPrice,
          currency: poDetail.currency,
        });
        done();
      });
  });
  it("It should not create a POItem with incomplete parameters", (done) => {
    const poDetail = {
      poNumber: "Second Automation POItem",
      partNumber: "XDX-0101",
      quantity: 10,
      unitPrice: 2.1,
      currency: "USD",
    };
    chai
      .request(app)
      .post("/api/v1/POItems")
      .set("Accept", "application/json")
      .send(poDetail)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        done();
      });
  });
  it("It should get all POItems", (done) => {
    chai
      .request(app)
      .get("/api/v1/POItems")
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        // console.log("RequestReturn:", res.body.data);
        res.body.data[0].should.have.property("id");
        res.body.data[0].should.have.property("poNumber");
        res.body.data[0].should.have.property("partNumber");
        res.body.data[0].should.have.property("partname");
        res.body.data[0].should.have.property("quantity");
        res.body.data[0].should.have.property("unitPrice");
        res.body.data[0].should.have.property("currency");
        done();
      });
  });
  it("It should get a particular POItem", (done) => {
    const poID = "1";
    chai
      .request(app)
      .get(`/api/v1/POItems/${poID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        console.log("get a particular POItem: ", res.status, res.body);
        expect(res.status).to.equal(HTTP_CODE.OK);
        res.should.be.json;

        res.body.data.should.have.property("id");
        res.body.data.should.have.property("poNumber");
        res.body.data.should.have.property("partNumber");
        res.body.data.should.have.property("partname");
        res.body.data.should.have.property("quantity");
        res.body.data.should.have.property("unitPrice");
        res.body.data.should.have.property("currency");
        done();
      });
  });
  it("It should not get a particular POItem with invalid id", (done) => {
    const poID = 8888;
    chai
      .request(app)
      .get(`/api/v1/POItems/${poID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Cannot find POItem with the id ${poID}`);
        done();
      });
  });
  it("It should not get a particular POItem with non-numeric id", (done) => {
    const poID = "aaa";
    chai
      .request(app)
      .get(`/api/v1/POItems/${poID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please input a valid numeric value");
        done();
      });
  });
  it("It should update a POItem", (done) => {
    const poID = 1;
    const updatedPOItem = {
      id: poID,
      poNumber: "Updated Awesome POItem",
      partNumber: "Updated partnumber",
      partname: "Updated partname",
      quantity: "101",
      unitPrice: 200000,
      currency: "VND",
    };
    chai
      .request(app)
      .put(`/api/v1/POItems/${poID}`)
      .set("Accept", "application/json")
      .send(updatedPOItem)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.OK);
        expect(res.body.data.id).equal(updatedPOItem.id);
        expect(res.body.data.poNumber).equal(updatedPOItem.poNumber);
        expect(res.body.data.PartNumer).equal(updatedPOItem.PartNumer);
        expect(res.body.data.partname).equal(updatedPOItem.partname);
        expect(res.body.data.quantity).equal(updatedPOItem.quantity);
        expect(res.body.data.unitPrice).equal(updatedPOItem.unitPrice);
        expect(res.body.data.currency).equal(updatedPOItem.currency);
        done();
      });
  });
  it("It should not update a POItem with invalid id", (done) => {
    const poID = "9999";
    const updatedPOItem = {
      id: poID,
      poNumber: "Updated Awesome POItem Again",
      partNumber: "Updated partnumber",
      partname: "Updated partname",
      quantity: "101",
      unitPrice: 1.1,
      currency: "USD",
    };
    chai
      .request(app)
      .put(`/api/v1/POItems/${poID}`)
      .set("Accept", "application/json")
      .send(updatedPOItem)
      .end((err, res) => {
        // console.log("res:", res.body);
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Cannot find POItem with the id: ${poID}`);
        done();
      });
  });
  it("It should not update a POItem with non-numeric id value", (done) => {
    const poID = "ggg";
    const updatedPO = {
      id: poID,
      poNumber: "Updated Awesome POItem Again",
      partNumber: "Updated partnumber",
      partname: "Updated partname",
      quantity: "101",
      unitPrice: 1.1,
      currency: "USD",
    };
    chai
      .request(app)
      .put(`/api/v1/POItems/${poID}`)
      .set("Accept", "application/json")
      .send(updatedPO)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please input a valid numeric value");
        done();
      });
  });
  it("It should delete a POItem", (done) => {
    const POId = 2;
    chai
      .request(app)
      .delete(`/api/v1/POItems/${POId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.OK);
        expect(res.body.data).to.include({});
        done();
      });
  });
  it("It should not delete a POItem with invalid id", (done) => {
    const POId = 777;
    chai
      .request(app)
      .delete(`/api/v1/POItems/${POId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`POItem with the id ${POId} cannot be found`);
        done();
      });
  });
  it("It should not delete a POItem with non-numeric id", (done) => {
    const POId = "bbb";
    chai
      .request(app)
      .delete(`/api/v1/POItems/${POId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please provide a numeric value");
        done();
      });
  });
});
