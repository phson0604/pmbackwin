import chai from "chai";
import chaiHttp from "chai-http";
import chaiDateTime from "chai-datetime";

import "chai/register-should";
import app from "../index";

const HTTP_CODE = require("../server/utils/HttpStatusCode");

chai.use(chaiHttp);
chai.use(chaiDateTime);

const { expect } = chai;

describe("Testing the Customer endpoints:", () => {
  it("It should create a Customer", done => {
    const customer = {
      name: "Bridgestone LLC",
      address:
        "Land Plot CN3.6-CN4.1, Dinh Vu IZ, Dong Hai 2,Hai An, Hai Phong, Viet Nam",
      contact: "admin.btmv@bridgestone.com"
    };
    // console.log(HTTP_CODE);
    chai
      .request(app)
      .post("/api/v1/Customers")
      .set("Accept", "application/json")
      .send(customer)
      .end((err, res) => {
        res.should.have.status(HTTP_CODE.CREATED);
        expect(res.body.data).to.include({
          name: customer.name,
          address: customer.address,
          contact: customer.contact
        });
        done();
      });
  });
  it("It should not create a Customer with incomplete parameters", done => {
    const customer = {
      name: customer.name,
      //   address: customer.address,
      contact: customer.contact
    };
    chai
      .request(app)
      .post("/api/v1/Customers")
      .set("Accept", "application/json")
      .send(customer)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        done();
      });
  });
  it("It should get all Customers", done => {
    chai
      .request(app)
      .get("/api/v1/Customers")
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(200);
        res.body.data[0].should.have.property("id");
        res.body.data[0].should.have.property("name");
        res.body.data[0].should.have.property("address");
        res.body.data[0].should.have.property("contact");
        done();
      });
  });
  it("It should get a particular Customer", done => {
    const customerID = "3";
    chai
      .request(app)
      .get(`/api/v1/Customers/${customerID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        // console.log("get a particular Customer: ", res.status);
        expect(res.status).to.equal(HTTP_CODE.OK);
        res.should.be.json;
        res.body.data.should.have.property("id");
        res.body.data[0].should.have.property("name");
        res.body.data[0].should.have.property("address");
        res.body.data[0].should.have.property("contact");
        done();
      });
  });
  it("It should not get a particular Customer with invalid id", done => {
    const customerID = 8888;
    chai
      .request(app)
      .get(`/api/v1/Customers/${customerID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Cannot find Customer with the id ${customerID}`);
        done();
      });
  });
  it("It should not get a particular Customer with non-numeric id", done => {
    const customerID = "aaa";
    chai
      .request(app)
      .get(`/api/v1/Customers/${customerID}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please input a valid numeric value");
        done();
      });
  });
  it("It should update a Customer", done => {
    const customerID = 5;
    const updatedCustomer = {
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };
    chai
      .request(app)
      .put(`/api/v1/Customers/${customerID}`)
      .set("Accept", "application/json")
      .send(updatedCustomer)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.OK);
        expect(res.body.data.id).equal(updatedCustomer.id);
        expect(res.body.data.name).equal(updatedCustomer.name);
        expect(res.body.data.address).equal(updatedCustomer.address);
        expect(res.body.data.contact).equal(updatedCustomer.contact);
        done();
      });
  });
  it("It should not update a Customer with invalid id", done => {
    const customerID = "9999";
    const updatedCustomer = {
      id: customerID,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };
    chai
      .request(app)
      .put(`/api/v1/Customers/${customerID}`)
      .set("Accept", "application/json")
      .send(updatedCustomer)
      .end((err, res) => {
        // console.log("res:", res.body);
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Cannot find Customer with the id: ${customerID}`);
        done();
      });
  });
  it("It should not update a Customer with non-numeric id value", done => {
    const CustomerId = "ggg";
    const updatedCustomer = {
      id: CustomerId,
      name: "Updated Bridgestone",
      address: "Sao Kiim",
      contact: "Thien Moc"
    };
    chai
      .request(app)
      .put(`/api/v1/Customers/${CustomerId}`)
      .set("Accept", "application/json")
      .send(updatedCustomer)
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please input a valid numeric value");
        done();
      });
  });
  it("It should delete a Customer", done => {
    const CustomerId = 7;
    chai
      .request(app)
      .delete(`/api/v1/Customers/${CustomerId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.OK);
        expect(res.body.data).to.include({});
        done();
      });
  });
  it("It should not delete a Customer with invalid id", done => {
    const CustomerId = 777;
    chai
      .request(app)
      .delete(`/api/v1/Customers/${CustomerId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.NOT_FOUND);
        res.body.should.have
          .property("message")
          .eql(`Customer with the id ${CustomerId} cannot be found`);
        done();
      });
  });
  it("It should not delete a Customer with non-numeric id", done => {
    const CustomerId = "bbb";
    chai
      .request(app)
      .delete(`/api/v1/Customers/${CustomerId}`)
      .set("Accept", "application/json")
      .end((err, res) => {
        expect(res.status).to.equal(HTTP_CODE.BAD_REQUEST);
        res.body.should.have
          .property("message")
          .eql("Please provide a numeric value");
        done();
      });
  });
});
