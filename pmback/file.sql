--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- name: ApprovalStatuses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."ApprovalStatuses" (
    id integer NOT NULL,
    description character varying(255),
    "descriptionVN" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: ApprovalStatuses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."ApprovalStatuses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: ApprovalStatuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."ApprovalStatuses_id_seq" OWNED BY public."ApprovalStatuses".id;


--
-- name: Customers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Customers" (
    id integer NOT NULL,
    "name" character varying(255),
    "address" character varying(255),
    "contact" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: Customers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Customers_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: Customers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Customers_id_seq" OWNED BY public."Customers".id;


--
-- name: PODetails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."PODetails" (
    id integer NOT NULL,
    "poNumber" character varying(255),
    "partNumber" character varying(255),
    "partname" character varying(255),
    "quantity" integer,
    "unitPrice" double precision,
    "currency" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "Manufacture" character varying(255)
);


--
-- name: PODetails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."PODetails_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: PODetails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."PODetails_id_seq" OWNED BY public."PODetails".id;


--
-- name: POStatuses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."POStatuses" (
    id integer NOT NULL,
    description character varying(255),
    "descriptionVN" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: POStatuses_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."POStatuses_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: POStatuses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."POStatuses_id_seq" OWNED BY public."POStatuses".id;


--
-- name: POs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."POs" (
    id integer NOT NULL,
    "poNumber" character varying(255),
    "customerID" integer,
    "issuedDate" timestamp with time zone,
    "deliveryDate" timestamp with time zone,
    "StatusID" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: POs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."POs_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: POs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."POs_id_seq" OWNED BY public."POs".id;


--
-- name: QuotationDetails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."QuotationDetails" (
    id integer NOT NULL,
    "quotationID" integer,
    "partNumber" character varying(255),
    "partname" character varying(255),
    "quantity" integer,
    "unitPrice" integer,
    "currency" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL,
    "Manufacture" character varying(255)
);


--
-- name: QuotationDetails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."QuotationDetails_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: QuotationDetails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."QuotationDetails_id_seq" OWNED BY public."QuotationDetails".id;


--
-- name: Quotations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Quotations" (
    id integer NOT NULL,
    "poNumber" character varying(255),
    "supplierID" integer,
    "receivedDate" timestamp with time zone,
    "validUntil" timestamp with time zone,
    "approvalID" integer,
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: Quotations_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Quotations_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: Quotations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Quotations_id_seq" OWNED BY public."Quotations".id;


--
-- name: SequelizeMeta; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."SequelizeMeta" (
    name character varying(255) NOT NULL
);


--
-- name: Suppliers; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Suppliers" (
    id integer NOT NULL,
    "name" character varying(255),
    "address" character varying(255),
    "contact" character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: Suppliers_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Suppliers_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: Suppliers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Suppliers_id_seq" OWNED BY public."Suppliers".id;


--
-- name: Users; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public."Users" (
    id integer NOT NULL,
    name character varying(255),
    username character varying(255),
    email character varying(255),
    password character varying(255),
    "createdAt" timestamp with time zone NOT NULL,
    "updatedAt" timestamp with time zone NOT NULL
);


--
-- name: Users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public."Users_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- name: Users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public."Users_id_seq" OWNED BY public."Users".id;


--
-- name: ApprovalStatuses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."ApprovalStatuses" ALTER COLUMN id SET DEFAULT nextval('public."ApprovalStatuses_id_seq"'::regclass);


--
-- name: Customers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Customers" ALTER COLUMN id SET DEFAULT nextval('public."Customers_id_seq"'::regclass);


--
-- name: PODetails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."PODetails" ALTER COLUMN id SET DEFAULT nextval('public."PODetails_id_seq"'::regclass);


--
-- name: POStatuses id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."POStatuses" ALTER COLUMN id SET DEFAULT nextval('public."POStatuses_id_seq"'::regclass);


--
-- name: POs id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."POs" ALTER COLUMN id SET DEFAULT nextval('public."POs_id_seq"'::regclass);


--
-- name: QuotationDetails id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."QuotationDetails" ALTER COLUMN id SET DEFAULT nextval('public."QuotationDetails_id_seq"'::regclass);


--
-- name: Quotations id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Quotations" ALTER COLUMN id SET DEFAULT nextval('public."Quotations_id_seq"'::regclass);


--
-- name: Suppliers id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Suppliers" ALTER COLUMN id SET DEFAULT nextval('public."Suppliers_id_seq"'::regclass);


--
-- name: Users id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Users" ALTER COLUMN id SET DEFAULT nextval('public."Users_id_seq"'::regclass);


--
-- Data for name: ApprovalStatuses; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."ApprovalStatuses" (id, description, "descriptionVN", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for name: Customers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."Customers" (id, "name", "address", "contact", "createdAt", "updatedAt") FROM stdin;
1	Bridgestone LLC	Land Plot CN3.6-CN4.1, Dinh Vu IZ, Dong Hai 2,Hai An, Hai Phong, Viet Nam	admin.btmv@bridgestone.com	2020-01-31 09:41:53.168+07	2020-01-31 09:41:53.168+07
\.


--
-- Data for name: PODetails; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."PODetails" (id, "poNumber", "partNumber", "partname", "quantity", "unitPrice", "currency", "createdAt", "updatedAt", "Manufacture") FROM stdin;
3	Second Test PODetail	OP-0705	Dell Optiplex SFF	4	300	USD	2020-01-21 13:42:49.95+07	2020-01-21 13:42:49.95+07	\N
4	Second Test PODetail	OP-0705	Dell Optiplex SFF	4	300	USD	2020-01-21 13:43:14.118+07	2020-01-21 13:43:14.118+07	\N
1	Updated Awesome PODetail	Updated partnumber	Updated partname	101	200000	VND	2020-01-21 10:20:18.753+07	2020-01-21 13:43:14.324+07	\N
\.


--
-- Data for name: POStatuses; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."POStatuses" (id, description, "descriptionVN", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for name: POs; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."POs" (id, "poNumber", "customerID", "issuedDate", "deliveryDate", "StatusID", "createdAt", "updatedAt") FROM stdin;
\.


--
-- Data for name: QuotationDetails; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."QuotationDetails" (id, "quotationID", "partNumber", "partname", "quantity", "unitPrice", "currency", "createdAt", "updatedAt", "Manufacture") FROM stdin;
1	1	HX5520	Think Agile HX5520 Appliance	1	35000	USD	2020-01-31 15:36:06.388+07	2020-01-31 15:36:06.388+07	Lenovo
2	1	WS-C3850-12XS-S	Cisco Switch WS-C3850-12XS-S	2	15000	USD	2020-01-31 15:37:31.5+07	2020-01-31 15:37:31.5+07	Cisco
3	1	GTX4-10000RT230	UPS Emerson Liebert GXT4 On-Line	2	3000	USD	2020-01-31 15:37:59.135+07	2020-01-31 15:37:59.135+07	Emerson
4	1	93074RX	Rack & PDU	1	2400	USD	2020-01-31 15:38:48.506+07	2020-01-31 15:38:48.506+07	Lenovo
\.


--
-- Data for name: Quotations; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."Quotations" (id, "poNumber", "supplierID", "receivedDate", "validUntil", "approvalID", "createdAt", "updatedAt") FROM stdin;
1	First Automation Quotation	1	2020-01-16 00:00:00+07	2021-01-16 00:00:00+07	\N	2020-01-30 15:12:57.351+07	2020-01-30 15:12:57.351+07
2	Second Automation Quotation	1	2020-01-30 00:00:00+07	2021-02-16 00:00:00+07	\N	2020-01-30 15:54:36.242+07	2020-01-30 15:54:36.242+07
3	Third Automation Quotation	1	2020-01-30 00:00:00+07	2020-02-15 00:00:00+07	\N	2020-01-30 16:00:01.715+07	2020-01-30 16:00:01.715+07
4	Third Automation Quotation	1	2020-01-30 00:00:00+07	2020-02-15 00:00:00+07	\N	2020-01-30 16:01:48.899+07	2020-01-30 16:01:48.899+07
6	Fourth Automation Quotation	2	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:13:03.725+07	2020-01-30 16:13:03.725+07
9	Fourth Automation Quotation	2	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:34:30.039+07	2020-01-30 16:34:30.039+07
10	Ten Automation Quotation	10	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:42:10.226+07	2020-01-30 16:42:10.226+07
11	Ten Automation Quotation	10	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:43:49.512+07	2020-01-30 16:43:49.512+07
5	Updated ten Quotation	10	2009-01-01 00:00:00+07	2020-01-01 00:00:00+07	\N	2020-01-30 16:09:06.921+07	2020-01-30 16:43:49.643+07
12	Tweleve Automation Quotation	12	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:55:03.582+07	2020-01-30 16:55:03.582+07
13	Tweleve Automation Quotation	12	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:55:37.226+07	2020-01-30 16:55:37.226+07
14	Thirdteen Automation Quotation	13	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 16:59:46.6+07	2020-01-30 16:59:46.6+07
15	Forteen Automation Quotation	14	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-30 17:04:07.595+07	2020-01-30 17:04:07.595+07
16	Forteen Automation Quotation	14	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-31 09:09:05.176+07	2020-01-31 09:09:05.176+07
17	Forteen Automation Quotation	14	2020-01-30 00:00:00+07	2020-03-01 00:00:00+07	\N	2020-01-31 09:10:08.428+07	2020-01-31 09:10:08.428+07
\.


--
-- Data for name: SequelizeMeta; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."SequelizeMeta" (name) FROM stdin;
20200115071234-create-user.js
20200115071235-create-po.js
20200115071236-create-po-detail.js
20200115071236-create-po-status.js
20200115071237-create-supplier.js
20200115071238-create-customer.js
20200115071239-create-quotation.js
20200115071240-create-quotation-detail.js
20200115071241-create-approval-status.js
20200131075216-PODetail_Add_ManufactureColumn.js
20200131080059-QuotationDetail_Add_ManufactureColumn.js
\.


--
-- Data for name: Suppliers; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."Suppliers" (id, "name", "address", "contact", "createdAt", "updatedAt") FROM stdin;
1	DELL LLC	Malaysia	ThienCang	2020-01-31 09:47:39.431+07	2020-01-31 09:47:39.431+07
2	DELL LLC	Malaysia	ThienCang	2020-01-31 15:16:06.266+07	2020-01-31 15:16:06.266+07
\.


--
-- Data for name: Users; Type: TABLE DATA; Schema: public; Owner: -
--

COPY public."Users" (id, name, username, email, password, "createdAt", "updatedAt") FROM stdin;
\.


--
-- name: ApprovalStatuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."ApprovalStatuses_id_seq"', 1, false);


--
-- name: Customers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."Customers_id_seq"', 1, true);


--
-- name: PODetails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."PODetails_id_seq"', 4, true);


--
-- name: POStatuses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."POStatuses_id_seq"', 1, false);


--
-- name: POs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."POs_id_seq"', 1, false);


--
-- name: QuotationDetails_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."QuotationDetails_id_seq"', 4, true);


--
-- name: Quotations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."Quotations_id_seq"', 17, true);


--
-- name: Suppliers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."Suppliers_id_seq"', 2, true);


--
-- name: Users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: -
--

SELECT pg_catalog.setval('public."Users_id_seq"', 1, false);


--
-- name: ApprovalStatuses ApprovalStatuses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."ApprovalStatuses"
    ADD CONSTRAINT "ApprovalStatuses_pkey" PRIMARY KEY (id);


--
-- name: Customers Customers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Customers"
    ADD CONSTRAINT "Customers_pkey" PRIMARY KEY (id);


--
-- name: PODetails PODetails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."PODetails"
    ADD CONSTRAINT "PODetails_pkey" PRIMARY KEY (id);


--
-- name: POStatuses POStatuses_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."POStatuses"
    ADD CONSTRAINT "POStatuses_pkey" PRIMARY KEY (id);


--
-- name: POs POs_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."POs"
    ADD CONSTRAINT "POs_pkey" PRIMARY KEY (id);


--
-- name: QuotationDetails QuotationDetails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."QuotationDetails"
    ADD CONSTRAINT "QuotationDetails_pkey" PRIMARY KEY (id);


--
-- name: Quotations Quotations_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Quotations"
    ADD CONSTRAINT "Quotations_pkey" PRIMARY KEY (id);


--
-- name: SequelizeMeta SequelizeMeta_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."SequelizeMeta"
    ADD CONSTRAINT "SequelizeMeta_pkey" PRIMARY KEY (name);


--
-- name: Suppliers Suppliers_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Suppliers"
    ADD CONSTRAINT "Suppliers_pkey" PRIMARY KEY (id);


--
-- name: Users Users_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public."Users"
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

